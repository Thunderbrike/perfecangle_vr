//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//																																	//
//	IMPORTANTE!! LEER ESTO ANTES DE TRADUCIR:																						//
//																																	//
//		1. Esta cabecera NO DEBE traducirse ni borrarse.																			//
//		2. Respetar los espacios entre las líneas tal y como están en el archivo original.											//
//		3. UTILIZAR únicamente comillas dobles normales ("). Cualquier otra comilla, da lugar a error (', '', ‟, ", ‘, ’, etc)		//
//		4. Al final de cada línea debe haber un punto y coma (;)																	//
//		5. Mantener los espacios antes y después del igual (=);																		//
//		6. Respetar las llaves con número dentro ( {0} ). Cualquier otro carácter da lugar a error ( "(0)", "[0]", "<0>", etc )		//
//																																	//
//			Estructura: 	"key" = "texto a traducir";																				//
//																																	//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// IDIOMAS --- NO TRADUCIR

"Spanish" = "Español";
"Catalan" = "Català";
"English" = "English";
"French" = "Français";
"German" = "Deutsche";
"Chinese" = "中国";
"Italian" = "Italiano";
"Portuguese" = "Português";
"Portuguese Brazil" = "Português - Brasil";
"Japanese" = "日本語";
"Korean" = "한국어";
"Russian" = "Русский";


// TEXTOS

"cardboard" = "Do you have Cardboard spectacles?";
"rate" = "Do you like PERFECT ANGLE ZEN?~Please Rate";

"download" = "PLAY";
"buy" = "BUY";
"included" = "INCLUDED";
"restorePurchases" = "RESTORE";

"gameModes" = "+ Game Modes";
"levels" = "+ Levels";
"storyIncluded" = "Story included";
"levelsIncluded" = "All levels are included through a single purchase"
"amazonZEN" = "The Premim version of PERFECT ANGLE ZEN is already included for free in AMAZON UNDERGROUND";

"congratulations" = "CONGRATULATIONS !";
"allLevels" = "You have successfully completed all levels of {0}";
"ifYouliked" = "If you liked playing, you can obtain the complete game version for only {0}";
"downloadGame" = "Do you want to play to PERFECT ANGLE FULL EDITION";

"moveTourHead" = "Find the perfect angle to move on to the next level";
"diveIntoPool" = "Dive into the pool to access the menu";
"slideYourFinger" = "Slide to find the perfect angle";

"resume" = "Resume";
"showHint" = "Show hint";
"clues" = "Use the button to get a hint about the puzzle";
"mainMenu" = "Main Menu";

"accept" = "Accept";
"cancel" = "Cancel";

"shapeClue" = "This is the shape you are searching";
"colorClue" = "The color change to green when you are close to the solution";

"VR" = "Virtual Reality";
"VR2" = "You can choose to play with or without the spectacles";

"actionsExit" = "Exit";

"actionsRecommend" = "Recommend";
"actionsSupport" = "Support";
"actionsRate" = "Rate";
"actionsFacebook" = "Facebook";
"actionsTwitter" = "Twitter";

"settingsGraphicsQuality" = "Graphic quality";
"settingsQualityLevel_0" = "Normal";
"settingsQualityLevel_1" = "High";
"settingsSound" = "Sound";
"settingsMusic" = "Music";
"settingsCardboard" = "Spectacles";
"settingsLanguage" = "Game language";
"settingsResetValues" = "Reset values";

"confirmationReset_Title" = "Are you sure you want to delete all progress?";
"confirmationReset_Confirm" = "Yes";
"confirmationReset_Cancel" = "No";

"CompartirPAfinal" = "Playing #PerfectAngle. The game becomes optical illusions in incredible 3D puzzles";

"NoVideo" = "No videos available at this moment. Try again later";

"_lowram1" = "WARNING";
"_lowram2" = "This device does not meet the recommended requirements for playing games with advanced 3D graphics.";
"_lowram3" = "We recommend you restart the device (switch it off and turn it on again) to increase your chances of it working.";
"_noHints" = "Insufficient hints. Playing video";
"_playFree" = "Play free";
"_comprarPremium" = "Buy premium {0}";
"_explicaPremium" = "All levels + unlimited hints + no ads";
"downloadGame" = "Want to play the full version of Perfect Angle?";
"downloadGame2" = "Want to play the ZEN version of Perfect Angle?";
"gyroscope" = "We’re sorry. Your device does not support virtual reality";
"amazonZEN" = "The Premium version of PERFECT ANGLE ZEN is included free with AMAZON UNDERGROUND";
"ShadowsAlert" = "Your device does not support shadows.";
"errorLevel" = "Want to skip this level?”;
