﻿#if UNITY_EDITOR
// Copyright 2015 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using UnityEngine;

[RequireComponent(typeof(Camera))]
public class EquiRect : MonoBehaviour {
  [HideInInspector]
  public Cubemap cubeMap;

  private Material mat;

  void Start() {
    mat = new Material(Shader.Find("Cardboard/EquiRect"));
    mat.SetTexture("_Cube", cubeMap);
  }

  void OnRenderImage(RenderTexture src, RenderTexture dest) {
    Graphics.Blit(src, dest, mat);
  }
}
#endif