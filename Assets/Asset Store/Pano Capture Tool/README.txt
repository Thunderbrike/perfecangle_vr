360 Panorama Capture Utility for Unity
v0.0.1

Usage:

1) Drag the prefab into your scene.
This tool is meant to be used in the Unity editor.

2) Adjust any settings on the Capture360Pano component.
If you leave the Viewpoint field blank, it will use the Main Camera.

3) Play the scene in the Editor.
If you want to preview the pano in the Game Window, enable the prefab's camera.
If you are using the Cardboard SDK, you need to turn off VR mode.

4) Press the "Capture Pano" button while playing (you can be paused).
Currently, the file is written as "pano.png" in the top-level Unity directory
(above Assets).

Notes:

The Capture360Pano calls "RenderCubemap()" on the viewpoint camera.  Then the
camera attached to the prefab renders from that, creating an equirectangular
360 panorama, using the EquiRect image effect and shader to do that.  In
preview mode, this goes to the Game window.  When capturing the image, it
goes to a RenderTexture, which is then read, converted to PNG, and saved to
disk.