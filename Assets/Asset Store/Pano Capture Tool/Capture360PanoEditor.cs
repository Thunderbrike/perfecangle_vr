﻿#if UNITY_EDITOR
// Copyright 2015 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Capture360Pano))]
public class Capture360PanoEditor : Editor {
  
  private GUIContent capturePano = new GUIContent("Capture Pano", 
      "Capture a 360 panorama from this camera.  Must be playing" +
      " in the editor (paused is OK).");

  public override void OnInspectorGUI() {
    Capture360Pano pano = (Capture360Pano)target;

    DrawDefaultInspector();

    GUI.enabled = Application.isPlaying;

    GUILayout.BeginHorizontal(GUILayout.ExpandHeight(false));
    GUILayout.FlexibleSpace();
    if (GUILayout.Button(capturePano, GUILayout.ExpandWidth(false))) {
      pano.Capture();
    }
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
  }
}
#endif
