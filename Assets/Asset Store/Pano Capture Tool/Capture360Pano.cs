﻿#if UNITY_EDITOR
// Copyright 2015 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using UnityEngine;
using System.IO;

[RequireComponent(typeof(EquiRect))]
public class Capture360Pano : MonoBehaviour {

  [Tooltip("The scene camera which will render the pano.  If not set, Main Camera will be used.")]
  public Camera viewpoint;

  [Tooltip("The resolution of the intermediate cube map used to render the pano.")]
  public int captureResolution = 1024;

  [Tooltip("The pixel resolution width of the rendered pano.")]
  public int panoImageWidth = 2048;

  [Tooltip("The pixel resolution height of the rendered pano.")]
  public int panoImageHeight = 1024;

  private Cubemap cubeMap;
  private RenderTexture image;
  private Material material;

#if UNITY_5
  new public Camera camera { get; private set; }
#endif

  void Reset() {
    captureResolution = 1024;
    panoImageWidth = 2048;
    panoImageHeight = 1024;
    viewpoint = null;
    var cam = GetComponent<Camera>();
    cam.backgroundColor = Color.black;
    cam.cullingMask = 0;
    cam.useOcclusionCulling = false;
    cam.depth = 50;
  }

  void Awake() {
#if UNITY_5
    camera = GetComponent<Camera>();
#endif
    if (!Application.isEditor) {
      gameObject.SetActive(false);
      return;
    }
    cubeMap = new Cubemap(captureResolution, TextureFormat.ARGB32, false);
    image = new RenderTexture(panoImageWidth, panoImageHeight, 0, RenderTextureFormat.ARGB32);
    image.filterMode = FilterMode.Bilinear;
    material = new Material(Shader.Find("Cardboard/EquiRect"));
    material.SetTexture("_Cube", cubeMap);
    GetComponent<EquiRect>().cubeMap = cubeMap;
  }

  void LateUpdate() {
    var vp = viewpoint ? viewpoint : Camera.main;
    if (vp != null && camera.enabled) {
      camera.targetTexture = null;
      vp.RenderToCubemap(cubeMap);
    }
  }

  public void Capture() {
    var vp = viewpoint ? viewpoint : Camera.main;
    if (vp == null) {
      Debug.LogError("Pano Capture Tool requires a viewpoint camera or an active Main Camera.");
      return;
    }
    camera.targetTexture = image;
    vp.RenderToCubemap(cubeMap);
    camera.Render();
    camera.targetTexture = null;
    SaveToFile(Application.dataPath + "/../" + "pano.png");
  }

  private void SaveToFile(string file) {
    byte[] pixels = GetPixels();
    File.WriteAllBytes(file, pixels);
  }

  private byte[] GetPixels() {
    RenderTexture current = RenderTexture.active;
    RenderTexture.active = image;
    Texture2D tex = new Texture2D(image.width, image.height, TextureFormat.RGB24, true);
    tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0, false);
    RenderTexture.active = current;
    tex.Apply();
    byte[] bytes = tex.EncodeToPNG();
    Object.Destroy(tex);
    return bytes;
  }
}
#endif