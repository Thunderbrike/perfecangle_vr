﻿// Copyright 2014 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

Shader "Cardboard/EquiRect" {

  Properties {
    _Cube ("Cube", CUBE) = "" {}
  }

  SubShader {
    Tags { "RenderType" = "Opaque" }
    LOD 150

    Pass {
      CGPROGRAM
      #include "UnityCG.cginc"
      #pragma vertex vert
      #pragma fragment frag

      #ifndef UNITY_PI
      #define UNITY_PI 3.14159265359f
      #endif

      struct v2f {
        float4 vertex  : SV_POSITION;
        half2 texcoord : TEXCOORD0;
      };

      samplerCUBE _Cube;

      v2f vert (appdata_base v) {
        v2f o;
        o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
        o.texcoord = v.texcoord.xy;
        return o;
      }

      fixed4 frag (v2f i) : SV_Target {
        float sinu, cosu, sinv, cosv;
        sincos(2 * UNITY_PI * i.texcoord.x - UNITY_PI, sinu, cosu);
        sincos(UNITY_PI * i.texcoord.y - UNITY_PI / 2, sinv, cosv);
        return texCUBE(_Cube, half3(sinu * cosv, sinv, cosu * cosv));
      }
      ENDCG
    }
  }

}
