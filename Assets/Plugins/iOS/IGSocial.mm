//
//  IGSocial.h
//
//
//  Created by Ivanovich Games on 04/12/15.
//  Copyright © 2015 Ivanovich Games. All rights reserved.
//

#import "IGSocial.h"



@implementation MyMailDelegate

+ (instancetype)sharedInstance{
    
    static MyMailDelegate *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MyMailDelegate alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

- (void)setBaseViewController:(UIViewController*)vc{
    _myBaseViewController = vc;
}

- (id)init {
    
    if (self = [super init]) {
        // Do nothing
    }
    
    return self;
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
    
    [_myBaseViewController dismissViewControllerAnimated:YES completion:nil];
}

@end

@implementation IGSocial

extern UIViewController *UnityGetGLViewController();

void sendMailNative(NSString *text, NSString *subject, NSString *to, UIViewController *viewController){
    
    MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
    [mail setMessageBody:text  isHTML:NO];
    [mail setSubject:subject];
    [mail setToRecipients:[NSArray arrayWithObject:to]];
    [[MyMailDelegate sharedInstance] setBaseViewController:viewController];
    mail.mailComposeDelegate = [MyMailDelegate sharedInstance];
    
    if (mail != nil && [MFMailComposeViewController canSendMail]) {
        [viewController presentViewController:mail animated:YES completion:nil];
    }
}

void shareTextNative(NSString *text, NSString *imagePath, NSString *urlStr, UIViewController *viewController){
    
    NSMutableArray *sharingItems = [NSMutableArray new];
    
    if (text && ![text isEqualToString:@""]) {
        [sharingItems addObject:text];
    }
    
    if (imagePath && ![imagePath isEqualToString:@""]) {
        UIImage *img = [UIImage imageWithContentsOfFile:imagePath];
        [sharingItems addObject:img];
    }
    
    if (urlStr && ![urlStr isEqualToString:@""]) {
        NSURL *url = [NSURL URLWithString:urlStr];
        [sharingItems addObject:url];
    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    
    [viewController presentViewController:activityController animated:YES completion:nil];
}

extern "C" {
    
    void shareText(const char *text, const char *imagePath, const char *urlStr){
        
        shareTextNative( [NSString stringWithUTF8String:text], [NSString stringWithUTF8String:imagePath], [NSString stringWithUTF8String:urlStr],  UnityGetGLViewController() );
    }
    
    void sendMailText(const char *text, const char *subject, const char *to){
        
        sendMailNative( [NSString stringWithUTF8String:text], [NSString stringWithUTF8String:subject], [NSString stringWithUTF8String:to],  UnityGetGLViewController() );
    }
    
}

@end
