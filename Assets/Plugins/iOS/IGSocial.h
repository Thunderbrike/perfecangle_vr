//
//  IGSocial.h
//  
//
//  Created by Ivanovich Games on 04/12/15.
//  Copyright © 2015 Ivanovich Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>

@interface IGSocial : NSObject

@end

@interface MyMailDelegate : NSObject<MFMailComposeViewControllerDelegate>{
    UIViewController *_myBaseViewController;
}

@end