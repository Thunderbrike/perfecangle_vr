﻿
Shader "PerfectAngle/AguaSpec"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 0)
		_Shininess ("Shininess", Range (0.01, 1)) = 0.078125
		_MainTex ("Base (RGB) TransGloss (A)", 2D) = "white" {}
		_BumpMap ("Normalmap", 2D) = "bump" {}
		[HideInInspector]
		_Amplitude ("Amplitud", Range(0.0, 0.1)) = 0.02
		[HideInInspector]
		_Length ("Longitud", Range(0.0, 10)) = 3
		[HideInInspector]
		_Frequency ("Frecuencia", Range(0.0, 300.0)) = 100.0
	}

	SubShader
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		LOD 400
		
		CGPROGRAM
		#pragma surface surf BlinnPhong alpha vertex:vert

		sampler2D _MainTex;
		sampler2D _BumpMap;
		fixed4 _Color;
		half _Shininess;

		float _Amplitude;
		float _Frequency;
		float _Length;
		
		struct Input
		{
			float2 uv_MainTex;
			float2 uv_BumpMap;
		};

		void vert (inout appdata_full v)
		{
			float freq= _Time * _Frequency;
			v.vertex.y +=  (sin(v.vertex.x * _Length + freq) + cos(v.vertex.z * _Length + freq)) * _Amplitude;
		}
		      
		void surf (Input IN, inout SurfaceOutput o)
		{
			fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
			o.Albedo = tex.rgb * _Color.rgb;
			o.Gloss = tex.a;
			o.Alpha = tex.a * _Color.a;
			o.Specular = _Shininess;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
		}
		ENDCG
	}

FallBack "Transparent/VertexLit"
}