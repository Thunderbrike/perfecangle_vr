Shader "PerfectAngle/LightMap Reflection"
{
	Properties 
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_Power ("Power", Range(1.0, 10.0)) = 1.0
		_MainTex ("Base (RGB) RefStrength (A)", 2D) = "white" {}
		_LighMapColor ("LightMap Color", Color) = (1,1,1,0.5)
		_LightMap ("Lightmap (RGB)", 2D) = "black" {}
		_ReflectColor ("Reflection Color", Color) = (1,1,1,0.5)
		_Cube ("Reflection Cubemap", Cube) = "_Skybox" { TexGen CubeReflect }
	}
	
	SubShader 
	{
		LOD 100
		Tags { "RenderType"="Opaque" }
		
		CGPROGRAM
		#pragma surface surf Lambert noforwardadd

		sampler2D _MainTex;
		sampler2D _LightMap;
		samplerCUBE _Cube;

		fixed4 _Color;
		fixed4 _LighMapColor;
		fixed4 _ReflectColor;
		float _Power;

		struct Input
		{
			float2 uv_MainTex;
			float2 uv2_LightMap;
			float3 worldRefl;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
			fixed4 lm = tex2D (_LightMap, IN.uv2_LightMap);
			fixed4 c = tex * _Color;
			o.Albedo = c.rgb * lm.rgb * _Power;
			

			fixed4 reflcol = texCUBE (_Cube, IN.worldRefl);
			reflcol *= tex.a * lm.a;
			o.Emission = (reflcol.rgb * _ReflectColor.rgb) * (_LighMapColor.rgb * lm.rgb);
			o.Alpha = (reflcol.a * _ReflectColor.a) * (_LighMapColor.a * lm.a);
		}
		
		ENDCG
	}
		
	FallBack "Reflective/VertexLit"
} 
