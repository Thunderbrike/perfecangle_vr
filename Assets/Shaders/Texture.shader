// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Versi�n vertex/fragment shader del Shader Legacy/LightMap Diffuse
// Esta es la versi�n m�s barata de un shader con textura.
		// Shader Standard Unity 5.x -> 25.000 a 100.000 lineas
		// Shader Legacy/Diffuse 4.x -> 8.000 lineas
		// Shader Mobile/Diffuse	 -> 5.000 lineas
		// This fucking shader		 -> 350 lineas
// No le afecta ning�n tipo de iluminaci�n ni de sombras
// S�lo tiene una pasada
// No tiene funcion de Fallback
// Compatible para cualquier platarforma y dispositivo
// Compatible para cualquier API y libreria (DirectX, GL2.0, GL3.0 y Metal)

Shader "SimpleShaders/Texture"{
	Properties {
		_Color ("Color Tint", Color) = (1.0,1.0,1.0,1.0)
		_MainTex ("Diffuse Texture", 2D) = "white" {}
		_Ambient ("Ambient Color", Color) = (1.0,1.0,1.0,1.0)
		_Power ("Power Ambient", Range(0.0, 2.0)) = 1.0
	}
	SubShader {
		Pass {
			Tags {"LightMode" = "ForwardBase"}

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			//user defined variables
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform float4 _Color;
			uniform float4 _Ambient;
			uniform float _Power;
			
			//unity defined variables
			uniform float4 _LightColor0;
			
			//base input structs
			struct vertexInput{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
			};
			
			struct vertexOutput{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
			};
			
			//vertex Function
			
			vertexOutput vert(vertexInput v){
				vertexOutput o;
				
				o.posWorld = mul(unity_ObjectToWorld, v.vertex);
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.tex = v.texcoord;
							
				return o;
			}
			
			//fragment function
			
			float4 frag(vertexOutput i) : COLOR
			{
				float4 tex = tex2D(_MainTex, i.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);
				return float4(tex.xyz * _Color.xyz * (_Ambient * _Power), 1.0);
			}
			
			ENDCG
			
		}
	}
}