﻿Shader "PerfectAngle/Agua"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
      	_SpecColor ("Specular Material Color", Color) = (1,1,1,1) 
      	_Shininess ("Shininess", Float) = 10		
		_MainTex ("Tint Color (RGB)", 2D) = "white" {}
		_MainTex2 ("Tint Color (RGB)", 2D) = "black" {}
		_BumpAmt  ("DistortionText1", range (0,64)) = 10
		_BumpAmt2  ("DistortionText2", range (0,64)) = 10
		_BumpMap ("Normalmap", 2D) = "bump" {}
		_BumpMap2 ("Normalmap", 2D) = "bump" {}
		//[HideInInspector]
		_Amplitude ("Amplitud", Range(0.0, 0.1)) = 0.02
		//[HideInInspector]
		_Length ("Longitud", Range(0.0, 10)) = 3
		//[HideInInspector]
		_Frequency ("Frecuencia", Range(0.0, 300.0)) = 100.0
	}

	Category
	{
		Tags { "Queue"="Transparent-100" "RenderType"="Opaque-100" }

		SubShader
		{

			GrabPass
			{							
				Name "BASE"
				Tags { "LightMode" = "Always" }
	 		}
	 		
			Pass
			{
				Name "BASE"
				Tags { "LightMode" = "Always" }
					
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma target 3.0
				#include "UnityCG.cginc"
				
				fixed4 _Color;
				float4 _LightColor;
         		float4 _SpecColor; 
         		float _Shininess;
				
				float _BumpAmt;
				float _BumpAmt2;
				float4 _BumpMap_ST;
				float4 _BumpMap2_ST;
				float4 _MainTex_ST;
				float4 _MainTex2_ST;
				
				float _Amplitude;
				float _Frequency;
				float _Length;
				 
				struct appdata_t
				{
					float4 vertex : POSITION;
					float3 normal : NORMAL;
					float2 texcoord: TEXCOORD0;
					float2 texcoord2: TEXCOORD1;
				};

				struct v2f {
            		float4 col : COLOR;
					float4 vertex : POSITION;
					float4 uvgrab : TEXCOORD0;
					float2 uvbump : TEXCOORD1;
					float2 uvmain : TEXCOORD2;
					float4 posProj : TEXCOORD3;
					float2 uvbump2 : TEXCOORD4;
					float2 uvmain2 : TEXCOORD5;
				};

				v2f vert (appdata_t v )
				{
					v2f o;
					
					// A partir de Unity 5.x hemos de inicializar v2f con la siguiente macro
					UNITY_INITIALIZE_OUTPUT(v2f, o);
					
//					float4x4 modelMatrix = _Object2World;
//            		float4x4 modelMatrixInverse = _World2Object;
//            		float3 normalDirection = normalize(mul(float4(v.normal, 0.0), modelMatrixInverse).xyz);
//            		float3 viewDirection = normalize(_WorldSpaceCameraPos - mul(modelMatrix, v.vertex).xyz);
//            		float3 lightDirection;
//            		float attenuation;
//            		
					#if UNITY_UV_STARTS_AT_TOP
					float scale = -1.0;
					#else
					float scale = 1.0;
					#endif

					float freq= _Time * _Frequency;

//					if (0.0 == _WorldSpaceLightPos0.w) // directional light?
//		            {
//		               attenuation = 1.0; // no attenuation
//		               lightDirection = normalize(_WorldSpaceLightPos0.xyz);
//		            } 
//		            else // point or spot light
//		            {
//		               float3 vertexToLightSource = _WorldSpaceLightPos0.xyz - mul(modelMatrix, v.vertex).xyz;
//		               float distance = length(vertexToLightSource);
//		               attenuation = 1.0 / distance; // linear attenuation 
//		               lightDirection = normalize(vertexToLightSource);
//		            }
//					
//					float3 ambientLighting = UNITY_LIGHTMODEL_AMBIENT.rgb * _Color.rgb;
//					float3 diffuseReflection = attenuation * _LightColor.rgb * _Color.rgb * max(0.0, dot(normalDirection, lightDirection));
// 					float3 specularReflection;
// 					
// 					if (dot(normalDirection, lightDirection) < 0.0) 
//		               specularReflection = float3(0.0, 0.0, 0.0); 
//		            else
//		               specularReflection = attenuation * _LightColor.rgb * _SpecColor.rgb * pow(max(0.0, dot( reflect(-lightDirection, normalDirection), viewDirection)), _Shininess);     
//		               
				    v.vertex.y +=  (sin(v.vertex.x * _Length + freq) + cos(v.vertex.z * _Length + freq)) * _Amplitude;
				    
//				    o.col = float4(ambientLighting + diffuseReflection + specularReflection, 1.0);
					o.vertex = mul( UNITY_MATRIX_MVP, v.vertex );
					o.uvgrab.xy = (float2(o.vertex.x, o.vertex.y*scale) + o.vertex.w) * 0.5;
					o.uvgrab.zw = o.vertex.zw;
					o.uvbump = TRANSFORM_TEX( v.texcoord, _BumpMap );
					o.uvbump2 = TRANSFORM_TEX( v.texcoord2, _BumpMap2 );
					o.uvmain = TRANSFORM_TEX( v.texcoord, _MainTex );
					o.uvmain2 = TRANSFORM_TEX( v.texcoord, _MainTex2 );

					return o;
				}

				sampler2D _GrabTexture;
				float4 _GrabTexture_TexelSize;
				sampler2D _BumpMap;
				sampler2D _BumpMap2;
				sampler2D _MainTex;
				sampler2D _MainTex2;
				 
				half4 frag( v2f i ) : COLOR
				{

					half2 bump = UnpackNormal(tex2D( _BumpMap, i.uvbump )).rg;
					half2 bump2 = UnpackNormal(tex2D( _BumpMap2, i.uvbump2 )).rg;
					float2 offset = bump * _BumpAmt * _GrabTexture_TexelSize.xy;
					float2 offset2 = bump2 * _BumpAmt2 * _GrabTexture_TexelSize.xy;
					i.uvgrab.xy = (offset + offset2) * i.uvgrab.z + i.uvgrab.xy;
					
					half4 col = tex2Dproj( _GrabTexture, UNITY_PROJ_COORD(i.uvgrab));
					half4 text = tex2D( _MainTex, i.uvmain ) + tex2D( _MainTex2, i.uvmain2 ) ;

				    return col * text * _Color;
				}
				
				ENDCG
			}
		}

		// Fallback for older cards and Unity non-Pro 
		SubShader
		{
			Blend DstColor Zero
			Pass {
				Name "BASE"
				SetTexture [_MainTex] {	combine texture }
			}
		}
	}
}
