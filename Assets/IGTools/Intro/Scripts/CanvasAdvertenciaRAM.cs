﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CanvasAdvertenciaRAM : MonoBehaviour {

    public Font tipografia;

    Intro intro;
//    Text textAdvertencia;
    Text lowRam1;
    Text lowRam2;
    Text accept;

    void Start () 
    {
        intro = transform.Find("../IvanovichRocas").gameObject.GetComponent<Intro>();

//        textAdvertencia = transform.Find("Text_Advertencia").gameObject.GetComponent<Text>();
        lowRam1 = transform.Find("Text_LowRam1").gameObject.GetComponent<Text>();
        lowRam2 = transform.Find("Text_LowRam2").gameObject.GetComponent<Text>();
        accept = transform.Find("Text_Accept").gameObject.GetComponent<Text>();

//        textAdvertencia.text = LanguageManager.instance.IdiomaKey("_lowram1");
        lowRam1.text = LanguageManager.instance.IdiomaKey("_lowram2");
        lowRam2.text = LanguageManager.instance.IdiomaKey("_lowram3");
        accept.text = LanguageManager.instance.IdiomaKey("accept");

        if (tipografia != null)
        {
//            textAdvertencia.font = tipografia;
            lowRam1.font = tipografia;
            lowRam2.font = tipografia;
            accept.font = tipografia;
        }
	}

    void Update()
    {
        /* TODO: Si hay controles de mandos, hay que substituir la condición del 'if' por la llamada */
        /* a la función que hace referencia a los controles en la siguiente linea */

        if (Input.GetKeyDown(KeyCode.Return))
        {
            intro.CargarEscena();
            Destroy(gameObject);
        }
    }
}
