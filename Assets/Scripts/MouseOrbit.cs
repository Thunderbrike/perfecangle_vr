﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MouseOrbit : MonoBehaviour
{
    #region Constants
    const int LIMITE_MAXIMO = 90;
    const int SENSIBILIDAD_MOUSE = 3500;
    #endregion

    #region Variables
    static public Quaternion rot;
    static public bool laCamaraEstaOrbitando = true;
    public Vector3 target = new Vector3(0, 10, 0);
    public float distance = 5.0f;
    public float xMinLimit, xMaxLimit;
    public float yMinLimit, yMaxLimit;
    public bool reverseMode = false;

    float rotationYAxis = 0.0f;
    float rotationXAxis = 0.0f;
    float velocityX = 0.0f;
    float velocityY = 0.0f;

    Vector3 posInicial;
    Touch touch;
    #endregion

    public static MouseOrbit instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        rotationYAxis = angles.y;
        rotationXAxis = angles.x;
    }

    void LateUpdate()
    {
        if (Niveles.instance.modoJuego == Niveles.ModoJuego.VR)
        {
            rotationYAxis = rot.eulerAngles.y;
            rotationXAxis = rot.eulerAngles.x;
        }
        else
        {
        
#if UNITY_EDITOR || UNITY_STANDALONE
            if (Input.GetMouseButton(0))
            {
                UpdateVelocidadMouse();
            }
#else    
            if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                touch = Input.GetTouch(0);
                UpdateVelocidadMouse();
            }
#endif

            rotationYAxis += velocityX;
            rotationXAxis -= velocityY;

            rotationYAxis = ClampAngleY(rotationYAxis, xMinLimit, xMaxLimit);
            rotationXAxis = ClampAngleX(rotationXAxis, yMinLimit, yMaxLimit);

        }

        Quaternion fromRotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0);
        Quaternion toRotation = Quaternion.Euler(rotationXAxis, rotationYAxis, 0);
        Quaternion rotation = toRotation;

        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        Vector3 position = rotation * negDistance + target;

        transform.rotation = rotation;
        transform.position = position;

        velocityX = Mathf.Lerp(velocityX, 0, Time.deltaTime * 2);
        velocityY = Mathf.Lerp(velocityY, 0, Time.deltaTime * 2);

    }

    private void UpdateVelocidadMouse()
    {
        float touchDeltax = Input.GetAxis("Mouse X") / Camera.main.pixelWidth * (reverseMode ? -1 : 1) * Time.deltaTime;
        float touchDeltay = Input.GetAxis("Mouse Y") / Camera.main.pixelHeight * Time.deltaTime;
        velocityX += touchDeltax * SENSIBILIDAD_MOUSE;
        velocityY += touchDeltay * SENSIBILIDAD_MOUSE;
    }

    public static float ClampAngleX(float angle, float min, float max)
    {
		if (SceneManager.GetActiveScene().name != "MenuEntrada") {
			if (angle <= -360f) angle += 360f;
			if (angle > 360f) angle -= 360f;
		}

        return Mathf.Clamp(angle, min, max);
    }

    public static float ClampAngleY(float angle, float min, float max)
    {
		if (SceneManager.GetActiveScene().name != "MenuEntrada") {
       	    if (angle <= 0f) angle += 360f;
       	 	if (angle > 360f) angle -= 360f;
		}

        return Mathf.Clamp(angle, min, max);
    }
}