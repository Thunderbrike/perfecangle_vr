﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class CalidadAutomatica : MonoBehaviour {

	public Text infoCalidad;

	int cantidadFrames;
	float currentAverageFps;
	float  intervaloDeTiempoParaAdaptarElQualitySettings = 3f;
	float fpsMinimos = 25f;
	float fpsMaximos = 40;
	IEnumerator routineTimeOut;

	int fpsTotalesEnStep=0;

	int calidadFinal=1;
	int pasos=0;
	
	void Start ()
	{
		infoCalidad.text ="OPTIMIZING graphic quality... \n Please wait...";
		
		StartCoroutine (AdaptarQuality ());
		
		routineTimeOut = timeOut();
		StartCoroutine (routineTimeOut);
	}
	
	
	void Update ()
	{
		UpdateAverageFpsAcumulados (1 / Time.deltaTime);
	}
	
	IEnumerator timeOut(){
		yield return new WaitForSeconds(60);
		FinAutoQualityTimeOut();
	}
	

	float UpdateAverageFpsAcumulados(float newFPS)
	{
		++cantidadFrames;
		currentAverageFps += (newFPS - currentAverageFps) / cantidadFrames;
		
		return currentAverageFps;
	}
	

	IEnumerator AdaptarQuality ()
	{
		while (true) {
			yield return new WaitForSeconds (intervaloDeTiempoParaAdaptarElQualitySettings);
			
			if (Debug.isDebugBuild) {
				Debug.Log ("FPS: " + currentAverageFps);
			}
			
			// Bajamos un nivel si el Fps es bajo
			if (currentAverageFps < fpsMinimos) {
				//Bajamos la calidad
				calidadFinal = 0;
				Debug.Log("Bajamos calidad a: "+calidadFinal );
				FinAutoQuality();
			}else if( currentAverageFps > fpsMinimos) {
				//Subimos la calidad si el fps es alto
				calidadFinal = 1;
				Debug.Log("Subimos calidad a: "+calidadFinal );
				FinAutoQuality();
			}else{
				pasos++;
			} 

			if( pasos>1){
				FinAutoQuality();
			} 

			// Reseteamos
			cantidadFrames = 0;
			currentAverageFps = 0;
		}
	}

	void FinAutoQuality(){

		StopCoroutine(routineTimeOut);
		
		if (Debug.isDebugBuild) {
			Debug.Log ("Fps es estable, eliminamos adaptacion automatica.");
		}

		infoCalidad.text = "End Quality.";
	
        switch (Niveles.instance.modoJuego)
        {
            case Niveles.ModoJuego.VR:
                PlayerPrefs.SetInt("QualityVR", calidadFinal);
                Niveles.instance.nivelDeCalidadParaModoVR = calidadFinal;
                break;

            case Niveles.ModoJuego.Normal:
                PlayerPrefs.SetInt("QualityNormal", calidadFinal);
                Niveles.instance.nivelDeCalidadParaModoNormal = calidadFinal;
                break;

            case Niveles.ModoJuego.AR:
                break;
        }
		

		//Cargamos el nivel principal si se ha completado con exito
        SceneManager.LoadScene("MenuEntrada");
		Destroy (this);
	}
	
	void FinAutoQualityTimeOut()
    {
        switch (Niveles.instance.modoJuego)
        {
            case Niveles.ModoJuego.VR:
                PlayerPrefs.SetInt("QualityVR", 0);
                Niveles.instance.nivelDeCalidadParaModoVR = 0;
                break;

            case Niveles.ModoJuego.Normal:
                PlayerPrefs.SetInt("QualityNormal", 0);
                Niveles.instance.nivelDeCalidadParaModoNormal = 0;
                break;

            case Niveles.ModoJuego.AR:
                break;
        }

		//Cargamos el nivel principal en TimeOut
        SceneManager.LoadScene("MenuEntrada");

		Destroy (this);
	}

}
