﻿using UnityEngine;
using System.Collections;

public class MenuPista : MonoBehaviour 
{
    private float rayCastDistance;

	void Start ()
    {
        FadeEntreEscenas.instance.FadeOut();

        Niveles.instance.tipoPista = (Niveles.instance.tipoPista == Niveles.TipoPista.None) ? Niveles.TipoPista.Forma : Niveles.TipoPista.Color;
        GameObject panelPista = Instantiate(Resources.Load("Prefabs/PanelPista" + (Niveles.instance.modoJuego == Niveles.ModoJuego.VR ? "VR" : "Normal")), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
        Niveles.instance.puedoEnviarAlgunEvento = true;
        Niveles.instance.yaHeUsadoUnaPistaAlgunaVez = true;
        PlayerPrefs.SetInt("YaHeUsadoUnaPistaAlgunaVez", 1);

        MetodosComunes.instance.ComprobarModoJuego(gameObject);
	}
}
