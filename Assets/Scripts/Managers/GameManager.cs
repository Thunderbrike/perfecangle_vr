﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
    public GameObject escena;
    public GameObject canvas;
    public GameObject fake;

    [HideInInspector]
    public GameObject puzzle;
    [HideInInspector]
    public Vector3 solution;
    [HideInInspector]
    public float oldDistance, newDistance;
	[HideInInspector]
	public GameObject sinPistas;
	[HideInInspector]
	public GameObject sinVideos;

    static public GameObject modelo;
    static public GameObject modeloSolucionado;
    static public GameObject piezas;
    static public GameObject soportes;

    private List<GameObject> pistas = new List<GameObject>();
    private int numPuzzle;
    private float counter = 0.0f;
    private bool flagEstableceDistancia;
    private float counterPista = 0;
    private GameObject hint, rate, help; 
    private GameObject padre, whiteRoom;
    private LandingSpotController landingController;
    public static int numPistas=0;
    public static int numNivelVideos=0;
	public GameObject Bolita;
	public Text numeroDePistasQueMeQuedan;
	public static int numeroDePistasMostradas=0;

	public static int loopPublicidad=0;
    
    void Awake()
    {
        ComprobarModoJuego();
        StartCoroutine(StartFadeOut());
    }

    IEnumerator StartFadeOut()
    {
        yield return new WaitForSeconds(1);
        FadeEntreEscenas.instance.FadeOut();
        fake.SetActive(false);
        GenericEffectHelpers.instance.FadeCanvas(canvas, 0, 1, 1);
    }

    void Start()
    {
        ComprobarCalidad();
        
        if (Niveles.instance.esLaPrimeraVezQueJuego) 
        {
            GameObject panelnfo = Instantiate(Resources.Load("Prefabs/PanelInfoInicio" + (Niveles.instance.modoJuego == Niveles.ModoJuego.VR ? "VR" : "Normal")), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
            if (Niveles.instance.modoJuego == Niveles.ModoJuego.Normal) canvas.SetActive(false);
        }
        else EmpiezoAJugar();

        CargarPistas();

		if( ComprasManager.instance.hemosCompradoElJuego ){
			Bolita.SetActive(false);
		}
    }

    void Update()
    {
        if (!Niveles.instance.puedoEmpezarAJugar) return;

        InicioPistaAyuda();
        //Debug.DrawLine(gameObject.GetComponent<MouseOrbit>().target, gameObject.transform.position, Color.magenta);

        if (flagEstableceDistancia)
        {
            counter += Time.deltaTime;
            oldDistance = gameObject.GetComponent<MouseOrbit>().distance;
            gameObject.GetComponent<MouseOrbit>().distance = Mathf.Lerp(gameObject.GetComponent<MouseOrbit>().distance, newDistance, counter * 0.5f);

            if (Mathf.Abs(gameObject.GetComponent<MouseOrbit>().distance - newDistance) < 0.001f)
            {
                flagEstableceDistancia = false;
                counter = 0;
            }
        }

        ComprueboBotonDelCardboard();
    }

    private void InicioPistaAyuda()
    {
        if (Niveles.instance.yaHeUsadoUnaPistaAlgunaVez) return;

        counterPista += Time.unscaledDeltaTime;

        if (counterPista >= 60 * 1 && Niveles.instance.puedoEnviarAlgunEvento) //Tiempo en que mostramos el tutorial de Pista = 2 minutos
        //WIP:Brike -- OJO que hay que cambiar el tiempo en que mostramos la pista. La linea correcta es la de arriba
        //if (counterPista >= 5 && Niveles.instance.puedoEnviarAlgunEvento)
        {
            gameObject.GetComponent<MouseOrbit>().enabled = false;
            Niveles.instance.yaHeUsadoUnaPistaAlgunaVez = true;
            PlayerPrefs.SetInt("YaHeUsadoUnaPistaAlgunaVez", 1);

            help.SetActive(true);
        }
    }

    private void ComprueboBotonDelCardboard() 
    {
        if (Niveles.instance.modoJuego != Niveles.ModoJuego.VR) return;

        Touch touch;

#if UNITY_EDITOR || UNITY_STANDALONE
        if (Input.GetMouseButton(0))
        {
            MetodosComunes.instance.MenuPausa();
        }
#else    
        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            touch = Input.GetTouch(0);
            MetodosComunes.instance.MenuPausa();
        }
#endif

    }

    public void EmpiezoAJugar()
    {

        numPuzzle = Niveles.instance.numPuzzle;
        Niveles.instance.puedoEmpezarAJugar = true;
        gameObject.GetComponent<MouseOrbit>().enabled = true;
        Destroy(gameObject.GetComponent<DeteccionBoton>());
        Destroy(gameObject.transform.Find("PuntoMira").gameObject);
        if (Niveles.instance.modoJuego == Niveles.ModoJuego.Normal) canvas.SetActive(true);
        CambioPuzzle(numPuzzle);
    }

    private void ComprobarCalidad()
    {
        switch (Niveles.instance.modoJuego)
        {
            case Niveles.ModoJuego.VR:
                whiteRoom = Instantiate(Resources.Load("Prefabs/" + (Niveles.instance.nivelDeCalidadParaModoVR == 0 ? "WhiteRoomBaja" : "WhiteRoomAlta"))) as GameObject;
                gameObject.GetComponent<UnityStandardAssets.ImageEffects.SunShafts>().enabled = Niveles.instance.nivelDeCalidadParaModoVR == 0 ? false : true;
                break;

            case Niveles.ModoJuego.Normal:
                whiteRoom = Instantiate(Resources.Load("Prefabs/" + (Niveles.instance.nivelDeCalidadParaModoNormal == 0 ? "WhiteRoomBaja" : "WhiteRoomAlta"))) as GameObject;
                gameObject.GetComponent<UnityStandardAssets.ImageEffects.SunShafts>().enabled = Niveles.instance.nivelDeCalidadParaModoNormal == 0 ? false : true;
                break;

            case Niveles.ModoJuego.AR:
                break;
        }
        
        whiteRoom.name = "WhiteRoom";
    }

    private void ComprobarModoJuego()
    {
        switch (Niveles.instance.modoJuego)
        {
            case Niveles.ModoJuego.VR:
                Destroy(gameObject.transform.Find("Canvas").gameObject);
                Destroy(gameObject.GetComponent<UnityStandardAssets.ImageEffects.SunShafts>());
                break;

            case Niveles.ModoJuego.Normal:
                gameObject.transform.Find("PuntoMira").gameObject.SetActive(false);
                Destroy(gameObject.GetComponent<StereoController>());
                Destroy(gameObject.GetComponent<CardboardHead>());
                Destroy(gameObject.transform.Find("Camera Left").gameObject);
                Destroy(gameObject.transform.Find("Camera Right").gameObject);
                Destroy(GameObject.Find("Cardboard").gameObject);
                gameObject.GetComponent<UnityStandardAssets.ImageEffects.SunShafts>().enabled = true;
                canvas.transform.parent = null;
                hint = canvas.transform.Find("IconoPistas").gameObject;
                hint.transform.Find("Hint").GetComponent<Image>().sprite = Niveles.instance.iconoNivel[Niveles.instance.GetNumeroDeSpriteSegunNombreDelNivel("Hint")];
                rate = canvas.transform.Find("PanelRate").gameObject;
                rate.SetActive(false);
                help = canvas.transform.Find("PanelAyuda").gameObject;
                help.SetActive(false);
				sinPistas = canvas.transform.Find("PanelSinPistas").gameObject;
				sinPistas.SetActive(false);
				sinVideos= canvas.transform.Find("PanelNoVideos").gameObject;
				sinVideos.SetActive(false);
                break;

            case Niveles.ModoJuego.AR:
                break;
        }        
    }

    private void CreateNullObject()
    {
        padre = new GameObject();
        padre.name = "Puzzle";
    }

    public void DestroyNullObject()
    {
        Destroy(padre);
    }

    public void EstablezcoNuevoPuzzle()
    {
        numPuzzle++;
        counterPista = 0;
		
        Niveles.instance.numPuzzle = numPuzzle;
        
        if (numPuzzle > Niveles.instance.numNivelAlcanzado)
        {
            Niveles.instance.numNivelAlcanzado = numPuzzle;
            PlayerPrefs.SetInt("NumeroNivelAlcanzado", numPuzzle);
        }

    }

    public void PuzzleSiguiente()
    {
		numeroDePistasMostradas=0;
//		Debug.Log("numeroDePistasMostradas: "+numeroDePistasMostradas);
//		Debug.Log("numPuzzle: "+numPuzzle);
//		Debug.Log("Niveles.instance.maximoNivelFree: "+Niveles.instance.maximoNivelFree);
//		Debug.Log("ComprasManager.instance.hemosCompradoElJuego: "+ComprasManager.instance.hemosCompradoElJuego);
//
       // Mostramos la pantalla de compra en caso que haya llegado al último nivel FREE
        if (numPuzzle >= Niveles.instance.maximoNivelFree &&
            !ComprasManager.instance.hemosCompradoElJuego &&
			Niveles.instance.modoJuego == Niveles.ModoJuego.VR)
        {

			Debug.Log("Compra!!!");
			Niveles.instance.tengoQueCargarPantallaCompras = true;
            MetodosComunes.instance.MenuEntrada();
            return;
        }

		//En modo VR no queremos pantalla de compra
		if (Niveles.instance.modoJuego != Niveles.ModoJuego.VR) {
			Niveles.instance.tengoQueCargarPantallaCompras = false;
		}
			
        // Mostramos la pantalla de fin de nivel en caso que haya acabado el juego
        if (numPuzzle == Niveles.instance.nombreNivel.Length)
        {
            Niveles.instance.tengoQueCargarPantallaFinal = true;
            MetodosComunes.instance.MenuEntrada();
            return;
        }

        // Mostramos la pantalla de Rate si estamos jugando en Modo Normal y llegamos al máximo permitido
        if (Niveles.instance.modoJuego == Niveles.ModoJuego.Normal && numPuzzle == Niveles.instance.maximoNivelRate && !Niveles.instance.heMostradoLaPantallaDeRate)
        {
            Niveles.instance.heMostradoLaPantallaDeRate = true;
            rate.SetActive(true);
            gameObject.GetComponent<MouseOrbit>().enabled = false;
            PlayerPrefs.SetInt("HemosMostradoLaPantallaDeRate", 1);
            PlayerPrefs.DeleteKey("TengoQueMostrarLaPantallaDeRateAlInicio");
        }

        // Nos aseguramos que nos muestre la pantalla de Rate al inicio del juego si llega al mínimo de niveles
        if (numPuzzle == Niveles.instance.minimoNivelRate && !Niveles.instance.heMostradoLaPantallaDeRate)
            PlayerPrefs.SetInt("TengoQueMostrarLaPantallaDeRateAlInicio", 1);

        // Por fin, si hemos llegado aquí, cambiamos de puzzle!!
        CambioPuzzle(numPuzzle);
    }

    public void CambioPuzzle(int numPuzzle)
    {

        CreateNullObject();

        // Buscamos el puzzle en Resources (según el número) y lo instanciamos
        GameObject clone = Resources.Load("Niveles/" + Niveles.instance.nombreNivel[numPuzzle].ToString()) as GameObject;
        puzzle = Instantiate(clone, padre.transform.position, padre.transform.rotation) as GameObject;
        puzzle.name = clone.name;
        puzzle.transform.parent = padre.transform;

        CargaVariables(puzzle);
    }

    void CargaVariables(GameObject puzzle)
    {
        // Si estamos en modo VR, tenemos que recolocar el puzzle según las nuevas coordenadas
        if (Niveles.instance.modoJuego == Niveles.ModoJuego.VR || Niveles.instance.modoJuego == Niveles.ModoJuego.Normal)
        {
            puzzle.transform.Find(puzzle.name).transform.position = puzzle.transform.Find(puzzle.name + "VR").transform.position;
            puzzle.transform.Find(puzzle.name).transform.rotation = puzzle.transform.Find(puzzle.name + "VR").transform.rotation;
            soportes = puzzle.transform.Find(puzzle.name + "VR/SoportesVR").gameObject;
            puzzle.transform.Find(puzzle.name + "/Puzzle/Modelo/Soportes").gameObject.SetActive(false);
        }
        else
        {
            soportes = puzzle.transform.Find(puzzle.name + "/Puzzle/Modelo/Soportes").gameObject;
            puzzle.transform.Find(puzzle.name + "VR/SoportesVR").gameObject.SetActive(false);
        }

        // Pasamos las variables del puzzle
        solution = puzzle.transform.Find(puzzle.name + "/Solution").transform.position;
        modelo = puzzle.transform.Find(puzzle.name + "/Puzzle/Modelo").gameObject;
        modeloSolucionado = puzzle.transform.Find("ModeloSolucionado").gameObject;
        piezas = puzzle.transform.Find(puzzle.name + "/Puzzle/Piezas").gameObject;
        landingController = puzzle.transform.Find("LandingController").gameObject.GetComponent<LandingSpotController>();

        // Desactivamos los objetos que no interesan
        modeloSolucionado.SetActive(false);
        piezas.SetActive(false);
        
        // Asignamos el controlador al 'Landing Controller' del puzzle y activamos las opciones del landing si venimos del menú de entrada
        landingController._flock = escena.transform.Find("Efectos/Mariposas").gameObject.GetComponent<FlockController>() as FlockController;
        if (Niveles.instance.vengoDelMenuEntrada)
        {
            Niveles.instance.vengoDelMenuEntrada = false;
            landingController._landOnStart = true;
            landingController._soarLand = false;
            landingController._onlyBirdsAbove = true;
        }


        // Activamos el flag para situar la cámara en el punto inicial en Update
        flagEstableceDistancia = true;
        oldDistance = gameObject.GetComponent<MouseOrbit>().distance;
        newDistance = Vector3.Distance(puzzle.transform.Find(puzzle.name + "/Solution").transform.position, gameObject.GetComponent<MouseOrbit>().target);
    }

    private void CargarPistas()
    {

		numeroDePistasQueMeQuedan.text = numPistas.ToString();

        for (int n = 0; n < 3; n++)
            pistas.Add(escena.transform.Find("Peces/koi_" + (n+1).ToString() + "/Centro/Pista").gameObject);

        OcultarPistaVR();

        switch (Niveles.instance.modoJuego)
        {
            case Niveles.ModoJuego.VR:
                if (Niveles.instance.tipoPista != Niveles.TipoPista.None)
                {
                    for (int n = 0; n < 3; n++)
                    {
                        pistas[n].SetActive(true);
                        pistas[n].GetComponent<SpriteRenderer>().sprite = Niveles.instance.iconoNivel[Niveles.instance.GetNumeroDeSpriteSegunNombreDelNivel(Niveles.instance.nombreNivel[numPuzzle])];
                    }
                }
                break;

            case Niveles.ModoJuego.Normal:
                if (Niveles.instance.tipoPista != Niveles.TipoPista.None)
                {
                    hint.transform.Find("Hint").GetComponent<Image>().sprite = Niveles.instance.iconoNivel[Niveles.instance.GetNumeroDeSpriteSegunNombreDelNivel(Niveles.instance.nombreNivel[numPuzzle])];
                }
                else
                {
                    OcultarPistaNormal();
                }
                break;

            case Niveles.ModoJuego.AR:
                break;
        }
    }

    public void OcultarPistaVR()
    {
        for (int n = 0; n < 3; n++) pistas[n].SetActive(false);
    }

    public void OcultarPistaNormal()
    {
        hint.transform.Find("Hint").GetComponent<Image>().sprite = Niveles.instance.iconoNivel[Niveles.instance.GetNumeroDeSpriteSegunNombreDelNivel("Hint")];
    }
}
