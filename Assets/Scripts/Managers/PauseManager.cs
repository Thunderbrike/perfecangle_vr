﻿using UnityEngine;

public class PauseManager : MonoBehaviour
{
    void OnApplicationFocus(bool focusStatus)
    {
#if !UNITY_EDITOR
        if (!focusStatus && !Niveles.instance.debugModoPausaDesactivado){
            MetodosComunes.instance.MenuPausa();
		}
#endif
    }
}
