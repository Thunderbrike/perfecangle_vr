﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.IO;

public class CompartirManager {

	#if UNITY_IPHONE	
	[DllImport ("__Internal")]
	private static extern void shareText(string arr1,string arr2,string arr3);
	#endif
	
	public static void Configurar(){

		#if UNITY_ANDROID

        Texture2D iconoPAVR = Resources.Load("IconoPAVR") as Texture2D;
		string pathShareJuegoAndroid = Application.persistentDataPath + "/ShareJuego.png" ;
		string pathShareFinalJuegoAndroid = Application.persistentDataPath + "/ShareFinal.png" ;

		if(!File.Exists(pathShareJuegoAndroid)){

            byte[] dataToSave = iconoPAVR.EncodeToPNG();
			File.WriteAllBytes(pathShareJuegoAndroid, dataToSave);
		}

		if(!File.Exists(pathShareFinalJuegoAndroid)){

            byte[] dataToSave2 = iconoPAVR.EncodeToPNG();
			File.WriteAllBytes(pathShareFinalJuegoAndroid, dataToSave2);
		}

		#endif
	}

	public static void Share( string text , string nombreImagen, string url) {

		#if UNITY_IPHONE

		shareText( text, nombreImagen, url );

		#elif UNITY_ANDROID

		string pathShareJuegoAndroid = Application.persistentDataPath + "/ShareJuego.png" ;
		string pathShareFinalJuegoAndroid = Application.persistentDataPath + "/ShareFinal.png" ;

		string imagepath = "";

		if(nombreImagen.Contains("IconoPA")){
			imagepath = pathShareJuegoAndroid;
		}else if(nombreImagen.Contains("medalla_compartir")){
			imagepath = pathShareFinalJuegoAndroid;
		}

		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file:"+imagepath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		
		intentObject.Call<AndroidJavaObject>("setType", "text/plain");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), "" + text);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), "" );
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TITLE"), text );
		
		intentObject.Call<AndroidJavaObject>("setType", "image/*");
		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
		
		currentActivity.Call("startActivity", intentObject);

		#elif UNITY_EDITOR || UNITY_STANDALONE || STEAM

		string mailSubject = System.Uri.EscapeDataString ("");
		string mailRecipient = "";
		string body = System.Uri.EscapeDataString (text);
		
		string mailURL = "mailto:" + mailRecipient + "?subject=" + mailSubject + "&body=" + body;
		
		Application.OpenURL(mailURL);

		#endif
	}

    //MetodosComunes.instance.IvanovichGames();
    //Texture2D iconoPAVR = Resources.Load("IconoPAVR") as Texture2D;
    //string pathImagen = Application.persistentDataPath + "/IconoPAVR.png" ;

    //if(! System.IO.File.Exists(pathImagen))
    //{
    //    byte[] dataToSave = iconoPAVR.EncodeToPNG();
    //    System.IO.File.WriteAllBytes(pathImagen, dataToSave);
    //}

    //CompartirManager.Share(LanguageManager.instance.IdiomaKey("CompartirPAfinal"), pathImagen, "");
}
