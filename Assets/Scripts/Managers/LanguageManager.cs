﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class LanguageManager
{
    private static LanguageManager sharedinstance = null;

    public static LanguageManager instance
    {
        get
        {
            if (LanguageManager.sharedinstance == null)
            {
                sharedinstance = new LanguageManager();
                return sharedinstance;
            }
            else
            {
                return LanguageManager.sharedinstance;
            }
        }
    }

    // Lista de idiomas que queremos mostrar. No importa si no están en la documentación de Unity. Podemos poner cualquiera que nos de la gana siempre y cuando el nombre del archivo coincida con el de la lista
    public List<string> idiomas = new List<string>(){"English", "Spanish", "Catalan", "French", "German", "Chinese", "Italian", "Portuguese", "Japanese", "Korean", "Russian"};
    public Dictionary<string, string> Idioma = new Dictionary<string, string>();
	public string idiomaActual;
	
    public LanguageManager()
    {
        // Storeamos en la variable 'idiomaActual' el archivo de texto del lenguaje que recogemos del PlayerPrefs (si es que existe) o del idioma por defecto del dispositivo
        if (ComprobarSiExisteIdioma(Application.systemLanguage.ToString()))
            idiomaActual = (PlayerPrefs.HasKey("Language") ? PlayerPrefs.GetString("Language") : Application.systemLanguage.ToString());
        else
        {
            Debug.Log("El idioma del dispositivo no se encuentra en la lista. Estableciendo 'English' como idioma por defecto");
            idiomaActual = idiomas[0];  // Por defecto, el idioma es el "English" en caso que nuestro dispositivo no tenga un idioma de la lista
        }

        // Cargamos el idioma que nos devuelve la función 'LeerIdiomaDeArchivo'
        CargarIdioma(idiomaActual);
	}

    // Cargamos el idioma y lo parseamos en un diccionario
    private void CargarIdioma(string idiomaActual)
    {
        TextAsset textFile = (TextAsset)Resources.Load("Language/" + idiomaActual);
        StringReader str = new StringReader(textFile.text);

        Idioma.Clear();

        string line;
        while ((line = str.ReadLine()) != null)
        {
            if (line.Length > 0 && line[0] == '"')
            {
                line = line.Replace("\"", "");
                line = line.Replace(";", "");

                string[] d = line.Split("="[0]);
                d[0] = d[0].Replace(" ", "");
                d[1] = d[1].Substring(1, d[1].Length - 1);

                if (!Idioma.ContainsKey(d[0]))
                {
                    Idioma.Add(d[0], d[1]);
                }
            }
        }

        str.Close();
    }

    public string IdiomaKey(string key)
    {
        string str = "";

        if (Idioma.ContainsKey(key))            
            str = Idioma[key];
        else
            str = key;

        str = str.Replace("~", "\n");
        str = str.Replace("∫", "");

        return str;
    }

    public void CambiarIdioma()
    {
        // Buscamos el índice de nuestro idioma en la lista de idiomas
        int indexIdioma = idiomas.IndexOf(idiomaActual);

        // Asignamos el siguiente idioma de la lista (o el primero en caso que sea el último de la lista)
        if (indexIdioma == idiomas.Count - 1)
            idiomaActual = idiomas[0];
        else
            idiomaActual = idiomas[indexIdioma + 1];

        // Grabamos el nuevo idioma
        PlayerPrefs.SetString("Language", idiomaActual);

        // Cargamos el idioma que nos devuelve la función 'LeerIdiomaDeArchivo'
        CargarIdioma(idiomaActual);
    }


    // Hacemos una comprobación por si algún dispositivo no tiene ninguno de los idiomas de la lista
    private bool ComprobarSiExisteIdioma(string idioma)
    {
        bool idiomaExiste = false;

        for (int n = 0; n < idiomas.Count; n++)
        {
            if (idioma == idiomas[n])
            {
                idiomaExiste = true;
                break;
            }
            else
                idiomaExiste = false;
        }

        return idiomaExiste;
    }
}
