﻿using UnityEngine;
using System.Collections;
using System;

public class AudioManager : MonoBehaviour {

    public AudioSource[] audioSource;

    public static AudioManager instance { get; private set; }

    void Awake ()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void PlayAudio(int source, AudioClip clip, float volumen)
    {
        //audioSource[source].clip = clip;
        //audioSource[source].volume = volumen;
        audioSource[source].PlayOneShot(clip, volumen);
        //GetComponent<AudioSource>().PlayOneShot(clip, volumen);
    }

    public void CambiaPitch(int source, float valor)
    {
        audioSource[source].pitch = valor;
        //GetComponent<AudioSource>().pitch = valor;
    }

    public void Mute(int source, bool state)
    {
        audioSource[source].mute = state;
        //GetComponent<AudioSource>().mute = state;
    }
}
