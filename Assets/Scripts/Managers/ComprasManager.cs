﻿using UnityEngine;
using UnityEngine.Purchasing;
using System;

public class ComprasManager : IStoreListener {

    private const string IAP_FULLVERSION = "pavr_premium";

	private IStoreController controller = null;
	private IExtensionProvider extensions = null;

    public string precioFullVersion = "---";
	
	public Action<bool> bloqueCompra = null;
	public Action<bool> bloqueRestaurarCompras = null;

	private bool initOk = false;
    public bool hemosCompradoElJuego = false;

	private static ComprasManager sharedinstance = null;

	#region Public methods

	public void RestaurarCompras(Action<bool> bloqueRestaurarCompras){

		this.bloqueRestaurarCompras = bloqueRestaurarCompras;

		extensions.GetExtension<IAppleExtensions> ().RestoreTransactions (result => {
			if (result) {
				// This does not mean anything was restored,
				// merely that the restoration process succeeded.
				if(bloqueRestaurarCompras != null){
					bloqueRestaurarCompras(true);
					bloqueRestaurarCompras = null;
				}
			} else {
				// Restoration failed.
				if(bloqueRestaurarCompras != null){
					bloqueRestaurarCompras(false);
					bloqueRestaurarCompras = null;
				}
			}
		});
	}

    public void CompraGenerica()
    {
        ComprarFullVersion(success =>
        {
            if (success)
            {
                Debug.Log("COMPRA REALIZADA CON ÉXITO");

                //GameObject panelFinCompra = Instantiate(Resources.Load("Prefabs/PanelFinCompras" + (Niveles.instance.tenemosCardboardGlasses ? "VR" : "Normal")), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
                //panelFinCompra.GetComponent<PanelFinCompra>().descripcion.text = "Your payment has been made successfully.\n\nThank you for your purchase.";
                //panelFinCompra.GetComponent<PanelFinCompra>().boton.text = "Enjoy"; 
            }
            else
            {
                Debug.Log("ERROR EN LA COMPRA");

                //GameObject panelFinCompra = Instantiate(Resources.Load("Prefabs/PanelFinCompras" + (Niveles.instance.tenemosCardboardGlasses ? "VR" : "Normal")), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
                //panelFinCompra.GetComponent<PanelFinCompra>().descripcion.text = "Unfortunately, it has not been possible to get your payment.";
                //panelFinCompra.GetComponent<PanelFinCompra>().boton.text = "Accpet"; 
            }
        });
    }

	public void CompraGenericaDesDeVideo()
    {
        ComprarFullVersion(success =>
        {
            if (success)
            {
                Debug.Log("COMPRA REALIZADA CON ÉXITO");

				MetodosComunes.instance.PerfectAngle();

                //GameObject panelFinCompra = Instantiate(Resources.Load("Prefabs/PanelFinCompras" + (Niveles.instance.tenemosCardboardGlasses ? "VR" : "Normal")), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
                //panelFinCompra.GetComponent<PanelFinCompra>().descripcion.text = "Your payment has been made successfully.\n\nThank you for your purchase.";
                //panelFinCompra.GetComponent<PanelFinCompra>().boton.text = "Enjoy"; 
            }
            else
            {
                Debug.Log("ERROR EN LA COMPRA");

                //GameObject panelFinCompra = Instantiate(Resources.Load("Prefabs/PanelFinCompras" + (Niveles.instance.tenemosCardboardGlasses ? "VR" : "Normal")), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
                //panelFinCompra.GetComponent<PanelFinCompra>().descripcion.text = "Unfortunately, it has not been possible to get your payment.";
                //panelFinCompra.GetComponent<PanelFinCompra>().boton.text = "Accpet"; 
            }
        });
    }

    public void Restaurar()
    {
        RestaurarCompras(success =>
        {
            if (success) {
                Debug.Log("COMPRA RESTURADA CON ÉXITO");
            }
            else {
                Debug.Log("ERROR EN LA RESTAURACIÓN");
            }

            Niveles.instance.puedoEnviarAlgunEvento = true;
        });
    }

    public void ComprarFullVersion(Action<bool> bloqueCompra)
    {

        if (!initOk)
        {
            bloqueCompra(false);
            return;
        }

        this.bloqueCompra = bloqueCompra;

        controller.InitiatePurchase(IAP_FULLVERSION);
    }

	#endregion

	#region LifeCycle

	public static ComprasManager instance {
		
		get {
			if (ComprasManager.sharedinstance == null) {
				sharedinstance = new ComprasManager ();
				return sharedinstance;
			} else {
				return ComprasManager.sharedinstance;
			}
		}
	}			

	public ComprasManager () {

		var module = StandardPurchasingModule.Instance();
		
		var builder = ConfigurationBuilder.Instance(module);
	
        //builder.Configure<IGooglePlayConfiguration>().SetPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjUAUvrqaAqCpvmo/PtckHYL5q6ni6r8z89Hfizag7WfV29DYri5kmDQgngjzVJbeZ2Y2Q9qDK1WT6IgGnHpvbrpDT3i/pFaSCHaM5rso017O+OnH/k4gPGkM/zULWPiaSURygXFJe4EdJ72LL7jxAs9fY2TVq94LGpQBc2XLI480m/cn5mRRqlN7wzxrkUrDvxHrg3pmU5w6QGhB9y7KuZQaJqEHNx/oPgO5QdxJzCOzLqBmwwNXDjqPOHJ3a7cAdFyo9L/5aQNF3YcmtRnpZwOtZtGvWxSDAOoxSIoBOMwtz0MaQqxsvtC9RC0w/fqSqd2V6Vh8GqMG2sFKcoBN9QIDAQAB");

        builder.AddProduct(IAP_FULLVERSION, ProductType.NonConsumable, new IDs
        {
            {IAP_FULLVERSION, GooglePlay.Name},
            {IAP_FULLVERSION, AppleAppStore.Name},
            {IAP_FULLVERSION, MacAppStore.Name}
        });

		UnityPurchasing.Initialize (this, builder);
	}
	#endregion

	#region IStoreListener methods
	/// <summary>
	/// Called when Unity IAP is ready to make purchases.
	/// </summary>
	public void OnInitialized (IStoreController controller, IExtensionProvider extensions)
	{
		this.controller = controller;
		this.extensions = extensions;

		foreach (var product in controller.products.all) {
            precioFullVersion = product.metadata.localizedPriceString;
		}

		initOk = true;
	}
	
	/// <summary>
	/// Called when Unity IAP encounters an unrecoverable initialization error.
	///
	/// Note that this will not be called if Internet is unavailable; Unity IAP
	/// will attempt initialization until it becomes available.
	/// </summary>
	public void OnInitializeFailed (InitializationFailureReason error)
	{
		initOk = false;
	}
	
	/// <summary>
	/// Called when a purchase completes.
	///
	/// May be called at any time after OnInitialized().
	/// </summary>
	public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs e)
	{
        if (e.purchasedProduct.definition.id == IAP_FULLVERSION)
        {
            hemosCompradoElJuego = true;
            PlayerPrefs.SetInt("HemosCompradoElJuego", 1);
        }

		if (bloqueCompra != null) {
			bloqueCompra(true);
			bloqueCompra = null;
		}

		return PurchaseProcessingResult.Complete;
	}
	
	/// <summary>
	/// Called when a purchase fails.
	/// </summary>
	public void OnPurchaseFailed (Product i, PurchaseFailureReason p)
	{

		if (bloqueCompra != null) {
			bloqueCompra(false);
			bloqueCompra = null;
		}

		if (p == PurchaseFailureReason.PurchasingUnavailable) {
			// IAP may be disabled in device settings.
		}
	}
	#endregion
}
