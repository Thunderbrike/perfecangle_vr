﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System;

#if UNITY_IOS || UNITY_ANDROID
using GoogleMobileAds;
using GoogleMobileAds.Api;

using ChartboostSDK;
using TapjoyUnity;
//using VungleSDKProxy;
#endif

//UNITYADS
#if UNITY_IOS || UNITY_ANDROID
using UnityEngine.Advertisements;
#endif

#if UNITY_IOS 
//using ADBannerView = UnityEngine.iOS.ADBannerView;
#endif

// VERSIÓN AMAZON NO TIENE VUNGLE - DEMASIADOS METODOS!!!!

/*
--------------------
	Chartboost
---------------------
		
		iOS Chartboost App ID
		568a291e2fdf342d2240518e
		
		iOS Chartboost App Signature
		3ab9bf93cfd4abd78af121757cdb009ea51ada98
		
		Android Chartboost App ID
		568a2a7d8838092b5e329d81
		
		Android Chartboost App Signature
		6a352dc787be61a719a423d7936e921f1eb069d0
		
——————————————————
Tapjoy
——————————————————

Tapjoy iOS AppID:
b3c10995-3fa2-40de-b76e-bee03c9e7c4e

Tapjoy iOS SDK key:
s8EJlT-iQN63br7gPJ58TgEB924wpKHh1b1EdBLg5FyeyNnjj2-TcmPKwrPK

Tapjoy Android AppID:
c52feb33-9f64-4d9d-90d2-8098ec13d103

Tapjoy Android SDK key:
xS_rM59kTZ2Q0oCY7BPRAwECDmW3GkuBH6Dj8KvC9zut4tzzxLINI_HCYCvm

*/

public class AdsManager : MonoBehaviour { 
	static public AdsManager AdsManagerScript;

    //#if UNITY_TVOS
    //void Awake()
    //{
    //    Destroy(gameObject);
    //}
    //#endif

	#if !UNITY_STANDALONE && !STEAM && !UNITY_TVOS
	bool accionDeVerUnvideoTerminado = false ;
	bool comoHaTerminadoElVideo = false ;
	
	public Text deb; 
		
	//AdMob
	private BannerView bannerView;
	static public InterstitialAd interstitial ;
	bool bannerRequested=false;
	
	static public List<string> Adslist = new List<string> ();
	static public List<string> VAdslist = new List<string> ();
	int Video_Ads_contador = -1;
	int Intertitial_Ads_contador = -1;
	static public int porcentajeAnuncios=50;
	
	
	//VUNGLE
//	string vungleAppIDiOS_MD="568a2c11063b1a3833000013";
//	string vungleAppIDAndroid_MD="568a2c7d5fe556c23900000d";

	
	private static string ENABLE_LOGGING_IOS = "TJC_OPTION_ENABLE_LOGGING";
	private static string ENABLE_LOGGING_ANDROID = "enable_logging";
	string tapPointsLabel = "";
	bool viewIsShowing = false;
	bool shouldTransition = false;
	public bool isConnected = false;
	public bool contentIsReadyForPlacement = false;
	public TJPlacement directPlayPlacement;
	public TJPlacement samplePlacement;
	
	public string output = "";
	public bool shouldPreload = false;
	
	//TAPJOY Placement
	public string samplePlacementName = "PerfectAngle Video Android";
	
	string tapjoyDirectPlayVideoNameAndroid="PerfectAngle Video Android";
	string tapjoyDirectPlayVideoNameiOS="PerfectAngle Video iOS";
	
	//-----------------------------------------------------------------------------------------------
	
	
	
	
	//------------------------------------- UNITYADS ------------------------------------
	
	string iosGameID = "96394";
	string androidGameID = "96397";
	
	string UnityAdsIosZoneId = "rewardedVideoZone";
	string UnityAdsAndroidZoneId = "rewardedVideoZone";

	
	public bool enableTestMode = false;
	public bool showInfoLogs;
	public bool showDebugLogs;
	public bool showWarningLogs = true;
	public bool showErrorLogs = true;
	
	private static Action _handleFinished;
	private static Action _handleSkipped;
	private static Action _handleFailed;
	private static Action _onContinue;
	//-----------------------------------------------------------------------------------

	//AdColony ---------------------------------------------------------------------------------
	
	bool adcolonyVideoPreparado= false;
	public string zoneString = "";
	public string adcZoneID = "";
	
	//public Dictionary<string, ADCVideoZone> videoZones = new Dictionary<string, ADCVideoZone>();
	#if UNITY_IPHONE
#endif
	static public string IosAppId = "app49a59b24b146446899";
	static public string IosZoneId = "vzf409c8c0867d4dada4";
	static public string AndroidAppId = "appdeabeeb6e50d46fd9a";
	static public string AndroidZoneId = "vzdfda9bd6f6304613af";
	

	//------------------------------------------------------------------------------------------------

	/// <summary>
	/// ChartBoost vars
	/// </summary>
	private bool hasInterstitial = false;
	private bool hasMoreApps = false;
	private bool hasRewardedVideo = false;
	private bool hasInPlay = false;
	private int frameCount = 0;
	
	private bool ageGate = false;
	private bool autocache = true;
	private bool activeAgeGate = false;
	private bool showInterstitial = true;
	private bool showMoreApps = true;
	private bool showRewardedVideo = true;
	private int BANNER_HEIGHT = 110;
	private int REQUIRED_HEIGHT = 650;
	private int ELEMENT_WIDTH = 190;
	private Rect scrollRect;
	private Rect scrollArea;
	private Vector3 guiScale;
	private float scale;
	/// <summary>
	/// End ChartBoost vars
	/// </summary>
	
	//--------------------------  Fyber Public Variables
	
	#if UNITY_ANDROID && !UNITY_EDITOR
	static public string appId = "39895"; 
	static public string securityToken = "8dfde6c179e7a214136f9cc783024ef6";    
	#else
	static public string appId = "39893"; 
	static public string securityToken = "c7c05f68f7afe235bf38f2a0c02ebe09";
	#endif
	public string userId = "";
	public string customCurrencyName = "";
	public string placementId = "1";
	public string currencyId = "";
	
	#if UNITY_IPHONE
//	SponsorPay.SponsorPayPlugin sponsorPayPlugin;
	#endif
	
	string mbeOffersStatus = "NO";
	string interstitialAdStatus = "No ads available";
	
	string pauseDownloadsString = "Pause Video Downloads";
	string credentialsToken = "";
	
	bool mbeVCSshowNotification = false;
	bool overrideCredentials = false;
	
	//----------------------------------------------------
	
	
	
	static public int Video_position;
	
	static public string Visto_desde = "";
	
	
	static public bool empiezoCorutina;
	static public bool hemosVistoVideo;
	
	static public string Debuc="";
	
	static public bool Inicializado = false ;
	
	static public bool tengoAnuncioPendienteDeValidarPorPatrocinio = false ;
	static public bool tengoVideoPendienteDeValidar = false ;
	
	bool estoyMostrandoUnVideo = false ;
	
	float timerVideoOut=0.0f;
	
	//------------------------- VUNGLE ------------------------------------------
	bool vungleAvailableCached = false;
	//---------------------------------------------------------------------------
	
	//------------------------- AppLovin ------------------------------------------
	bool applovinCached = false;
	//---------------------------------------------------------------------------
	
	//Banner IAD
	#if UNITY_IOS 
//	private ADBannerView banner = null;
	#endif
	
	void Awake()
	{	
	
		
		DontDestroyOnLoad(transform.gameObject);
		
		AdsManagerScript=this;

		//TAPJOY ID
		#if UNITY_IPHONE
		samplePlacementName = tapjoyDirectPlayVideoNameiOS ;
		#elif UNITY_ANDROID
		samplePlacementName =  tapjoyDirectPlayVideoNameAndroid ;
		#endif

		
	}
	
	
	public void Inicializar(){
//				Debug.Log("############# Inicializar Videos #################");
		Inicializado = true ;
		
		#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
		
		Initialize_Tapjoy();
		Initialize_Charboost();
		Initialize_UnityAds();
//		Initialize_Vungle();
		Initialize_AdColony ();
		Initialize_AppLovin();
		//Desactivamos Fyber
		//Initialize_Fyber();
		
		RequestInterstitial();
//		RequestBanner();

		#endif
		
	}

	
	void Start () {
		

		
		//Chartboost cache
		Chartboost.setShouldPauseClickForConfirmation(ageGate);
		Chartboost.setAutoCacheAds(autocache);
		
		#if ( (UNITY_ANDROID || UNITY_IPHONE) ) && !UNITY_EDITOR
		//FYBER
		
		/*Desactivamos arranque de SP que aparentemente es inestable
		
		print ("SponsorPaySDKDemo's Start invoked");
		
		// get a hold of the SponsorPay plugin instance (SponsorPayPluginMonoBehaviour must be attached to a scene object)
		
		
		sponsorPayPlugin = SponsorPayPluginMonoBehaviour.PluginInstance;		
		sponsorPayPlugin.EnableLogging(true);
		sponsorPayPlugin.SetLogLevel(SPLogLevel.Debug);
		
		
		// Register delegates to be notified of the result of the "get new coins" request
//		sponsorPayPlugin.OnSuccessfulCurrencyRequestReceived += new SponsorPay.SuccessfulCurrencyResponseReceivedHandler(OnSuccessfulCurrencyRequestReceived);
//		sponsorPayPlugin.OnDeltaOfCoinsRequestFailed += new SponsorPay.ErrorHandler(OnSPDeltaOfCoinsRequestFailed);
		
		// Register delegates to be notified of the result of a BrandEngage request
		sponsorPayPlugin.OnBrandEngageRequestResponseReceived += new SponsorPay.BrandEngageRequestResponseReceivedHandler(OnSPBrandEngageResponseReceived);
		sponsorPayPlugin.OnBrandEngageRequestErrorReceived += new SponsorPay.BrandEngageRequestErrorReceivedHandler(OnSPBrandEngageErrorReceived);
		
		// Register delegates to be notified when a native exception occurs on the plugin
		sponsorPayPlugin.OnNativeExceptionReceived += new SponsorPay.NativeExceptionHandler(OnNativeExceptionReceivedFromSDK);
		sponsorPayPlugin.OnOfferWallResultReceived += new SponsorPay.OfferWallResultHandler(OnOFWResultReceived);
		sponsorPayPlugin.OnBrandEngageResultReceived += new SponsorPay.BrandEngageResultHandler(OnMBEResultReceived);
		
		//Interstitial delegates
//		sponsorPayPlugin.OnInterstitialRequestResponseReceived += new SponsorPay.InterstitialRequestResponseReceivedHandler(OnSPInterstitialResponseReceived);
//		sponsorPayPlugin.OnInterstitialRequestErrorReceived += new SponsorPay.InterstitialRequestErrorReceivedHandler(OnSPInterstitialErrorReceived);
//		
//		sponsorPayPlugin.OnInterstitialStatusCloseReceived += new SponsorPay.InterstitialStatusCloseHandler(OnSPInterstitialStatusCloseReceived);
//		sponsorPayPlugin.OnInterstitialStatusErrorReceived += new SponsorPay.InterstitialStatusErrorHandler(OnSPInterstitialStatusErrorReceived);
//		
//		Dictionary<string, string> dictionary = new Dictionary<string, string>();
//		dictionary.Add("additional_param", "WOOT");
//		dictionary.Add("another_one", "ImHERE");
//		
//		sponsorPayPlugin.AddParameters(dictionary);

		*/
		
		#endif

		#if UNITY_IPHONE
//		banner = new ADBannerView(ADBannerView.Type.Banner, ADBannerView.Layout.BottomCenter);
//		ADBannerView.onBannerWasClicked += OnBannerClicked;
//		ADBannerView.onBannerWasLoaded += OnBannerLoaded;
//		ADBannerView.onBannerFailedToLoad += OnBannerFailedToLoad;
		#endif


		if (!Tapjoy.IsConnected) {
		
			Tapjoy.Connect();			
		}	
		if( !Inicializado ) Inicializar();



	}
	
	public void initvideo(string name){
		VAdslist.Clear();
		VAdslist.Add (name);
	}
	
	void Update ()
	{
		
//		if( estoyMostrandoUnVideo ){
//			timerVideoOut +=Time.unscaledDeltaTime;
//			if( timerVideoOut>40.0f ){
//				estoyMostrandoUnVideo = false ;
//				accionDeVerUnvideoTerminado = true ;
//			}
//		}
		
	}
	
	
	
	#if (UNITY_ANDROID || UNITY_IPHONE)
	
	void OnEnable()
	{
	
//		Debug.Log("C#: Enabling Tapjoy Delegates");
		// Tapjoy Connect Delegates
		Tapjoy.OnConnectSuccess += HandleConnectSuccess;
		Tapjoy.OnConnectFailure += HandleConnectFailure;
		
		// Tapjoy View Delegates
//		Tapjoy.OnViewWillOpen += HandleViewWillOpen;
//		Tapjoy.OnViewDidOpen += HandleViewDidOpen;
//		Tapjoy.OnViewWillClose += HandleViewWillClose;
//		Tapjoy.OnViewDidClose += HandleViewDidClose;
		
		// Placement Delegates
		TJPlacement.OnRequestSuccess += HandlePlacementRequestSuccess;
		TJPlacement.OnRequestFailure += HandlePlacementRequestFailure;
		TJPlacement.OnContentReady += HandlePlacementContentReady;
		TJPlacement.OnContentShow += HandlePlacementContentShow;
		TJPlacement.OnContentDismiss += HandlePlacementContentDismiss;
		TJPlacement.OnPurchaseRequest += HandleOnPurchaseRequest;
		TJPlacement.OnRewardRequest += HandleOnRewardRequest;
		
		Tapjoy.OnVideoStart += HandleVideoStart;
		Tapjoy.OnVideoError += HandleVideoError;
		Tapjoy.OnVideoComplete += HandleVideoComplete;
		
//		//TAPJOY PLACEMENT
		if (directPlayPlacement == null) {
			directPlayPlacement = TJPlacement.CreatePlacement(samplePlacementName);
			if (directPlayPlacement != null) {
				directPlayPlacement.RequestContent();
			}
		}
		
		//ChartBoost delegates
		SetupDelegates();
		
		//------------------------- VUNGLE ------------------
//		Vungle.onAdStartedEvent += onAdStartedEvent;
//		Vungle.onAdEndedEvent += onAdEndedEvent;
//		Vungle.onAdViewedEvent += onAdViewedEvent;
//		Vungle.onCachedAdAvailableEvent += onCachedAdAvailableEvent;
		
	}
	
	void OnDisable()
	{
//		Debug.Log("C#: Disabling and removing Tapjoy Delegates");
		// Connect Delegates
		Tapjoy.OnConnectSuccess -= HandleConnectSuccess;
		Tapjoy.OnConnectFailure -= HandleConnectFailure;
		
		// Tapjoy View Delegates
//		Tapjoy.OnViewWillOpen -= HandleViewWillOpen;
//		Tapjoy.OnViewDidOpen -= HandleViewDidOpen;
//		Tapjoy.OnViewWillClose -= HandleViewWillClose;
//		Tapjoy.OnViewDidClose -= HandleViewDidClose;
		
		
		// Offers Delegates
//		Tapjoy.OnOffersResponse -= HandleShowOffers;
//		Tapjoy.OnOffersResponseFailure -= HandleShowOffersFailure;
		
		// Placement delegates
		TJPlacement.OnRequestSuccess -= HandlePlacementRequestSuccess;
		TJPlacement.OnRequestFailure -= HandlePlacementRequestFailure;
		TJPlacement.OnContentReady -= HandlePlacementContentReady;
		TJPlacement.OnContentShow -= HandlePlacementContentShow;
		TJPlacement.OnContentDismiss -= HandlePlacementContentDismiss;
		TJPlacement.OnPurchaseRequest -= HandleOnPurchaseRequest;
		TJPlacement.OnRewardRequest -= HandleOnRewardRequest;
		
		// Currency Delegates
		Tapjoy.OnAwardCurrencyResponse -= HandleAwardCurrencyResponse;
		Tapjoy.OnAwardCurrencyResponseFailure -= HandleAwardCurrencyResponseFailure;
		Tapjoy.OnSpendCurrencyResponse -= HandleSpendCurrencyResponse;
		Tapjoy.OnSpendCurrencyResponseFailure -= HandleSpendCurrencyResponseFailure;
		Tapjoy.OnGetCurrencyBalanceResponse -= HandleGetCurrencyBalanceResponse;
		Tapjoy.OnGetCurrencyBalanceResponseFailure -= HandleGetCurrencyBalanceResponseFailure;
		Tapjoy.OnEarnedCurrency -= HandleEarnedCurrency;
		
		// Tapjoy Video Delegates
		Tapjoy.OnVideoStart -= HandleVideoStart;
		Tapjoy.OnVideoError -= HandleVideoError;
		Tapjoy.OnVideoComplete -= HandleVideoComplete;
		
		
//		Vungle.onAdStartedEvent -= onAdStartedEvent;
//		Vungle.onAdEndedEvent -= onAdEndedEvent;
//		Vungle.onAdViewedEvent -= onAdViewedEvent;
//		Vungle.onCachedAdAvailableEvent -= onCachedAdAvailableEvent;
	}
	
	void SetupDelegates()
	{
		// Listen to all impression-related events
		Chartboost.didFailToLoadInterstitial += didFailToLoadInterstitial;
		Chartboost.didDismissInterstitial += didDismissInterstitial;
		Chartboost.didCloseInterstitial += didCloseInterstitial;
		Chartboost.didClickInterstitial += didClickInterstitial;
		Chartboost.didCacheInterstitial += didCacheInterstitial;
		Chartboost.shouldDisplayInterstitial += shouldDisplayInterstitial;
		Chartboost.didDisplayInterstitial += didDisplayInterstitial;
		Chartboost.didFailToLoadMoreApps += didFailToLoadMoreApps;
		Chartboost.didDismissMoreApps += didDismissMoreApps;
		Chartboost.didCloseMoreApps += didCloseMoreApps;
		Chartboost.didClickMoreApps += didClickMoreApps;
		Chartboost.didCacheMoreApps += didCacheMoreApps;
		Chartboost.shouldDisplayMoreApps += shouldDisplayMoreApps;
		Chartboost.didDisplayMoreApps += didDisplayMoreApps;
		Chartboost.didFailToRecordClick += didFailToRecordClick;
		Chartboost.didFailToLoadRewardedVideo += didFailToLoadRewardedVideo;
		Chartboost.didDismissRewardedVideo += didDismissRewardedVideo;
		Chartboost.didCloseRewardedVideo += didCloseRewardedVideo;
		Chartboost.didClickRewardedVideo += didClickRewardedVideo;
		Chartboost.didCacheRewardedVideo += didCacheRewardedVideo;
		Chartboost.shouldDisplayRewardedVideo += shouldDisplayRewardedVideo;
		Chartboost.didCompleteRewardedVideo += didCompleteRewardedVideo;
		Chartboost.didDisplayRewardedVideo += didDisplayRewardedVideo;
		Chartboost.didCacheInPlay += didCacheInPlay;
		Chartboost.didFailToLoadInPlay += didFailToLoadInPlay;
		Chartboost.didPauseClickForConfirmation += didPauseClickForConfirmation;
		Chartboost.willDisplayVideo += willDisplayVideo;
		#if UNITY_IPHONE
		//Chartboost.didCompleteAppStoreSheetFlow += didCompleteAppStoreSheetFlow;
		#endif
	}
	
	public void Initialize_Fyber()
	{
//		string token = sponsorPayPlugin.Start (appId, null, securityToken);
//		credentialsToken = string.IsNullOrEmpty (token) ? "" : token;
	}
	

	public void Initialize_AppLovin(){
		
		AppLovin.SetSdkKey("DHW6UpbwPJK5-X8nfW9DpOb7_LrIxBK8SsFyfrt39B97kHvNo2abQBkQWTiSk1bBtr7Wg_aqBkyv0rHptsOI27");
		AppLovin.SetUnityAdListener(gameObject.name);
		AppLovin.InitializeSdk();
		
	}

	public void Initialize_Vungle(){

//		Vungle.init( vungleAppIDAndroid_MD, vungleAppIDiOS_MD );
		
		
		//		Vungle.init( "com.minidrivers.formula1.com", "873538439" );
	}
	
	public void Initialize_Charboost(){
		Chartboost.cacheRewardedVideo( CBLocation.Default );
		Chartboost.cacheInterstitial( CBLocation.Default );
	}
	
	
	public void Initialize_Tapjoy(){
		#if UNITY_ANDROID
		if (Application.platform == RuntimePlatform.Android)
			UnityEngine.AndroidJNI.AttachCurrentThread();
		#endif
		
		if (!Tapjoy.IsConnected) {
			Debug.Log("Inicializando TAPJOY , Conectando.......................................................");
			Tapjoy.Connect();
		}else{
			Debug.Log("Ya estoy conectado a TAPJOY .......................................................");
		}
	}
	
	//UNITYADS
	public void Initialize_UnityAds (){
		Debug.Log("Running precheck for Unity Ads initialization...");
		
		string gameID = null;
		

			#if UNITY_IOS
			gameID = iosGameID;
			#elif UNITY_ANDROID
			gameID = androidGameID;
			#endif
		
		
		if (!Advertisement.isSupported)
		{
			Debug.Log("Unity Ads is not supported on the current runtime platform.");
		}
		else if (Advertisement.isInitialized)
		{
			Debug.Log("Unity Ads is already initialized.");
		}
		else if (string.IsNullOrEmpty(gameID))
		{
			Debug.Log("The game ID value is not set. A valid game ID is required to initialize Unity Ads.");
		}
		else
		{
			Advertisement.debugLevel = Advertisement.DebugLevel.NONE;	
			if (showInfoLogs) Advertisement.debugLevel    |= Advertisement.DebugLevel.INFO;
			if (showDebugLogs) Advertisement.debugLevel   |= Advertisement.DebugLevel.DEBUG;
			if (showWarningLogs) Advertisement.debugLevel |= Advertisement.DebugLevel.WARNING;
			if (showErrorLogs) Advertisement.debugLevel   |= Advertisement.DebugLevel.ERROR;
			
			if (enableTestMode && !Debug.isDebugBuild)
			{
//				Debug.Log("Development Build must be enabled in Build Settings to enable test mode for Unity Ads.");
			}
			
			bool isTestModeEnabled = Debug.isDebugBuild && enableTestMode;
			Debug.Log(string.Format("Precheck done. Initializing Unity Ads for game ID {0} with test mode {1}...",
			                        gameID, isTestModeEnabled ? "enabled" : "disabled"));
			
			Advertisement.Initialize(gameID,false);
			
			StartCoroutine(LogWhenUnityAdsIsInitialized());
		}
		
	}
	private IEnumerator LogWhenUnityAdsIsInitialized ()
	{
		float initStartTime = Time.time;
		
		do yield return new WaitForSeconds(0.1f);
		while (!Advertisement.isInitialized);
		
//		Debug.Log(string.Format("Unity Ads was initialized in {0:F1} seconds.",Time.time - initStartTime));
		yield break;
	}
	

	//ADC
	public void Initialize_AdColony ()  //A partir de ahora FYBER
	{
		
		
//		Debug.Log("C#: Inicializo Adcolony");
		
		AdColony.OnVideoFinished = this.OnVideoFinished;
		AdColony.OnAdAvailabilityChange = OnAdAvailabilityChange;

		
		adcZoneID = "";
		#if UNITY_IPHONE
		adcZoneID = IosZoneId ;
		#elif UNITY_ANDROID
		adcZoneID = AndroidZoneId ;
		#endif		
		
		#if UNITY_IPHONE
		AdColony.Configure("1.0",IosAppId,adcZoneID);
		#elif UNITY_ANDROID
		AdColony.Configure("1.0",AndroidAppId,adcZoneID);
		#endif

	}
	
	
#if UNITY_IPHONE
#endif
	

	
	public IEnumerator PlayAVideo (string p , Action<bool> onComplete)
	{
	
		
		comoHaTerminadoElVideo=false;
		accionDeVerUnvideoTerminado = false ;

		Debug.Log ("Play VIDEO estoyMostrandoUnVideo ------------------------->>>>>>>> "+estoyMostrandoUnVideo);
		if( estoyMostrandoUnVideo ) yield return 0;
		estoyMostrandoUnVideo = true;
		
		timerVideoOut = 0.0f;
		
		tengoVideoPendienteDeValidar = true;
		
		Debug.Log ("Play VIDEO con Prioridad ------------------------->>>>>>>>");


//		if(StartGame.A_pasado_revision_def_FALSE && StartGame.dejar_ver_videos){
			
			Visto_desde = p ;
			
			Video_Ads_contador = -1;
			string ActualAds = VAdslist [0];
			
		Debug.Log (" Video prioridad "+ActualAds);
			
			if (ActualAds == "adcolony") {
				Debug.Log ("Play Adcolony ------------------------->>>>>>>>");
				AdcolonyPlayAVideo ();
			} else if (ActualAds == "tapjoy") {
				Debug.Log ("Play Tapjoy ------------------------->>>>>>>>");
				TapjoyPlayAVideo ();
			} else if (ActualAds == "chartboost") {
				Debug.Log ("Play CharBoost ------------------------->>>>>>>>");
				CharboostPlayAVideo();
			} else if (ActualAds == "unityads") {
				Debug.Log ("Play UnityAds ------------------------->>>>>>>>");
				UnityAdsPlayAVideo();
//			} 
//		else if (ActualAds == "vungle") {
//				Debug.Log ("Play Vungle ------------------------->>>>>>>>");
//				VunglePlayAVideo();
			} else if (ActualAds == "applovin") {
				Debug.Log ("Play AppLovin ------------------------->>>>>>>>");
				ApplovinPlayAVideo();
			} else if (ActualAds == "fyber"){
				Debug.Log ("Play fyber ------------------------->>>>>>>>");
				FyberPlayAVideo();
			}else{
				No_HayningunVideo();
			}
		

		while( !accionDeVerUnvideoTerminado ){
			yield return null;
		}
		Debug.Log (" onComplete ------------------------->>>>>>>> "+comoHaTerminadoElVideo);
		onComplete(comoHaTerminadoElVideo);
		
	}
	
	public void SiguientePlayAVideo ()
	{
		
		Video_Ads_contador++;
		
		if (Video_Ads_contador > VAdslist.Count - 1) {
			//Ir a pantalla compra*/
			No_HayningunVideo();
			return;
		}
		
		string ActualAds = VAdslist [Video_Ads_contador];
		
//		Debug.Log("Siguiente PLay Video : "+ActualAds);

	//if UNITY_IPHONE
		Debug.Log ("Siguiente Video prioridad "+ActualAds);

		if (ActualAds == "adcolony") {
			AdcolonyPlayAVideo ();
		} else if (ActualAds == "tapjoy") {
			TapjoyPlayAVideo ();
		} 

		else if (ActualAds == "chartboost") {
			CharboostPlayAVideo();
		}else if (ActualAds == "unityads") {
			UnityAdsPlayAVideo();
//		}else if (ActualAds == "vungle") {
//			VunglePlayAVideo();
		}else if (ActualAds == "applovin") {
			ApplovinPlayAVideo();
		}else if (ActualAds == "fyber") {
			FyberPlayAVideo();
		}

		
	}
	
	public void FyberPlayAVideo(){
		if (mbeOffersStatus == "YES") {
//			sponsorPayPlugin.StartBrandEngage();
			No_HayVideo ();
		}else{
			No_HayVideo ();
		}
	}
	
//	public void VunglePlayAVideo(){
//		if( vungleAvailableCached ){//if(  Vungle.isAdvertAvailable() ){
//			Debug.Log ("(Vungle) Play Vungle Video");
//			Vungle.playAd();	
//			vungleAvailableCached = false;
//		}else{
//			Debug.Log ("(Vungle) Video No Cacheado");
//			No_HayVideo();
//		}
//	}

	public void ApplovinPlayAVideo(){
		if( applovinCached ){
			Debug.Log ("(AppLovin) Play AppLovin Video");
			AppLovin.ShowRewardedInterstitial(); //ShowInterstitial();
			AppLovin.ShowInterstitial();
			applovinCached = false;
		}else{
			Debug.Log ("(AppLovin) Video No Cacheado");
			No_HayVideo ();
		}
	}

	public void AdcolonyPlayAVideo ()
	{
				//
//		if (AdColony.IsVideoAvailable (adcZoneID)) {  //adcZoneID
////			if( adcolonyVideoPreparado ){ //adcZoneID
//				Debug.Log ("(AdColony) Play AdColony Video");
//				AdColony.ShowVideoAd (adcZoneID); //adcZoneID
//				adcolonyVideoPreparado = false;
////			}else{
////				Debug.Log ("(AdColony) Video No Cacheado");
////				No_HayVideo ();
////			}

		if(AdColony.IsV4VCAvailable(adcZoneID)){
			AdColony.ShowV4VC( false, adcZoneID );
		} else {
			Debug.Log ("(AdColony) Video Not Available");
			No_HayVideo ();
		}
	}
#if UNITY_IPHONE
#endif
	public void No_HayVideo (){
		SiguientePlayAVideo ();
		Debug.Log ("No Hy videos nah");
	}
	
	public void TapjoyPlayAVideo ()
	{	
		//Camera.main.GetComponent<AudioListener>().enabled = false ;
		
		if (directPlayPlacement.IsContentAvailable())
		{
			if (directPlayPlacement.IsContentReady())
			{
//				Debug.Log( "(Tapjoy) ShowContent ");
				shouldTransition = true;
				directPlayPlacement.ShowContent();
			} 
			else
			{
//				Debug.Log( "(Tapjoy) Direct play video not ready to show.");
				No_HayVideo ();
			}
		}
		else
		{
//			Debug.Log( "(Tapjoy) No direct play video to show.");
			No_HayVideo ();
		}
		
	}
	
	public void CharboostPlayAVideo(){
		if( Chartboost.hasRewardedVideo( CBLocation.Default ) ){
			hemosVistoVideo = true;
			Debug.Log ("(Chartboost) Play Chartboost Video");
			Chartboost.showRewardedVideo( CBLocation.Default );
			Chartboost.cacheRewardedVideo( CBLocation.Default );
		}else{
			Debug.Log ("(Chartboost) Video No Cacheado");
			No_HayVideo ();
		}
		
	}
	
	
	public void UnityAdsPlayAVideo(){
		string unityadszoneID = "";
		#if UNITY_IPHONE
		unityadszoneID = UnityAdsIosZoneId ;
		#elif UNITY_ANDROID
		unityadszoneID = UnityAdsAndroidZoneId ;
		#endif
		if (Advertisement.isReady(unityadszoneID)){
			Debug.Log ("(UnityAds) Play UnityAds Video");
			ShowAd(unityadszoneID);
		}else{
			Debug.Log ("(UnityAds) Video No Cacheado");
			No_HayVideo();
		}
		
	}
	
	
	
	public void Dame_premio(){
		
		Debug.Log("################################################# DAME_PREMIO Visto_desde: " + Visto_desde);
		
		estoyMostrandoUnVideo = false;
		comoHaTerminadoElVideo = true ;
		accionDeVerUnvideoTerminado = true ;

		hemosVistoVideo = true;
//		GameManager.Reintentos = 2;

	}
	
	
	void No_HayningunVideo(){
		
		estoyMostrandoUnVideo = false;
		accionDeVerUnvideoTerminado = true ;

		Debug.Log ("####################### no hay ningun video");
		

		
	}
	
	
	public bool Hay_videos(){
		
		if( Application.isEditor ) return false; 

		if (Application.internetReachability == NetworkReachability.NotReachable) return false;
		
		bool hay = false;
		
		//TAPJOY
		if (VAdslist.Contains ("tapjoy")) {
//			Debug.Log("Pruebo con TAPJOY");
			
			if (!Tapjoy.IsConnected) {
//				Debug.Log("RE conectando a TAPJOY , Conectando.......................................................");
				Tapjoy.Connect();
			}else{
//				Debug.Log("Hay_VIDEOS ya esta conectado a TAPJOY .......................................................");
			}
			
			if (directPlayPlacement.IsContentReady()){
				
//				Debug.Log("tapjoy tiene video");
				
				return true;
				
			}else{ //no hay videos precacheo uno
				
//				Debug.Log("tapjoy NO tiene video");
				
				directPlayPlacement = TJPlacement.CreatePlacement( samplePlacementName);
				if (directPlayPlacement != null) {
					directPlayPlacement.RequestContent();
				}
				hay=false;
			}
		}
		
		//UNITYADS
		if (VAdslist.Contains ("unityads")) {
//			Debug.Log("Pruebo con UNITYADS");
			string UnityZoneID = "";
			#if UNITY_IPHONE
			UnityZoneID = UnityAdsIosZoneId ;
			#elif UNITY_ANDROID
			UnityZoneID = UnityAdsAndroidZoneId ;
			#endif
			
			if (Advertisement.isReady(UnityZoneID))
			{
			
//				Debug.Log("unityads tiene video");
			
				return true;
			}
			else 
			{
			
//				Debug.Log("unityads NO tiene video");
			
				hay=false;
			}
		}

		//APPLOVIN
		if (VAdslist.Contains ("applovin")) {
//			Debug.Log("Pruebo con APPLOVIN");
			if(  applovinCached ){
			
//				Debug.Log("applovin tiene video");
			
				return true;	
			}else{
			
//				Debug.Log("applovin NO tiene video");
			
				AppLovin.PreloadInterstitial();
				AppLovin.LoadRewardedInterstitial();
				hay=false;
			}
		}
#if UNITY_IPHONE
#endif
		
		//VUNGLE
//		if (VAdslist.Contains ("vungle")) {
////			Debug.Log("Pruebo con VUNGLE");
//			if( vungleAvailableCached ){//if(  Vungle. isAdvertAvailable() ){
//			
////				Debug.Log("vungle tiene video");
//			
//				return true;	
//			}else{
//			
////				Debug.Log("vungle NO tiene video");
//			
//				hay=false;
//			}
//		}
		

		////ADC
		if (VAdslist.Contains ("adcolony")) {
			
			Debug.Log("Pruebo con ADCOLONY");
//			if (AdColony.IsVideoAvailable (adcZoneID)) { //adcZoneID
			if ( AdColony.IsV4VCAvailable(adcZoneID) ){
//				if( adcolonyVideoPreparado ){
					Debug.Log("AdColony tiene video");
					return true;
//				}else{
//					Debug.Log("AdColony NO tiene video Cacheado");
//					hay=false;
//				}
			} else {
				Debug.Log("AdColony NO tiene video");
				hay=false;
			}
		}
		#if UNITY_IPHONE
#endif
		
		
		
		#if (UNITY_ANDROID || UNITY_IPHONE)
		
		//CHARTBOOST
		if (VAdslist.Contains ("chartboost")) {
//			Debug.Log("Pruebo con CHARTBOOST");
			if (Chartboost.hasRewardedVideo( CBLocation.Default )) {
//				Debug.Log("chartboost tiene video");
				return true;
			} else { //no hay videos precacheo uno
				
//				Debug.Log("chartboost NO tiene video");
			
				Chartboost.cacheRewardedVideo( CBLocation.Default );
				hay=false;
			}
		}
		
		//FYBER
		// Desactivamos Fyber porque no esta integrado correctamente
		/*
		if (VAdslist.Contains ("fyber")) {
//			Debug.Log("Pruebo con FYBER");
			if (mbeOffersStatus == "YES") {
				Debug.Log ("############################## hay fyber ###############################");
				return true;
			} else {
			
//				Debug.Log("fyber NO tiene video");
				requestMBEOffers ();
				hay = false;
			}
		}
		*/
		#endif
		
		
		
		return hay;
	}


	
	#if (UNITY_ANDROID || UNITY_IPHONE)
	//###################################################### Intertitials ###########################################################
	
	public bool Hay_Intertitials(){
		//CHARTBOOST intertistials
		bool hay=false;
		if( Chartboost.hasInterstitial( CBLocation.Default ) ){
			return true;
		}else{
			Chartboost.cacheInterstitial(CBLocation.Default);
		}


		//ADMOB intertistials
		if (interstitial.IsLoaded()){
			return true;
		}else{
			RequestInterstitial();
		}

		return hay;
	}
	
	public void PlayAIntertitial(){

		if (Niveles.instance.modoJuego != Niveles.ModoJuego.Normal)
			return;

		int rnd = UnityEngine.Random.Range(0,100);
		if(rnd>=porcentajeAnuncios) return;

		if( !Hay_Intertitials() ) return;

//		Debug.Log("Play Interstitial");
		Intertitial_Ads_contador = -1;
		string ActualAds = Adslist[0];
		if (ActualAds == "chartboost") {
			Debug.Log ("Play chartboost ------------------------->>>>>>>>");
			PlayChartBoos_inter();
		} 
	
		else if (ActualAds == "admob") {
			Debug.Log ("Play admob ------------------------->>>>>>>>");
			ShowInterstitial();
		}else{
			
		}

	}
	
	public void SiguientePlayAIntertitial ()
	{
//		Debug.Log("Siguiente Interstitial");
		Intertitial_Ads_contador++;
		
		if (Intertitial_Ads_contador > Adslist.Count - 1) {
			return;
		}
		
		string ActualAds = Adslist [Intertitial_Ads_contador];
		
//		Debug.Log("Siguiente Intertitials : "+ActualAds);
		
		if (ActualAds == "chartboost") {
			PlayChartBoos_inter();
		}

		else if (ActualAds == "admob") {
			ShowInterstitial();
		} 

	}
	
	
	public void PlayChartBoos_inter(){
//		if(GameManager.No_Publicidad)return;
//		Debug.Log("Entro en chartboost Interstitial");
		if( Chartboost.hasInterstitial( CBLocation.Default ) ){
			
			Chartboost.showInterstitial( CBLocation.Default );
//			Debug.Log("play en chartboost Interstitial");
			Chartboost.cacheInterstitial(CBLocation.Default );
		}else{
//			Debug.Log("No hay chartboost Interstitial");
			SiguientePlayAIntertitial ();
		}
	}
	
	
	public void PlayChartBoostInterOnly(){
		if( Chartboost.hasInterstitial( CBLocation.Default ) ){
			
			Chartboost.showInterstitial( CBLocation.Default );
//			Debug.Log("play en chartboost Interstitial only");
			Chartboost.cacheInterstitial( CBLocation.Default );
		}
	}
	
	public bool Hay_CB_intertitial(){
		if( Chartboost.hasInterstitial(CBLocation.Default) ){
			return true;
		}else{
			Chartboost.cacheInterstitial(CBLocation.Default);
			return false;
		}
		
		
		return false;
	}
	
	//----------------------------------------         AdMob -----------------------------------------------------------------------------------------
	public void RequestInterstitial()
	{

		string adUnitId = "unused";
		
		#if UNITY_EDITOR
		adUnitId = "unused";
		#elif UNITY_ANDROID
		adUnitId = "ca-app-pub-1646902843099814/4089797887";
		#elif UNITY_IPHONE
		adUnitId = "ca-app-pub-1646902843099814/8659598284";
		#else
		adUnitId = "unexpected_platform";
		#endif
		



		// Create an interstitial. ADMOB
		
		interstitial = new InterstitialAd(adUnitId);
		// Register for ad events.
		interstitial.AdLoaded += HandleInterstitialLoaded;
		interstitial.AdFailedToLoad += HandleInterstitialFailedToLoad;
		interstitial.AdOpened += HandleInterstitialOpened;
		interstitial.AdClosing += HandleInterstitialClosing;
		interstitial.AdClosed += HandleInterstitialClosed;
		interstitial.AdLeftApplication += HandleInterstitialLeftApplication;
	
		// Load an interstitial ad.
		interstitial.LoadAd(createAdRequest());
		
	}
	
	private void RequestBanner()
	{
//		Debug.Log("####### RequestBanner");
		#if UNITY_EDITOR
		string adUnitId = "unused";
		#elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-1646902843099814/2613064688";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-1646902843099814/7182865081";
		#else
		string adUnitId = "unexpected_platform";
		#endif
		
		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);
		// Register for ad events.
		bannerView.AdLoaded += HandleAdLoaded;
		bannerView.AdFailedToLoad += HandleAdFailedToLoad;
		bannerView.AdOpened += HandleAdOpened;
		bannerView.AdClosing += HandleAdClosing;
		bannerView.AdClosed += HandleAdClosed;
		bannerView.AdLeftApplication += HandleAdLeftApplication;
		// Load a banner ad.
		bannerView.LoadAd(createAdRequest());
		bannerRequested = true;
	}
	
	public void ShowInterstitial()
	{

//		Debug.Log("Entro en AdMob Interstitial");
		
		//ADMOB
		if (interstitial.IsLoaded())
		{
			interstitial.Show();
//			Debug.Log("Play AdMob Interstitial");
			RequestInterstitial();
		}
		else
		{
//			Debug.Log("No hay AdMob Interstitial");
			SiguientePlayAIntertitial ();
		}

	}
	
	public void ShowBanner(){
	return;
//	if( GameManager.reachedLevel<=3 || GameManager.instance.tipoPlataforma != GameManager.Kplataforma.Freemium ) return;
//	if ( Niveles.instance.numNivelAlcanzado<4 || Niveles.instance.modoJuego != Niveles.ModoJuego.Normal) return;

//		Debug.Log("####### ShowBanner");
		#if !UNITY_EDITOR
//			#if UNITY_IPHONE 
//			if(ADBannerView.IsAvailable(ADBannerView.Type.Banner)){
//				banner.visible = true;
//			}else{
//				if( !bannerRequested){
//					RequestBanner();
//				}else{
//					if(bannerView != null) bannerView.Show();
//				}
//			} 
//			#elif UNITY_ANDROID 

			if( !bannerRequested){
				RequestBanner();
			}else{
				if(bannerView != null) bannerView.Show();
			}

//			#endif
		#endif
	}
	public void HideBanner(){
//		Debug.Log("####### HideBanner");
		#if !UNITY_EDITOR
//			#if UNITY_IPHONE 
//			if( banner.visible ){
//				banner.visible = false;
//			}else{
//				if(bannerView != null) bannerView.Hide();
//			} 
//			#elif UNITY_ANDROID

			if( !bannerRequested) return;
			if(bannerView != null) bannerView.Hide();

//			#endif
		#endif
	}
	public void DestroyBanner(){
//		Debug.Log("####### DestroyBanner");
		#if !UNITY_EDITOR
//		#if UNITY_IPHONE && !UNITY_EDITOR
//		banner.visible = false;
//		#elif UNITY_ANDROID && !UNITY_EDITOR

		if( !bannerRequested) return;
		if(bannerView != null) bannerView.Destroy();
		#endif
	}
		
	// Returns an ad request with custom ad targeting. ADMOB
	public AdRequest createAdRequest()
	{
//		Debug.Log("####### createAdRequest");
		return new AdRequest.Builder()
			.AddTestDevice(AdRequest.TestDeviceSimulator)
				.AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
				.AddKeyword("game")
				.SetGender(Gender.Unknown)
				.SetBirthday(new DateTime(1985, 1, 1))
				.TagForChildDirectedTreatment(false)
				.AddExtra("color_bg", "9B30FF")
				.Build();
	}
#endif
	
	
	
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------- DELEGATES ------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	//AdColony Video Finalizado
	private void OnVideoFinished (bool ad_was_shown)
	{
		Debug.Log (" (AdColony) On Video Finalizado");
		if( ad_was_shown ){
			Dame_premio();
		}else{
			Dame_premio();
			estoyMostrandoUnVideo = false;
			accionDeVerUnvideoTerminado = true ;
		}
	}
	#if UNITY_IPHONE
#endif
	//Tapjoy Video Finalizado
	public void HandleVideoAdCompleted()
	{
//		Debug.Log("(Tapjoy) C#: HandleVideoAdCompleted");
		//		CanvasDebug.Write("Video finalizado. Llamamos a Dame_Premio");
		Dame_premio();
	}
	
	//Chartboost Video Finalizado
	void didCompleteRewardedVideo(CBLocation location, int reward) {
//		Debug.Log(string.Format("didCompleteRewardedVideo: reward {0} at location {1}", reward, location));
		Dame_premio();
		estoyMostrandoUnVideo = false;
		accionDeVerUnvideoTerminado = true ;
	}
	
	//Vungle Video Finalizado
	
//	void onAdViewedEvent( double watched, double length )
//	{
////		Debug.Log( "onAdViewedEvent. watched: " + watched + ", length: " + length );
//		
////		VideoTest.instance.debug.text =  "watched: " + watched + ", length: " + length ;
//		
//		if(watched == length ){
//			Dame_premio();
//		}else{
//			estoyMostrandoUnVideo = false;
//			accionDeVerUnvideoTerminado = true ;
//		}
//		vungleAvailableCached = false ;
//	}
//	
	/// <summary>
	/// Handles the tapjoy connect success. ----------- TAPJOY
	/// </summary>
	#region Connect Delegate Handlers
	public void HandleConnectSuccess() {
//		Debug.Log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ C#: Handle Connect Success");
		isConnected = true;
		//init Tapjoy placement
		if (directPlayPlacement == null) {
			directPlayPlacement = TJPlacement.CreatePlacement( samplePlacementName );
			if (directPlayPlacement != null) {
				directPlayPlacement.RequestContent();
			}
		}
	}
	
	public void HandleConnectFailure() {
//		Debug.Log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ C#: Handle Connect Failure");
		//		if (!Tapjoy.IsConnected) {
		//			Tapjoy.Connect();
		//		}
//		accionDeVerUnvideoTerminado = true ;
	}
	#endregion
	
	
	
	#region View Delegate Handlers
	public void HandleViewWillOpen(int viewType) {
//		Debug.Log("C#: HandleViewWillOpen, viewType: " + viewType);
	}
	
	public void HandleViewDidOpen(int viewType) {
//		Debug.Log("C#: HandleViewDidOpen, viewType: " + viewType);
		viewIsShowing = true;
	}
	
	public void HandleViewWillClose(int viewType) {
//		Debug.Log("C#: HandleViewWillClose, viewType: " + viewType);
		//accionDeVerUnvideoTerminado = true ;
	}
	
	public void HandleViewDidClose(int viewType) {
//		Debug.Log("C#: HandleViewDidClose, viewType: " + viewType);
		viewIsShowing = false;
		//accionDeVerUnvideoTerminado = true ;
	}
	#endregion
	
	//UnityAds Video Finalizado
	public void HandleShowResult (ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
//			Debug.Log("The ad was successfully shown.");
			Dame_premio();
			if (!object.ReferenceEquals(_handleFinished,null)) _handleFinished();
			break;
		case ShowResult.Skipped:
//			Debug.Log("The ad was skipped before reaching the end.");
			estoyMostrandoUnVideo = false;
			accionDeVerUnvideoTerminado = true ;
			if (!object.ReferenceEquals(_handleSkipped,null)) _handleSkipped();
			break;
		case ShowResult.Failed:
//			Debug.Log("The ad failed to be shown.");
			estoyMostrandoUnVideo = false;
			accionDeVerUnvideoTerminado = true ;
			if (!object.ReferenceEquals(_handleFailed,null)) _handleFailed();
			break;
		}
		
		
		
		if (!object.ReferenceEquals(_onContinue,null)) _onContinue();
	}
	
	#region Tapjoy Delegate Handlers
	
	
	public void HandleShowOffers() {
//		Debug.Log("C#: HandleShowOffers");
	}
	
	public void HandleShowOffersFailure(string error) {
//		Debug.Log("C#: HandleShowOffersFailure: " + error);
	}
	#endregion
	
	#region Placement Delegate Handlers
	public void HandlePlacementRequestSuccess(TJPlacement placement) {
		if (placement.IsContentAvailable() && placement.GetName() == samplePlacementName) {
//			Debug.Log("C#: Content available for " + placement.GetName());
			contentIsReadyForPlacement = true;
			//placement.ShowContent();
		} else {
//			Debug.Log("C#: No content available for " + placement.GetName());
		}
	}
	
	public void HandlePlacementRequestFailure(TJPlacement placement, string error) {
//		Debug.Log("C#: HandlePlacementRequestFailure");
//		Debug.Log("C#: Request for " + placement.GetName() + " has failed because: " + error);
	}
	
	public void HandlePlacementContentReady(TJPlacement placement) {
//		Debug.Log("C#: HandlePlacementContentReady");
		if (placement.IsContentAvailable()) {
			//placement.ShowContent();
		} else {
//			Debug.Log("C#: no content");
		}
	}
	
	public void HandlePlacementContentShow(TJPlacement placement) {
//		Debug.Log("C#: HandlePlacementContentShow");
	}
	
	public void HandlePlacementContentDismiss(TJPlacement placement) {
//		Debug.Log("C#: HandlePlacementContentDismiss");
		contentIsReadyForPlacement = false;
	}
	
	void HandleOnPurchaseRequest (TJPlacement placement, TJActionRequest request, string productId)
	{
		Debug.Log ("C#: HandleOnPurchaseRequest");
		request.Completed();
	}
	
	void HandleOnRewardRequest (TJPlacement placement, TJActionRequest request, string itemId, int quantity)
	{
		Debug.Log ("C#: HandleOnRewardRequest");
		request.Completed();
	}
	
	#endregion
	
	#region Currency Delegate Handlers
	public void HandleAwardCurrencyResponse(string currencyName, int balance) {
//		Debug.Log("C#: HandleAwardCurrencySucceeded: currencyName: " + currencyName + ", balance: " + balance);
	}
	
	public void HandleAwardCurrencyResponseFailure(string error) {
//		Debug.Log("C#: HandleAwardCurrencyResponseFailure: " + error);
	}
	
	public void HandleGetCurrencyBalanceResponse(string currencyName, int balance) {
//		Debug.Log("C#: HandleGetCurrencyBalanceResponse: currencyName: " + currencyName + ", balance: " + balance);
	}
	
	public void HandleGetCurrencyBalanceResponseFailure(string error) {
//		Debug.Log("C#: HandleGetCurrencyBalanceResponseFailure: " + error);
	}
	
	public void HandleSpendCurrencyResponse(string currencyName, int balance) {
//		Debug.Log("C#: HandleSpendCurrencyResponse: currencyName: " + currencyName + ", balance: " + balance);
	}
	
	public void HandleSpendCurrencyResponseFailure(string error) {
//		Debug.Log("C#: HandleSpendCurrencyResponseFailure: " + error);
	}
	
	public void HandleEarnedCurrency(string currencyName, int amount) {
//		Debug.Log("C#: HandleEarnedCurrency: currencyName: " + currencyName + ", amount: " + amount);
		
		Tapjoy.ShowDefaultEarnedCurrencyAlert();
	}
	#endregion
	
	#region Video Delegate Handlers
	public void HandleVideoStart() {
//		Debug.Log("C#: HandleVideoStarted");
	}
	
	public void HandleVideoError(string status) {
//		Debug.Log("C#: HandleVideoError, status: " + status);
	}
	
	public void HandleVideoComplete() {
//		Debug.Log("C#: HandleVideoComplete");
		Dame_premio();
	}
	#endregion


	//------------------------------- ADCOLONY -------------------------------------------------
	public void OnAdAvailabilityChange(bool availability, string zoneId) {
		if(availability && zoneId == zoneString) {
			Debug.Log("Adcolony video preparado");
			adcolonyVideoPreparado = true ;
		}
		else {
			Debug.Log("Adcolony video Nooo preparado");
			adcolonyVideoPreparado = false ;
		}
	}
	//------------------------------------------------------------------------------------------
#if UNITY_IPHONE
#endif

	//------------------------------- ChartBoost -----------------------------------------------
	
	void didCacheInterstitialEvent( string location )
	{
//		Debug.Log( "didCacheInterstitialEvent: " + location );
	}
	
	
	void didFailToCacheInterstitialEvent( string location, string error )
	{
//		Debug.Log( "didFailToCacheInterstitialEvent: " + location + ", error: " + error );
	}
	
	
	void didFinishInterstitialEvent( string location, string reason )
	{
//		Debug.Log( "didFinishInterstitialEvent. Location: " + location + ", reason: " + reason );
		
		estoyMostrandoUnVideo = false;
		accionDeVerUnvideoTerminado = true ;
		
		if( !tengoAnuncioPendienteDeValidarPorPatrocinio )	return;
		tengoAnuncioPendienteDeValidarPorPatrocinio = false;
		//Finalizado ver Intertitial ChartBoost
		
		if(reason=="click"){
//			Debug.Log("Chartboot click");
			PlayerPrefs.SetInt("CbVistosSinHacerClick",-3);
			
//			GameManager.patrocinios += 2;
//			GetSetPlayerPrefs.SetGenericPlayerPref("Patrocinios",GameManager.patrocinios);
			

		}else if(reason=="dismiss"){
//			Debug.Log("Chartboot dismiss");
			
			
		}else if(reason=="close"){
//			Debug.Log("Chartboot close");
			
			
			int VecesSinHacerClickEnChartBoostIntertitial=0;
			if(PlayerPrefs.HasKey("CbVistosSinHacerClick") ){
				VecesSinHacerClickEnChartBoostIntertitial = PlayerPrefs.GetInt("CbVistosSinHacerClick");
			}
			
			VecesSinHacerClickEnChartBoostIntertitial++;
			
			PlayerPrefs.SetInt("CbVistosSinHacerClick",VecesSinHacerClickEnChartBoostIntertitial);
			
			if( VecesSinHacerClickEnChartBoostIntertitial == 2 || VecesSinHacerClickEnChartBoostIntertitial == 7 ){
//				AlertEventos.Alerta("No_Hay_Videos_DIsponibles", IdiomaManager.IdiomaKey("No_Hay_Videos_DIsponibles_Ver_un_Chartboost2-7") ,false, "", "Aceptar", "CB_inertitial_patrocinios_Dismiss");
			}else if( VecesSinHacerClickEnChartBoostIntertitial == 5 || VecesSinHacerClickEnChartBoostIntertitial == 10){
//				AlertEventos.Alerta("No_Hay_Videos_DIsponibles", IdiomaManager.IdiomaKey("No_Hay_Videos_DIsponibles_Ver_un_Chartboost10") ,false, "", "Aceptar", "CB_inertitial_patrocinios_Dismiss");
			}else if( VecesSinHacerClickEnChartBoostIntertitial == 12){
//				AlertEventos.Alerta("No_Hay_Videos_DIsponibles", IdiomaManager.IdiomaKey("No_Hay_Videos_DIsponibles_Ver_un_Chartboost12") ,false, "", "Aceptar", "CB_inertitial_patrocinios_Dismiss");
			}else{
//				LoadParameters.LoadParametersScript.ReleaseCamera();
//				LoadParameters.LoadParametersScript.SetParameters();
			}
		}
		
		
		
	}
	
	
	
	//CHARTBOOST
	
	void didCacheMoreAppsEvent( string location )
	{
//		Debug.Log( "didCacheMoreAppsEvent: " + location );
	}
	
	
	void didFailToCacheMoreAppsEvent( string location, string error )
	{
//		Debug.Log( "didFailToCacheMoreAppsEvent: " + location + ", error: " + error );
	}
	
	
	void didFinishMoreAppsEvent( string location, string reason )
	{
//		Debug.Log( "didFinishMoreAppsEvent. Location: " + location + ", reason: " + reason );
	}
	
	
	void didCacheRewardedVideoEvent( string location )
	{
//		Debug.Log( "didCacheRewardedVideoEvent: " + location );
	}
	
	
	void didFailToLoadRewardedVideoEvent( string location, string error )
	{
		//Debug.Log( "didFailToLoadRewardedVideoEvent: " + location + ", error: " + error );
		//CanvasDebug.Write("Cancelo video desde Chartboost");
		//VideoCancelado();
	}
	
	
	void didFinishRewardedVideoEvent( string location, string reason )
	{
//		Debug.Log( "didFinishRewardedVideoEvent. Location: " + location + ", reason: " + reason );
		// Si pongo Dame_Premio, corremos el riesgo de darlo dos veces.
		//		Dame_premio();
		estoyMostrandoUnVideo = false;
		accionDeVerUnvideoTerminado = true ;
	}
	
	
	
	
	/// <summary>
	/// /*Nuevos  delegates de CHARTBOOST*/
	/// </summary>
	
	void didFailToLoadInterstitial(CBLocation location, CBImpressionError error) {
//		Debug.Log(string.Format("didFailToLoadInterstitial: {0} at location {1}", error, location));
		estoyMostrandoUnVideo = false;
		accionDeVerUnvideoTerminado = true ;
	}
	
	void didDismissInterstitial(CBLocation location) {
//		Debug.Log("didDismissInterstitial: " + location);
	}
	
	void didCloseInterstitial(CBLocation location) {
//		Debug.Log("didCloseInterstitial: " + location);
	}
	
	void didClickInterstitial(CBLocation location) {
//		Debug.Log("didClickInterstitial: " + location);
	}
	
	void didCacheInterstitial(CBLocation location) {
//		Debug.Log("didCacheInterstitial: " + location);
	}
	
	bool shouldDisplayInterstitial(CBLocation location) {
		// return true if you want to allow the interstitial to be displayed
//		Debug.Log("shouldDisplayInterstitial @" + location + " : " + showInterstitial);
		return showInterstitial;
	}
	
	void didDisplayInterstitial(CBLocation location){
//		Debug.Log("didDisplayInterstitial: " + location);
	}
	
	void didFailToLoadMoreApps(CBLocation location, CBImpressionError error) {
//		Debug.Log(string.Format("didFailToLoadMoreApps: {0} at location: {1}", error, location));
	}
	
	void didDismissMoreApps(CBLocation location) {
//		Debug.Log(string.Format("didDismissMoreApps at location: {0}", location));
	}
	
	void didCloseMoreApps(CBLocation location) {
//		Debug.Log(string.Format("didCloseMoreApps at location: {0}", location));
	}
	
	void didClickMoreApps(CBLocation location) {
//		Debug.Log(string.Format("didClickMoreApps at location: {0}", location));
	}
	
	void didCacheMoreApps(CBLocation location) {
//		Debug.Log(string.Format("didCacheMoreApps at location: {0}", location));
	}
	
	bool shouldDisplayMoreApps(CBLocation location) {
//		Debug.Log(string.Format("shouldDisplayMoreApps at location: {0}: {1}", location, showMoreApps));
		return showMoreApps;
	}
	
	void didDisplayMoreApps(CBLocation location){
//		Debug.Log("didDisplayMoreApps: " + location);
	}
	
	void didFailToRecordClick(CBLocation location, CBClickError error) {
//		Debug.Log(string.Format("didFailToRecordClick: {0} at location: {1}", error, location));
	}
	
	void didFailToLoadRewardedVideo(CBLocation location, CBImpressionError error) {
//		Debug.Log(string.Format("didFailToLoadRewardedVideo: {0} at location {1}", error, location));
	}
	
	void didDismissRewardedVideo(CBLocation location) {
//		Debug.Log("didDismissRewardedVideo: " + location);
	}
	
	void didCloseRewardedVideo(CBLocation location) {
//		Debug.Log("didCloseRewardedVideo: " + location);
		estoyMostrandoUnVideo = false;
		accionDeVerUnvideoTerminado = true ;
	}
	
	void didClickRewardedVideo(CBLocation location) {
//		Debug.Log("didClickRewardedVideo: " + location);
		estoyMostrandoUnVideo = false;
		accionDeVerUnvideoTerminado = true ;
	}
	
	void didCacheRewardedVideo(CBLocation location) {
//		Debug.Log("didCacheRewardedVideo: " + location);
	}
	
	bool shouldDisplayRewardedVideo(CBLocation location) {
//		Debug.Log("shouldDisplayRewardedVideo @" + location + " : " + showRewardedVideo);
		return showRewardedVideo;
	}
	
	
	
	void didDisplayRewardedVideo(CBLocation location){
//		Debug.Log("didDisplayRewardedVideo: " + location);
	}
	
	void didCacheInPlay(CBLocation location) {
//		Debug.Log("didCacheInPlay called: "+location);
	}
	
	void didFailToLoadInPlay(CBLocation location, CBImpressionError error) {
//		Debug.Log(string.Format("didFailToLoadInPlay: {0} at location: {1}", error, location));
		estoyMostrandoUnVideo = false;
		accionDeVerUnvideoTerminado = true ;
	}
	
	void didPauseClickForConfirmation() {
//		Debug.Log("didPauseClickForConfirmation called");
		
	}
	
	void willDisplayVideo(CBLocation location) {
//		Debug.Log("willDisplayVideo: " + location);
	}
	
	
	//ADMOB
	#region Interstitial callback handlers
	public void HandleInterstitialLoaded(object sender, EventArgs args)
	{
//		print("HandleInterstitialLoaded event received.");
	}
	public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
//		print("HandleInterstitialFailedToLoad event received with message: " + args.Message);
	}
	public void HandleInterstitialOpened(object sender, EventArgs args)
	{
//		print("HandleInterstitialOpened event received");
		
	}
	void HandleInterstitialClosing(object sender, EventArgs args)
	{
//		print("HandleInterstitialClosing event received");
		
	}
	public void HandleInterstitialClosed(object sender, EventArgs args)
	{
//		print("HandleInterstitialClosed event received");

		
	}
	public void HandleInterstitialLeftApplication(object sender, EventArgs args)
	{
//		print("HandleInterstitialLeftApplication event received");
	}
	#endregion
	
	#region Banner callback handlers
	
	public void HandleAdLoaded(object sender, EventArgs args)
	{
//		print("HandleAdLoaded event received.");

	}
	
	public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
//		print("HandleFailedToReceiveAd event received with message: " + args.Message);
	}
	
	public void HandleAdOpened(object sender, EventArgs args)
	{
//		print("HandleAdOpened event received");
	}
	
	void HandleAdClosing(object sender, EventArgs args)
	{
//		print("HandleAdClosing event received");
	}
	
	public void HandleAdClosed(object sender, EventArgs args)
	{
//		print("HandleAdClosed event received");
	}
	
	public void HandleAdLeftApplication(object sender, EventArgs args)
	{
		print("HandleAdLeftApplication event received");
	}
	
	#endregion
	
	#region handlers de UnityAds
	//--- Static Helper Methods
	
	public bool isShowing { get { return Advertisement.isShowing; }}
	public bool isSupported { get { return Advertisement.isSupported; }}
	public bool isInitialized { get { return Advertisement.isInitialized; }}
	
	public bool IsReady () 
	{ 
		return IsReady(null); 
	}
	public bool IsReady (string zoneID) 
	{
		if (string.IsNullOrEmpty(zoneID)) zoneID = null;
		
		return Advertisement.isReady(zoneID);
	}
	
	public void ShowAd () 
	{
		ShowAd(null,null,null,null,null);
	}
	public void ShowAd (string zoneID) 
	{
		ShowAd(zoneID,null,null,null,null);
	}
	public void ShowAd (string zoneID, Action handleFinished) 
	{
		ShowAd(zoneID,handleFinished,null,null,null);
	}
	public void ShowAd (string zoneID, Action handleFinished, Action handleSkipped) 
	{
		ShowAd(zoneID,handleFinished,handleSkipped,null,null);
	}
	public void ShowAd (string zoneID, Action handleFinished, Action handleSkipped, Action handleFailed) 
	{
		ShowAd(zoneID,handleFinished,handleSkipped,handleFailed,null);
	}
	public void ShowAd (string zoneID, Action handleFinished, Action handleSkipped, Action handleFailed, Action onContinue)
	{
		if (string.IsNullOrEmpty(zoneID)) zoneID = null;
		
		_handleFinished = handleFinished;
		_handleSkipped = handleSkipped;
		_handleFailed = handleFailed;
		_onContinue = onContinue;
		
		if (Advertisement.isReady(zoneID))
		{
//			Debug.Log("Showing ad now...");
			
			ShowOptions options = new ShowOptions();
			options.resultCallback = HandleShowResult;
			options.pause = true;
			
			Advertisement.Show(zoneID,options);
		}
		else 
		{
			estoyMostrandoUnVideo = false;
			accionDeVerUnvideoTerminado = true ;
//			Debug.Log(string.Format("Unable to show ad. The ad placement zone {0} is not ready.",
//			                        object.ReferenceEquals(zoneID,null) ? "default" : zoneID));
		}
	}
	
	
	#endregion
	
	//--------------------------------VUNGLE EVENTS -----------------------------------------------------------------
	
//	void onAdStartedEvent()
//	{
////		Debug.Log( "onAdStartedEvent" );
//	}
//	
//	void onAdEndedEvent()
//	{
////		Debug.Log( "onAdEndedEvent" );
//		estoyMostrandoUnVideo = false;
//		//accionDeVerUnvideoTerminado = true ;
//	}
//	
//	void onCachedAdAvailableEvent()
//	{
////		Debug.Log( "onCachedAdAvailableEvent" );
//		vungleAvailableCached = true ;
//	}


	
	//--------------------------------APPLOVIN EVENTS -----------------------------------------------------------------
	
	void onAppLovinEventReceived(string ev)
	{
		Debug.Log("APPLOVING EVENTS: "+ev);
		if (ev.Contains("REWARDAPPROVEDINFO"))
		{
			// Split the string into its three components.
			char[] delimiter = { '|' };
			string[] split = ev.Split(delimiter);
			// Pull out and parse the amount of virtual currency.
			double amount = double.Parse(split[1]);
			// Pull out the name of the virtual currency
			string currencyName = split[2];
			
			Dame_premio();
			applovinCached = false;
			AppLovin.PreloadInterstitial();
			AppLovin.LoadRewardedInterstitial();
			Debug.Log ("AppLovin REWARDAPPROVEDINFO Do something with this info - for example, grant coins to the user");
			//myFunctionToUpdateBalance(currencyName, amount);
		}
		if (string.Equals(ev, "DISPLAYEDINTER") || string.Equals(ev, "VIDEOBEGAN"))
		{
			applovinCached = false;
			Debug.Log ("AppLovin DISPLAYEDINTER or VIDEOBEGAN");

		}
		if (string.Equals(ev, "HIDDENINTER") || string.Equals(ev, "VIDEOSTOPPED"))
		{
			//			Time.timeScale = 1.0f;
			//			AudioListener.pause = false;
			applovinCached = false;
			Debug.Log ("AppLovin HIDDENINTER or VIDEOSTOPPED");

		}
		if (string.Equals(ev, "LOADEDINTER"))
		{
			applovinCached = true;
			// The last ad load was successful.
			Debug.Log (" Probably do AppLovin.ShowInterstitial");
		}
		if (string.Equals(ev, "LOADFAILED"))
		{
			applovinCached = false;
			estoyMostrandoUnVideo = false;
			accionDeVerUnvideoTerminado = true ;
			AppLovin.PreloadInterstitial();
			AppLovin.LoadRewardedInterstitial();
			Debug.Log ("AppLovin The last ad load failed.");
		}
		
		if (string.Equals(ev, "LOADINTERFAILED"))
		{
			applovinCached = false;

			Debug.Log ("AppLovin The last video load failed.");
		}
		
		if(ev.Contains("LOADEDREWARDED")){
			Debug.Log ("AppLovin A rewarded video was successfully loaded.");
			applovinCached = true;
		}
		else if(ev.Contains("LOADREWARDEDFAILED")){
			Debug.Log ("AppLovin A rewarded video failed to load.");
			applovinCached = false;

		}
		else if(ev.Contains("HIDDENREWARDED")){
			Debug.Log ("AppLovin A rewarded video was closed.  Preload the next rewarded video.");
			applovinCached = false;
			AppLovin.LoadRewardedInterstitial();
		}
	}
	#if UNITY_IPHONE
#endif
	
	//------------------------------------------ Fyber ---------------------------------------------------------------------------
	
	// Registered to be called upon reception of the answer for a successful offer request  
	public void OnSPBrandEngageResponseReceived(bool offersAvailable) {
		if (offersAvailable) {
			mbeOffersStatus = "YES";
		} else {
			mbeOffersStatus = "NO";
		}
		
	}
	
	// Registered to be called if an error is triggered by the offer request
	public void OnSPBrandEngageErrorReceived(string message) {
		mbeOffersStatus = "Error:\n" + message;
	}
	
	// OnNativeExceptionReceivedFromSDK:
	public void OnNativeExceptionReceivedFromSDK(string message) {
//		Debug.Log("Fyber: OnNativeExceptionReceivedFromSDK Error");
	}
	
	
	// OnOFWResultReceived:
	public void OnOFWResultReceived(string message) {
//		Debug.Log("OfferWall return status");
	}
	
	
	// OnMBEResultReceived:
	public void OnMBEResultReceived(string message) {
		mbeOffersStatus = "NO";
//		Debug.Log ("video finalizado Fyber "+message);
		estoyMostrandoUnVideo = false;
		accionDeVerUnvideoTerminado = true ;
		requestMBEOffers();

		if( message=="CLOSE_FINISHED"){
			Dame_premio();
		}
	}
	
	// requestMBEOffers: Request Mobile Brand Engagement offers
	private void requestMBEOffers() {
//		if (overrideCredentials) {
//			sponsorPayPlugin.RequestBrandEngageOffers (credentialsToken, customCurrencyName, mbeVCSshowNotification, currencyId, placementId);
//		} else {
//			sponsorPayPlugin.RequestBrandEngageOffers (null, customCurrencyName, mbeVCSshowNotification,currencyId, placementId);
//		}
	}
	
	//----------------------------------------------------------------------------------------------------------------------------
	
	#region View Delegate Handlers IAD
//	void OnBannerClicked()
//	{
////		Debug.Log("Clicked!\n");
//	}
//	
//	void OnBannerLoaded()
//	{
////		Debug.Log("Loaded!\n");
//		#if UNITY_IPHONE
//		banner.visible = false;
//		#endif
//	}
//	
//	void OnBannerFailedToLoad()
//	{
////		Debug.Log("FAIL!\n");
//		#if UNITY_IPHONE
//		banner.visible = false;
//		#endif
//	}
	#endregion
	
	
	#endif
	#endif
}



