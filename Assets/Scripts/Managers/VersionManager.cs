﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class VersionManager : MonoBehaviour {

#if UNITY_STANDALONE
	#if STEAM
	static public string gameVersion = "1.0"; // Versión de Steam
	#else
	static public string gameVersion = "1.0"; // Versión de OS: X
	#endif
#elif UNITY_ANDROID
	static public string gameVersion = "1.4"; // Versión de Android
#else
	static public string gameVersion = "1.4"; // Versión de iPhone
#endif

static public bool haPasadoRevisionDefault_TRUE = true;
static public bool haPasadoRevisionDefault_FALSE = false;

static public bool dejarVerVideos = false;

public string Url_anuncio_interno="";
public string Texto_anuncio="";
public int pistasPorVideo=2;
public int vidasPorVideo=3;
public int pistasDadasAlInicioJuego=3;

static public VersionManager instance;

	void Awake(){
		DontDestroyOnLoad(transform.gameObject);
		instance = this ;
	}

	void Start () {

		#if UNITY_ANDROID || UNITY_IPHONE
				dejarVerVideos = true;
		#else
				dejarVerVideos = false;
		#endif
 
		StartCoroutine(CargarJson());
	}

	IEnumerator CargarJson(){

#if UNITY_ANDROID
		haPasadoRevisionDefault_TRUE = true;
		haPasadoRevisionDefault_FALSE = true;
#endif

#if UNITY_STANDALONE && !UNITY_EDITOR
	#if STEAM
	string url = "http://Bestwordgames.net/PerfectAngle/getInfoSTEAMVR.php";
	#else
	string url = "http://Bestwordgames.net/PerfectAngle/getInfoMACVR.php";
	#endif
#elif UNITY_ANDROID && !UNITY_EDITOR
	string url = "http://Bestwordgames.net/PerfectAngle/getInfoAndroidVR.php";
	#if AMAZON
	url = "http://Bestwordgames.net/PerfectAngle/getInfoAmazonVR.php";
	#endif		

#elif UNITY_IPHONE && !UNITY_EDITOR
	string url = "http://Bestwordgames.net/PerfectAngle/getInfoiOSVR.php";
#else
string url = "http://Bestwordgames.net/PerfectAngle/getInfoiOSVR.php";
//	string url = "http://Bestwordgames.net/PerfectAngle/getInfoTestVR.php";
#endif

//        Debug.Log ("URL: " + url);

		WWWForm form = new WWWForm();
		form.AddField ("language", Application.systemLanguage.ToString() );

		WWW www = new WWW(url, form);

		yield return www;

		if(www.error != null){

//            Debug.Log ("www.error: " + www.error);
			#if !UNITY_STANDALONE && !UNITY_TVOS
			AdsManager.VAdslist.Clear();
			AdsManager.VAdslist.Add ("unityads");	//OK
			AdsManager.VAdslist.Add ("tapjoy"); 		// nueva Sdk Key
			AdsManager.VAdslist.Add ("applovin");
			AdsManager.VAdslist.Add ("adcolony");
			AdsManager.VAdslist.Add ("chartboost"); //creat en la dashboard CBlocation

			AdsManager.Adslist.Add ("chartboost");
			AdsManager.Adslist.Add ("admob");

			AdsManager.porcentajeAnuncios = 50 ;
			#endif
		}else{
//            Debug.Log (www.text);
			CargarDatosDelJson(www.text);
		}

	}

	void CargarDatosDelJson(string jsontext){

		var N = JSON.Parse(jsontext);

		string revision = "";

		revision = N["inReview"].Value.ToString();

		pistasDadasAlInicioJuego = N["pistasIniciales"].AsInt;

		//Anuncios Videos e Interticiales
		#if (UNITY_ANDROID || UNITY_IPHONE)

			AdsManager.porcentajeAnuncios = N ["porcentajeAnuncios"].AsInt ;

			string[] SA = N ["sistemaAnuncios"].Value.ToString ().Split ("," [0]);
			for (int i=0; i<SA.Length; i++) {
				AdsManager.Adslist.Add (SA [i]);
			}
			string[] SV = N ["sistemaVideos"].Value.ToString ().Split ("," [0]);
			for (int i=0; i<SV.Length; i++) {
				AdsManager.VAdslist.Add (SV [i]); 
			}
        #endif

		if(revision != gameVersion){
			//Ha pasado la revisión
			haPasadoRevisionDefault_TRUE = true;
			haPasadoRevisionDefault_FALSE = true;
		}else{
			//No ha pasado la revisión
			haPasadoRevisionDefault_TRUE = false;
			haPasadoRevisionDefault_FALSE = false;
		}

	}



}
