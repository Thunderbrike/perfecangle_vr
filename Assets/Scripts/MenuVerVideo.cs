﻿using UnityEngine;
using System.Collections;

public class MenuVerVideo : MonoBehaviour {
	void Start ()
    {
 
        FadeEntreEscenas.instance.FadeOut();
		GameObject panelnfo = Instantiate(Resources.Load("Prefabs/PanelJugarViendoVideo"), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
        MetodosComunes.instance.ComprobarModoJuego(gameObject);
	}
}
