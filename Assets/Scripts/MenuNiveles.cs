﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuNiveles : MonoBehaviour 
{
    public GameObject botonNivelVR;
    public GameObject botonNivelNormal;
    public Transform panelMenuPrefab;
    public GameObject whiteRoom;

    public float espacioEntreBotones = 1;
    public float distanciaDelCentro = 3;
    public float medidaDelBoton = 1;

    private float numFilas = 4;
    private float numColumnas = 18;
    private float distOld, medidaOld, espOld;
    private float initRotationMenu = 0;
    private float rayCastDistance;

	List<string[]> nivelesConLinks=new List<string[]>();

    Quaternion rotation = Quaternion.identity;
    Quaternion rotWR = Quaternion.identity;
    GameObject botonNivel;
    GameObject centroMenu, menu, panelMenu, panelOpciones, panelAcciones, panelExit;
    ComprasManager comprasManager;
   
    private bool modoDisenyoActivado = false;

    #region LIFE CYCLE

    void Awake()
    {
        FadeEntreEscenas.instance.FadeOut();
        comprasManager = ComprasManager.instance;   
    }

    void Start () 
    {
        // Comprobamos el modo de juego y inicializamos algunas cosas;
        MetodosComunes.instance.ComprobarModoJuego(gameObject);
        Inicializar();

        // Recogemos los sprites de los iconos y los storeamos en un array para poder utilizarlos más tarde.
        Sprite[] iconosNiveles = Resources.LoadAll<Sprite>("Sprites/IconosNiveles");        // Creamos array con los sprites que están en Resources
        for (int m = 0; m < iconosNiveles.Length; m++) Niveles.instance.iconoNivel.Add(iconosNiveles[m]);    // Storeamos los sprites en nuestra lista
        iconosNiveles = null;                                                               // Borramos el array 

        // Si es la primera vez que jugamos, saltamos el menú y nos vamos al juego directamente
        if (Niveles.instance.esLaPrimeraVezQueJuego)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("PerfectAngle");
            return;
        }

        // Situamos y escalamos nuestro prefab según las variables públicas
        botonNivel.transform.Find("Boton").gameObject.transform.position = new Vector3(distanciaDelCentro, 0, 0);
        botonNivel.transform.Find("Boton").transform.localScale = new Vector3(medidaDelBoton, medidaDelBoton, medidaDelBoton);

        // Inicializamos las variables de actualización del menú
        medidaOld = medidaDelBoton;
        distOld = distanciaDelCentro;
        espOld = espacioEntreBotones;

        CrearArrayMenu();
        CentrarMenu(initRotationMenu);

	
        //Si venimos del último nivel FREE del juego, tenemos que cargar la pantalla de compras
        if (Niveles.instance.tengoQueCargarPantallaCompras) {
            GameObject panelCompra = Instantiate(Resources.Load("Prefabs/PanelCompra" + (Niveles.instance.modoJuego == Niveles.ModoJuego.VR ? "VR" : "Normal")), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
        }
       

        //Si venimos del último nivel FREE del juego, tenemos que cargar la pantalla de compras
        if (Niveles.instance.tengoQueCargarPantallaFinal) {
            Niveles.instance.tengoQueCargarPantallaFinal = false;
            GameObject panelCompra = Instantiate(Resources.Load("Prefabs/PanelFinJuego" + (Niveles.instance.modoJuego == Niveles.ModoJuego.VR ? "VR" : "Normal")), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
        }
	}

    void Update()
    {
        //Debug.Log("puedoEnviarAlgunEvento: " + Niveles.instance.puedoEnviarAlgunEvento);
        //Debug.Log("hayUnPanelActivado: " + Niveles.instance.hayUnPanelActivado);
        //Debug.Log("rayCastDistance: " + rayCastDistance);

        if (modoDisenyoActivado) ActualizarMenu();
        if (Niveles.instance.modoJuego != Niveles.ModoJuego.VR)
        {
            float grados = gameObject.transform.rotation.eulerAngles.y * 0.75f;
            rotWR.eulerAngles = new Vector3(0, grados, 0);
            whiteRoom.transform.rotation = rotWR;
        }

        //ComprueboBotonDelCardboard();
    }

    #endregion

    #region PRIVATE METHODS

    private void Inicializar()
    {
        // Inicializamos las pistas
        Niveles.instance.tipoPista = Niveles.TipoPista.None;
		GameManager.numeroDePistasMostradas=0;
		Debug.Log("numeroDePistasMostradas: "+GameManager.numeroDePistasMostradas);

        // Activamos los parámetros dependiendo del modo de juego
        switch (Niveles.instance.modoJuego)
        {
            case Niveles.ModoJuego.VR:
                botonNivel = botonNivelVR;
                break;

            case Niveles.ModoJuego.Normal:
                botonNivel = botonNivelNormal;
				RellenarDatosNivelesConUrl();
                break;

            case Niveles.ModoJuego.AR:
                break;
        }
    }

    private void ComprueboBotonDelCardboard()
    {
        if (Niveles.instance.modoJuego != Niveles.ModoJuego.VR) return;

        Niveles.instance.numPuzzle = Niveles.instance.numNivelAlcanzado;

        Touch touch;

#if UNITY_EDITOR || UNITY_STANDALONE
        if (Input.GetMouseButton(0))
        {
            MetodosComunes.instance.PerfectAngle();
        }
#else    
        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            touch = Input.GetTouch(0);
            MetodosComunes.instance.PerfectAngle();
        }
#endif

    }

    private void CrearArrayMenu()
    {
        centroMenu = new GameObject();
        centroMenu.transform.position = gameObject.transform.position;
        centroMenu.transform.rotation = gameObject.transform.rotation;
        centroMenu.name = "PuntoAuxiliar";

        menu = new GameObject();
        menu.name = "Menu";

        switch (Niveles.instance.modoJuego)
        {
            case Niveles.ModoJuego.VR:
#if !UNITY_WSA
                panelMenu = Instantiate(Resources.Load("Prefabs/PanelMenuVR")) as GameObject;
#endif
                panelOpciones = Instantiate(Resources.Load("Prefabs/PanelOpcionesVR")) as GameObject; 
                panelAcciones = Instantiate(Resources.Load("Prefabs/PanelAccionesVR")) as GameObject;

#if !UNITY_IPHONE
                panelExit = Instantiate(Resources.Load("Prefabs/PanelExitVR")) as GameObject;
#endif
                break;

            case Niveles.ModoJuego.Normal:
#if !UNITY_WSA
                panelMenu = Instantiate(Resources.Load("Prefabs/PanelMenuNormal")) as GameObject;
#endif
                panelOpciones = Instantiate(Resources.Load("Prefabs/PanelOpcionesNormal")) as GameObject;
                panelAcciones = Instantiate(Resources.Load("Prefabs/PanelAccionesNormal")) as GameObject;

#if !UNITY_IPHONE
                panelExit = Instantiate(Resources.Load("Prefabs/PanelExitNormal")) as GameObject;
#endif
                break;

            case Niveles.ModoJuego.AR:
                break;
        }
#if !UNITY_WSA        
        panelMenu.name = "PanelMenu";
        panelMenu.transform.parent = menu.transform;
#endif
        panelOpciones.name = "PanelOptions";
        panelOpciones.transform.parent = menu.transform;

        panelAcciones.name = "PanelActions";
        panelAcciones.transform.parent = menu.transform;

#if !UNITY_IPHONE
        panelExit.name = "PanelExit";
        panelExit.transform.parent = menu.transform;
#endif

#if UNITY_WSA
        panelOpciones.transform.eulerAngles += new Vector3(0, 40, 0);
        panelAcciones.transform.eulerAngles += new Vector3(0, 40, 0);
        panelExit.transform.eulerAngles += new Vector3(0, 40, 0);
#endif


        int numNivel = 0;

        for (int n=0; n<numFilas; n++)
        {
            for (int m=0; m<numColumnas; m++)
            {
                rotation.eulerAngles = new Vector3(0, espacioEntreBotones * m, (espacioEntreBotones * -n) + (((numFilas-1)*espacioEntreBotones)/2.0f));
                centroMenu.transform.rotation = rotation;

                GameObject nuevoBoton = Instantiate(botonNivel, centroMenu.transform.position, centroMenu.transform.rotation) as GameObject;
                nuevoBoton.transform.parent = menu.transform;
                nuevoBoton.name = "Boton_" + Niveles.instance.nombreNivel[numNivel];

                if (numNivel < Niveles.instance.numNivelAlcanzado)
                    nuevoBoton.transform.Find("Boton/Sprite").gameObject.GetComponent<SpriteRenderer>().sprite = Niveles.instance.iconoNivel[Niveles.instance.GetNumeroDeSpriteSegunNombreDelNivel(Niveles.instance.nombreNivel[numNivel])];
                else if (numNivel == Niveles.instance.numNivelAlcanzado)
                {
                    nuevoBoton.transform.Find("Boton/Sprite").gameObject.GetComponent<SpriteRenderer>().sprite = Niveles.instance.iconoNivel[Niveles.instance.GetNumeroDeSpriteSegunNombreDelNivel("Interrogante")];
                    initRotationMenu = nuevoBoton.transform.rotation.eulerAngles.y;
                }
                else
                    nuevoBoton.transform.Find("Boton/Sprite").gameObject.GetComponent<SpriteRenderer>().sprite = Niveles.instance.iconoNivel[Niveles.instance.GetNumeroDeSpriteSegunNombreDelNivel("Candado")];

                nuevoBoton.transform.Find("Boton").gameObject.GetComponent<ReturnNumeroNivel>().numLevel = numNivel;

				
				if( Niveles.instance.modoJuego == Niveles.ModoJuego.Normal ){
					foreach( string[] dat in nivelesConLinks){
						if( dat[0]==Niveles.instance.nombreNivel[numNivel]){
							if (numNivel <= Niveles.instance.numNivelAlcanzado){
								nuevoBoton.transform.Find("Boton/Sprite").gameObject.GetComponent<SpriteRenderer>().color = Color.yellow ;
							}
							nuevoBoton.transform.Find("Boton").gameObject.GetComponent<BotonMenuNormal>().UrlJuego = dat[2];
							nuevoBoton.transform.Find("Boton").gameObject.GetComponent<BotonMenuNormal>().Url = dat[1];
						}
					}
				}

                numNivel++;

                if (numNivel == 70) break;
            }
        }

        Destroy(centroMenu);
    }

    void RellenarDatosNivelesConUrl(){

		nivelesConLinks.Clear();

		string[] datosUrl=new string[3];
		datosUrl[0]="FinalKickCachitos";
		datosUrl[1]="http://www.ivanovichgames.com/direct_FinalKick.php";
		datosUrl[2]="Final Kick";
		nivelesConLinks.Add(datosUrl);

		string[] datosUrl1=new string[3];
		datosUrl1[0]="IvanovichRocas";
		datosUrl1[1]="http://www.ivanovichgames.com";
		datosUrl1[2]="Ivanovich Games";
		nivelesConLinks.Add(datosUrl1);

		string[] datosUrl3=new string[3];
		datosUrl3[0]="IconoLetris";
		datosUrl3[1]="http://www.ivanovichgames.com/direct_Letris.php";
		datosUrl3[2]="Letris";
		nivelesConLinks.Add(datosUrl3);

		string[] datosUrl6=new string[3];
		datosUrl6[0]="BreakingFarm";
		datosUrl6[1]="http://www.ivanovichgames.com/direct_BrakingFarm.php";
		datosUrl6[2]="Breaking Farm";
		nivelesConLinks.Add(datosUrl6);

		string[] datosUrl7=new string[3];
		datosUrl7[0]="MotoMinibikers";
		datosUrl7[1]="http://www.ivanovichgames.com/direct_MiniBikers.php";
		datosUrl7[2]="MiniBikers";
		nivelesConLinks.Add(datosUrl7);

		string[] datosUrl8=new string[3];
		datosUrl8[0]="CocheMinidrivers";
		datosUrl8[1]="http://www.ivanovichgames.com/direct_MiniDrivers.php";
		datosUrl8[2]="MiniDrivers";
		nivelesConLinks.Add(datosUrl8);

		string[] datosUrl9=new string[3];
		datosUrl9[0]="FinalKickSombras";
		datosUrl9[1]="http://www.ivanovichgames.com/direct_FinalKick.php";
		datosUrl9[2]="Final Kick";
		nivelesConLinks.Add(datosUrl9);

//				nuevoBoton.transform.Find("Boton/Sprite").gameObject.GetComponent<SpriteRenderer>().color = Color.yellow ;

				/*
				FinalKickCachitos = http://www.ivanovichgames.com/direct_FinalKick.php
				IvanovichRocas = http://www.ivanovichgames.com
				Dinosaurio = https://www.youtube.com/watch?v=A4QcyW-qTUg
				IconoLetris = http://www.ivanovichgames.com/direct_Letris.php
				IconoNumtris = https://itunes.apple.com/app/id770145061?mt=8?ign-mpt=uo%3D8
				CruzImposible = https://www.youtube.com/watch?v=1dISzq4kIPc
				RampasImposibles = https://www.youtube.com/watch?v=hAXm0dIuyug
				BreakingFarm = http://www.ivanovichgames.com/direct_BrakingFarm.php
				MotoMiniBikers = http://www.ivanovichgames.com/direct_MiniBikers.php
				CocheMinidrivers = http://www.ivanovichgames.com/direct_MiniDrivers.php
				FinalKickSombras = http://www.ivanovichgames.com/direct_FinalKick.php

				*/
    }

    private void DestruirArrayMenu()
    {
        Destroy(centroMenu);
        Destroy(menu);
    }

    private void CentrarMenu(float grados)
    {
        rotation.eulerAngles = new Vector3(0, (Niveles.instance.modoJuego == Niveles.ModoJuego.VR ? 270 : 90) - grados, 0);
        menu.transform.rotation = rotation;

        if (Niveles.instance.modoJuego != Niveles.ModoJuego.VR)
        {
#if UNITY_WSA
            gameObject.GetComponent<MouseOrbit>().xMinLimit = 120 - grados;     // Grados mínimos de giro de la cámara para WSA (No están los paneles de publicidad)
#elif UNITY_IPHONE
            gameObject.GetComponent<MouseOrbit>().xMinLimit = 105 - grados;     // Grados mínimos de giro de la cámara para IPHONE (No está el panel de EXIT)
#else
            gameObject.GetComponent<MouseOrbit>().xMinLimit = -50; //-50 - grados;     // Grados mínimos de giro de la cámara para el resto de plataformas
#endif
            gameObject.GetComponent<MouseOrbit>().xMaxLimit = 308 - grados;     // Grados máxinos de giro de la cámara para todas las plataformas
        }
    }

    // Metodo utilizado para actualizar el menú dinámicamente y poder poner los botones allá en donde visualmente nos parece más correcto.
    // Sólo se llama en caso que tengamos la variable "modoDisenyoActivado" a true
    private void ActualizarMenu()
    {
        if ((medidaDelBoton - medidaOld) != 0)
        {
            botonNivel.transform.Find("Boton").transform.localScale = new Vector3(medidaDelBoton, medidaDelBoton, medidaDelBoton);
            DestruirArrayMenu();
            CrearArrayMenu();
            medidaOld = medidaDelBoton;
        }

        if ((distanciaDelCentro - distOld) != 0)
        {
            botonNivel.transform.Find("Boton").gameObject.transform.position = new Vector3(distanciaDelCentro, 0, 0);
            DestruirArrayMenu();
            CrearArrayMenu();
            distOld = distanciaDelCentro;
        }

        if ((espacioEntreBotones - espOld) != 0)
        {
            DestruirArrayMenu();
            CrearArrayMenu();
            espOld = espacioEntreBotones;
        }
    }

    #endregion
}
