﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelModoJuego : MonoBehaviour
{
    public Text cardboard;
    public GameObject pantallaRate;
    public GameObject pantallaGiroscopio;

    void Awake()
    {
        FadeEntreEscenas.instance.FadeOut();
        cardboard.text = LanguageManager.instance.IdiomaKey("cardboard");
        InstanciarAudioManager();
        MuestroPantallaDeRate();

#if UNITY_ANDROID
        InstanciarButtonBackAndroid();
#endif
        pantallaGiroscopio.SetActive(false);
    }

    public void CargoModoJuego(Niveles.ModoJuego modoJuego)
    {
        Niveles.instance.modoJuego = modoJuego;

#if !UNITY_EDITOR
        if (!SystemInfo.supportsGyroscope && Niveles.instance.modoJuego == Niveles.ModoJuego.VR)
        {
            Niveles.instance.puedoEnviarAlgunEvento = true;
            pantallaGiroscopio.SetActive(true);
            return;
        }
#endif



#if UNITY_IPHONE

        FadeEntreEscenas.instance.FadeIn("MenuEntrada");

        PlayerPrefs.SetInt("QualityVR", 1);
        Niveles.instance.nivelDeCalidadParaModoVR = 1;

        PlayerPrefs.SetInt("QualityNormal", 1);
        Niveles.instance.nivelDeCalidadParaModoNormal = 1;
#else
        switch (Niveles.instance.modoJuego)
        {
            case Niveles.ModoJuego.VR:
                if (PlayerPrefs.HasKey("QualityVR"))
                    FadeEntreEscenas.instance.FadeIn("MenuEntrada");
                else
                {
                    PlayerPrefs.SetInt("QualityVR", 0);
                    FadeEntreEscenas.instance.FadeIn("CalidadAutomaticaVR");
                }
                break;

            case Niveles.ModoJuego.Normal:
                if (PlayerPrefs.HasKey("QualityNormal"))
                    FadeEntreEscenas.instance.FadeIn("MenuEntrada");
                else
                {
                    PlayerPrefs.SetInt("QualityNormal", 0);
                    FadeEntreEscenas.instance.FadeIn("CalidadAutomatica");
                }
                break;

            case Niveles.ModoJuego.AR:
                break;
        }
#endif
    }

    public void MuestroPantallaDeRate()
    {
        if (Niveles.instance.tengoQueMostrarLaPantallaDeRateAlInicio && !Niveles.instance.heMostradoLaPantallaDeRate)
        {
            Niveles.instance.heMostradoLaPantallaDeRate = true;
            PlayerPrefs.SetInt("HemosMostradoLaPantallaDeRate", 1);
        }
        else
        {
            pantallaRate.SetActive(false);
        }
    }

    private void InstanciarAudioManager()
    {
        if (!Niveles.instance.audioManagerInstanciado)
        {
            Niveles.instance.audioManagerInstanciado = true;
            GameObject audioManager = Instantiate(Resources.Load("Prefabs/AudioManager")) as GameObject;

            audioManager.name = "AudioManager";
        }
    }

    private void InstanciarButtonBackAndroid()
    {
        if (!Niveles.instance.buttonBackManagerInstanciado)
        {
            Niveles.instance.buttonBackManagerInstanciado = true;

            GameObject buttonBack = Instantiate(Resources.Load("Prefabs/ButtonBackManager")) as GameObject;
            buttonBack.name = "ButtonBackManager";
        }
    }
}
