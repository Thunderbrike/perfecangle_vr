﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelNoHayVideos : PanelBase {

	const float DISTANCIA_RAYCAST = 5.6f;

    [Header("Textos a traducir")]
//    public Text resume;
//    public Text showHint;
    public Text mainMenu;

    float tm=0.0f;

    void Start()
    {
        base.InicializarPanel(base.rotacionInicial);
        base.EstablecerDistanciaRayCast(DISTANCIA_RAYCAST);

        CambiarTextos();
    }

	void Update(){
		tm+=Time.deltaTime;
		if( tm>2){
			tm=0;
			MetodosComunes.instance.MenuEntrada();
		}
    }

    public void CambiarTextos()
    {
//		resume.text = LanguageManager.instance.IdiomaKey("_playFree");
//		showHint.text = LanguageManager.instance.IdiomaKey("_comprarPremium");
		mainMenu.text = LanguageManager.instance.IdiomaKey("NoVideo") ;
    }
}
