﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelFinJuego : PanelBase
{
    const float DISTANCIA_RAYCAST = 2.6f;

    [Header("Textos a traducir")]
    public Text congratulations;
    public Text allLevels;
    public Text downloadGame;
    public Text accept;
    public Text cancel;

    void Start()
    {
        base.InicializarPanel(base.rotacionInicial);
        base.EstablecerDistanciaRayCast(DISTANCIA_RAYCAST);

        Niveles.instance.tengoQueCargarPantallaCompras = false;

        CambiarTextos();
    }

    public void Cancelar()
    {
        base.CerrarPanel();
    }

    public void AbrirTienda()
    {
        MetodosComunes.instance.DescargarPerfectAngle();
        base.CerrarPanel();
    }

    public void CambiarTextos()
    {
        congratulations.text = LanguageManager.instance.IdiomaKey("congratulations");
        allLevels.text = string.Format(LanguageManager.instance.IdiomaKey("allLevels"), "PERFECT ANGLE ZEN");
        downloadGame.text = LanguageManager.instance.IdiomaKey("downloadGame");
        accept.text = LanguageManager.instance.IdiomaKey("accept");
        cancel.text = LanguageManager.instance.IdiomaKey("cancel");
    }
}
