﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelJugarViendoVideo : PanelBase {
	const float DISTANCIA_RAYCAST = 5.6f;

    [Header("Textos a traducir")]
    public Text resume;
    public Text showHint;
    public Text mainMenu;
	public Text Cancelar;

    void Start()
    {
        base.InicializarPanel(base.rotacionInicial);
        base.EstablecerDistanciaRayCast(DISTANCIA_RAYCAST);

        CambiarTextos();
    }

    public void CambiarTextos()
    {
		resume.text = LanguageManager.instance.IdiomaKey("_playFree");
		showHint.text = string.Format(  LanguageManager.instance.IdiomaKey("_comprarPremium"), ComprasManager.instance.precioFullVersion );
		mainMenu.text = LanguageManager.instance.IdiomaKey("_explicaPremium");
		Cancelar.text = LanguageManager.instance.IdiomaKey("cancel");
    }

	public void MostrarUnVideo(){

		if( Niveles.instance.modoJuego != Niveles.ModoJuego.Normal || ComprasManager.instance.hemosCompradoElJuego ){
			Niveles.instance.puedoEnviarAlgunEvento = true;
			return;
		} 
		#if AMAZON
		Niveles.instance.puedoEnviarAlgunEvento = true;
		return;
		#endif
		if( !VersionManager.haPasadoRevisionDefault_FALSE ){
			Niveles.instance.puedoEnviarAlgunEvento = true;
			return;
		} 

		#if !UNITY_STANDALONE && !UNITY_TVOS
		if( AdsManager.AdsManagerScript.Hay_videos() ){

			Debug.Log("Hay VIDEOs") ;

			StartCoroutine( AdsManager.AdsManagerScript.PlayAVideo("null", value=>{
				if(value){
					Debug.Log("Video finalizado con Exito");
					PlayerPrefs.DeleteKey("TengoQueMostrarVideo");
					Niveles.instance.tengoQueMostrarVideo = false ;
					MetodosComunes.instance.PerfectAngle();
				}else{
					Debug.Log("Video no ha finalizado");
					ActionsButtons.instance.Accion("NO_VIDEOS_DISPONIBLES", gameObject);
				}

				Niveles.instance.puedoEnviarAlgunEvento = true;
			}) 
			);
			
		}else{
			Debug.Log("No hay videos");
			Niveles.instance.puedoEnviarAlgunEvento = true;
			ActionsButtons.instance.Accion("NO_VIDEOS_DISPONIBLES", gameObject);
		}
		#else
		#endif
    }

    public void BuyPremium(){
		if (!ComprasManager.instance.hemosCompradoElJuego)
            ComprasManager.instance.CompraGenericaDesDeVideo();

    }


}
