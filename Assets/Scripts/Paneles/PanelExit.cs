﻿using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class PanelExit : MonoBehaviour 
{
    public Text actionsExit;

	void Start () 
    {
        CambiarTextos();
	}
	
    public void CambiarTextos()
    {
        actionsExit.text = LanguageManager.instance.IdiomaKey("actionsExit");
    }
}
