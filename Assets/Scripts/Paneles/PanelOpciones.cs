﻿using UnityEngine;
using UnityEngine.UI;

public class PanelOpciones : MonoBehaviour 
{
    [Header("Textos")]
    public Text settingsMusic;
    public Text settingsSound;
    public Text settingsLanguage;
    public Text settingsLanguageName;
    public Text settingsGraphicsQuality;
    public Text settingsGraphicsQualityName;
    public Text settingsCardboard;
    public Text settingsResetValues;

    [Header("Imagenes")]
    public Image imageMusic;
    public Image imageSound;
    public Image imageSpectacles;

    private Sprite[] iconosOpciones; 

	void Start () 
    {
        CargarSprites();
        InicializarOpciones();
        CambiarTextos();
	}
	
    public void CambiarTextos()
    {
        settingsMusic.text = LanguageManager.instance.IdiomaKey("settingsMusic");
        settingsSound.text = LanguageManager.instance.IdiomaKey("settingsSound");
        settingsLanguage.text = LanguageManager.instance.IdiomaKey("settingsLanguage");
        settingsLanguageName.text = LanguageManager.instance.IdiomaKey(LanguageManager.instance.idiomaActual);
        settingsGraphicsQuality.text = LanguageManager.instance.IdiomaKey("settingsGraphicsQuality");
        settingsCardboard.text = LanguageManager.instance.IdiomaKey("settingsCardboard");
        settingsResetValues.text = LanguageManager.instance.IdiomaKey("settingsResetValues");
        InicializarCalidad();
    }

    private void CargarSprites()
    {
        iconosOpciones = Resources.LoadAll<Sprite>("Sprites/IconosOpciones");
    }

    private void InicializarOpciones()
    {
        InicializarMusica();
        InicializarSonidos();
        InicializarGafas();
    }

    private void InicializarMusica()
    {
        AudioManager.instance.Mute(0, !Niveles.instance.musica);           
        imageMusic.sprite = iconosOpciones[GetNumeroDeSpriteSegunNombreDelBoton("Music_" + (Niveles.instance.musica ? "ON" : "OFF").ToString())];
    }

    private void InicializarSonidos()
    {
        AudioManager.instance.Mute(1, !Niveles.instance.sonido);
        imageSound.sprite = iconosOpciones[GetNumeroDeSpriteSegunNombreDelBoton("Sound_" + (Niveles.instance.sonido ? "ON" : "OFF").ToString())];
    }

    private void InicializarGafas()
    {
        imageSpectacles.sprite = iconosOpciones[GetNumeroDeSpriteSegunNombreDelBoton("Spectacles_" + (Niveles.instance.modoJuego == Niveles.ModoJuego.VR ? "ON" : "OFF").ToString())];
    }

    private void InicializarCalidad()
    {
        if (Niveles.instance.modoJuego == Niveles.ModoJuego.VR)
            settingsGraphicsQualityName.text = LanguageManager.instance.IdiomaKey("settingsQualityLevel_" + Niveles.instance.nivelDeCalidadParaModoVR.ToString());
        else
            settingsGraphicsQualityName.text = LanguageManager.instance.IdiomaKey("settingsQualityLevel_" + Niveles.instance.nivelDeCalidadParaModoNormal.ToString());
    }

    public void Musica()
    {
        Niveles.instance.puedoEnviarAlgunEvento = true;
        Niveles.instance.musica = !Niveles.instance.musica;
        PlayerPrefs.SetInt("OpcionesMusica", !Niveles.instance.musica ? 0 : 1);

        InicializarMusica();
    }

    public void Sonido()
    {
        Niveles.instance.puedoEnviarAlgunEvento = true;
        Niveles.instance.sonido = !Niveles.instance.sonido;
        PlayerPrefs.SetInt("OpcionesSonidos", Niveles.instance.sonido ? 0 : 1);

        InicializarSonidos();
    }

    public void Gafas()
    {
        InicializarGafas();

        if (Niveles.instance.modoJuego == Niveles.ModoJuego.VR)
            Niveles.instance.modoJuego = Niveles.ModoJuego.Normal;
        else
            Niveles.instance.modoJuego = Niveles.ModoJuego.VR;

        MetodosComunes.instance.MenuEntrada(); 
    }

    public void Idioma()
    {
        Niveles.instance.puedoEnviarAlgunEvento = true;

        LanguageManager.instance.CambiarIdioma();
        gameObject.transform.Find("../PanelMenu").gameObject.GetComponent<PanelMenu>().CambiarTextos();
        gameObject.transform.Find("../PanelActions").gameObject.GetComponent<PanelAcciones>().CambiarTextos();
        gameObject.transform.Find("../PanelExit").gameObject.GetComponent<PanelExit>().CambiarTextos();
        CambiarTextos();
    }

    public void Calidad()
    {
        Niveles.instance.puedoEnviarAlgunEvento = true;

        if (Niveles.instance.modoJuego == Niveles.ModoJuego.VR)
        {
            Niveles.instance.nivelDeCalidadParaModoVR = Niveles.instance.nivelDeCalidadParaModoVR == 0 ? 1 : 0;
            PlayerPrefs.SetInt("QualityVR", Niveles.instance.nivelDeCalidadParaModoVR);
        }
        else
        {
            Niveles.instance.nivelDeCalidadParaModoNormal = Niveles.instance.nivelDeCalidadParaModoNormal == 0 ? 1 : 0;
            PlayerPrefs.SetInt("QualityNormal", Niveles.instance.nivelDeCalidadParaModoNormal);
        }

        InicializarCalidad();
    }

    public void ResetearValores()
    {
        Niveles.instance.numNivelAlcanzado = 0;
        PlayerPrefs.SetInt("NumeroNivelAlcanzado", 0);

        MetodosComunes.instance.MenuEntrada();
    }

    public int GetNumeroDeSpriteSegunNombreDelBoton(string nivel)
    {
        int n = 0;

        for (int m = 0; m < iconosOpciones.Length; m++)
        {
            if (iconosOpciones[m].name == nivel)
            {
                n = m;
                break;
            }
        }

        return n;
    }
}
