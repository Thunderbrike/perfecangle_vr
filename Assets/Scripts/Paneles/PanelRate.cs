﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelRate : MonoBehaviour
{
    public Text rate;
    
    void Start()
    {
        rate.text = LanguageManager.instance.IdiomaKey("rate");
    }

    public void Ratear()
    {
        MetodosComunes.instance.Ratear();
        MetodosComunes.instance.DestroyPanel(gameObject);
    }
}
