﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelSinPistas : PanelBase {

	public Text useHint;


    void Start()
    {
		useHint.text = LanguageManager.instance.IdiomaKey("_noHints");
    }

	public void MostrarPanelSinPistas()
    {
		if( Niveles.instance.modoJuego != Niveles.ModoJuego.Normal || ComprasManager.instance.hemosCompradoElJuego ) return;

        Camera.main.gameObject.GetComponent<GameManager>().sinPistas.SetActive(true);
        StartCoroutine(ApagarSinPistas());
    }

    IEnumerator ApagarSinPistas(){
    	yield return new WaitForSeconds(2);
		Niveles.instance.puedoEnviarAlgunEvento = true;
		//WIP:VIDEO
		//lanzar Mostrar video
		MostrarVideo();
//        Camera.main.gameObject.GetComponent<GameManager>().sinPistas.SetActive(false);
    }

	IEnumerator ApagarSinVideos(){
    	yield return new WaitForSeconds(2);
		Niveles.instance.puedoEnviarAlgunEvento = true;
		Camera.main.gameObject.GetComponent<GameManager>().sinVideos.SetActive(false);
		Camera.main.gameObject.GetComponent<GameManager>().sinPistas.SetActive(false);
    }


    void MostrarVideo(){
		//WIP:VIDEO
		//Mostrar video
		#if AMAZON
		return;
		#endif
		if( !VersionManager.haPasadoRevisionDefault_FALSE ) return;

		#if !UNITY_STANDALONE && !UNITY_TVOS
		if( AdsManager.AdsManagerScript.Hay_videos() ){

			Debug.Log("Hay VIDEOs") ;

			StartCoroutine( AdsManager.AdsManagerScript.PlayAVideo("null", value=>{
				if(value){
					Debug.Log("Video finalizado con Exito");
					GameManager.numPistas = 2;
					PlayerPrefs.SetInt("Pistas", GameManager.numPistas );
					Camera.main.gameObject.GetComponent<GameManager>().numeroDePistasQueMeQuedan.text = GameManager.numPistas.ToString();
					PlayerPrefs.SetString(Niveles.instance.numPuzzle+"_"+GameManager.numeroDePistasMostradas,"");
					GameManager.numeroDePistasMostradas++;
					Debug.Log("numeroDePistasMostradas: "+GameManager.numeroDePistasMostradas);
					ActionsButtons.instance.Accion("MENU_PISTA", Camera.main.gameObject.GetComponent<GameManager>().canvas);
					AudioManager.instance.PlayAudio(1, Resources.Load("Audios/Efectos/MagicPA") as AudioClip, 0.5f);
					Camera.main.gameObject.GetComponent<GameManager>().sinPistas.SetActive(false);
				}else{
					Debug.Log("Video no ha finalizado");
					Niveles.instance.puedoEnviarAlgunEvento = false;
					Camera.main.gameObject.GetComponent<GameManager>().sinPistas.SetActive(false);
				}
			}) 
			);

		}else{
			Debug.Log("No hay videos");
			Camera.main.gameObject.GetComponent<GameManager>().sinVideos.SetActive(true);
			StartCoroutine( ApagarSinVideos() );
		}
		#else


		#endif
    }
}
