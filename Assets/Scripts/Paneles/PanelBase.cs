﻿using UnityEngine;
using System.Collections;

public class PanelBase : MonoBehaviour 
{
    protected GameObject mainCamera;
    protected float distanciaOriginalRayCast;
    public int rotacionInicial;

    private Quaternion rotation = Quaternion.identity;
    private float rot;

    void Start()
    {
        InicializarPanel(0); 
    }

    protected void InicializarPanel(float rotIni)
    {
        mainCamera = Camera.main.gameObject;

        //Posicionamos el panel justo en donde estamos mirando pero centrado en altura
        gameObject.transform.position = mainCamera.transform.position;

        // Rotamos el panel dependiendo del modo de juego y de la escena en donde nos encontremos
        RotarPanelesSegunEscena(rotIni);
        
        // Acivamos la variable para que no cree ningún otro panel hasta que no haya desparecido este y reseteamos la variable de carga del Panel por si venimos del juego
        Niveles.instance.hayUnPanelActivado = true;

        Niveles.instance.tengoQueCargarPantallaCompras = false;
    }

    protected void EstablecerDistanciaRayCast(float distancia)
    {
        //Recojo la distancia acutal de la detección de botones para establecerla cuando cierre el panel
        distanciaOriginalRayCast = mainCamera.GetComponent<DeteccionBoton>().rayCastDistance;

        //Cambiamos el rango del raycast de la detección de botones para que no detecte los que están por detrás del panel
        mainCamera.GetComponent<DeteccionBoton>().rayCastDistance = distancia;
    }

    protected void CerrarPanel()
    {
        // Establezco la distancia anterior del RayCast y me cargo el panel
        mainCamera.GetComponent<DeteccionBoton>().rayCastDistance = distanciaOriginalRayCast;
        Niveles.instance.hayUnPanelActivado = false;
        Niveles.instance.puedoEnviarAlgunEvento = true;

        Destroy(gameObject);
    }

    private void RotarPanelesSegunEscena(float rotIni)
    {
        //Debug.Log("tengoQueCargarPantallaCompras: " + Niveles.instance.tengoQueCargarPantallaCompras);
        // Parche NECESARIO para girar 180 grados el panel de compras cuando venimos de la escena "Perfect Angle"
        float addRot = 0;
        if (Niveles.instance.tengoQueCargarPantallaCompras && Niveles.instance.modoJuego == Niveles.ModoJuego.VR) addRot = 180;

        // Código normal para la rotación inicial de los paneles (y le sumo la rotación anterior)
        rot = mainCamera.transform.localRotation.eulerAngles.y + rotIni + addRot;
        rotation.eulerAngles = new Vector3(0, rot, 0);
        gameObject.transform.rotation = rotation;
    }
}
