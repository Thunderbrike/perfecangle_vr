﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelInfo : PanelBase
{
    const float DISTANCIA_RAYCAST = 2.6f;

    [Header("Textos a traducir")]
    public Text slideYourFinger;
    public Text moveTourHead;
    public Text diveIntoPool;
    public Text accept;

    void Start()
    {
        base.InicializarPanel(base.rotacionInicial);
        base.EstablecerDistanciaRayCast(DISTANCIA_RAYCAST);

        Niveles.instance.tengoQueCargarPantallaCompras = false;

        CambiarTextos();
    }

    public void EmpiezoAJugar()
    {
        mainCamera.GetComponent<GameManager>().EmpiezoAJugar();
        Niveles.instance.esLaPrimeraVezQueJuego = false;
        Niveles.instance.puedoEnviarAlgunEvento = true;
        Destroy(gameObject);
    }

    public void CambiarTextos()
    {
        if (slideYourFinger != null) slideYourFinger.text = LanguageManager.instance.IdiomaKey("slideYourFinger");
        if (moveTourHead != null) moveTourHead.text = string.Format(LanguageManager.instance.IdiomaKey("moveTourHead"), "PERFECT ANGLE");
        if (diveIntoPool != null) diveIntoPool.text = LanguageManager.instance.IdiomaKey("diveIntoPool");
        accept.text = LanguageManager.instance.IdiomaKey("accept");
    }
}
