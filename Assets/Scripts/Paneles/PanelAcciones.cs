﻿using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class PanelAcciones : MonoBehaviour 
{
    [DllImport("__Internal")]
    private static extern void sendMailText(string arr1, string arr2, string arr3);

    [Header("Textos")]
    public Text actionsSupport;
    public Text actionsFacebook;
    public Text actionsTwitter;
    public Text actionsRecommend;
    public Text actionsRate;

    private Sprite[] iconosOpciones; 

	void Start () 
    {
        CambiarTextos();
	}
	
    public void CambiarTextos()
    {
        actionsSupport.text = LanguageManager.instance.IdiomaKey("actionsSupport");
        actionsFacebook.text = LanguageManager.instance.IdiomaKey("actionsFacebook");
        actionsTwitter.text = LanguageManager.instance.IdiomaKey("actionsTwitter");
        actionsRecommend.text = LanguageManager.instance.IdiomaKey("actionsRecommend");
        actionsRate.text = LanguageManager.instance.IdiomaKey("actionsRate");
    }

    public void Soporte()
    {
        Niveles.instance.puedoEnviarAlgunEvento = true;

        SendMail(DatosDelJuego(), "Support Perfect Angle VR", "support@ivanovichgames.com");
    }

    public void Facebook()
    {
        Niveles.instance.puedoEnviarAlgunEvento = true;
        Application.OpenURL("https://www.facebook.com/Perfect-Angle-1703523763209535/");
    }

    public void Twitter()
    {
        Niveles.instance.puedoEnviarAlgunEvento = true;
        Application.OpenURL("https://twitter.com/PerfectAngleIG");
    }

    public void Recomienda()
    {
        Niveles.instance.puedoEnviarAlgunEvento = true;
        SendMail(LanguageManager.instance.IdiomaKey("CompartirPAfinal"), "Perfect Angle VR");
    }

    public static void SendEmail(string extraData, string subject, string email)
    {
#if UNITY_ANDROID		
		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		intentObject.Call<AndroidJavaObject>("setType", "message/rfc822");
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_EMAIL"), new string[] { email });
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), subject);
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), extraData);				
		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");		
		currentActivity.Call("startActivity", intentObject);		
#endif
    }

    static string DatosDelJuego()
    {
        string text = "";
        text += "\n";
        text += "\n";
        text += "\n";
        text += "\n";
        text += "\n";
        text += "*-------------DEVICE INFO:----------------- \n";
        text += "Device Type: " + SystemInfo.deviceType + "\n";
        text += "Model Name: " + SystemInfo.deviceModel + "\n";
        text += "OS: " + SystemInfo.operatingSystem + "\n";
        text += "CPU: " + SystemInfo.processorType + " x " + SystemInfo.processorCount + "\n";
        text += "Memory: " + SystemInfo.systemMemorySize.ToString("#,0") + "kB (VRAM:" + SystemInfo.graphicsMemorySize.ToString("#,0") + "kB)\n";
        text += "GPU: " + SystemInfo.graphicsDeviceName + " (ID:" + SystemInfo.graphicsDeviceID + ")\n";
        text += "Vendor: " + SystemInfo.graphicsDeviceVendor + " (ID:" + SystemInfo.graphicsDeviceVendorID + ")\n";
        text += "Driver: " + SystemInfo.graphicsDeviceVersion + " (SM:" + SystemInfo.graphicsShaderLevel + ")\n";
        text += "Resolution: " + Screen.width + " x " + Screen.height + "\n";
        text += "*--------------- END INFO ----------------- \n";
        return text;
    }

    void SendMail(string extraData="", string subject="", string email="")
    {
#if UNITY_STANDALONE || UNITY_EDITOR
        Application.OpenURL("mailto:" + email);
#elif UNITY_ANDROID
		SendEmail(extraData, subject, email);
#elif UNITY_IPHONE
		sendMailText(extraData, subject, email);
#endif
    }
}
