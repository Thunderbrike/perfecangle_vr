﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelFinCompra : PanelBase
{
    public Text descripcion;
    public Text boton;

    const float DISTANCIA_RAYCAST = 2.6f;

    void Start()
    {
        base.InicializarPanel(0);
        base.EstablecerDistanciaRayCast(DISTANCIA_RAYCAST);

        Niveles.instance.tengoQueCargarPantallaCompras = false;
    }

    public void Cancelar()
    {
        base.CerrarPanel();
    }
}
