﻿using UnityEngine;
using UnityEngine.UI;

public class PanelMenu : MonoBehaviour 
{
    public GameObject botonBuy;
    public GameObject botonRestore;
    [Space]
    public Material panelAndroid;
    public Material paneliOS;
    [Space]
    public Text gameModes;
    public Text levels;
    public Text storyIncluded;
    public Text levelsIncluded;
    public Text download;
    public Text buy;
    public Text restorePurchases;

	void Start () 
    {
        CambiarTextos();
        // Sacamos el botón de RESTORE siempre que no sea la versión de iOS y cambiamos el material del panel
#if UNITY_IPHONE
        botonRestore.SetActive(true);
        gameObject.transform.Find("PanelMenuFront").gameObject.GetComponent<MeshRenderer>().material = paneliOS;
#else
        botonRestore.SetActive(false);
        gameObject.transform.Find("PanelMenuFront").gameObject.GetComponent<MeshRenderer>().material = panelAndroid;
#endif

        // Cambiamos el texto en el botón de BUY para la versión de AMAZON
#if AMAZON
        if (Niveles.instance.modoJuego == Niveles.ModoJuego.VR)
            botonBuy.GetComponent<Timer>().accion = "INCLUDED";
        else
            botonBuy.GetComponent<BotonMenuPanel>().accion = "INCLUDED";
#else
        if (Niveles.instance.modoJuego == Niveles.ModoJuego.VR)
            botonBuy.GetComponent<Timer>().accion = "BUY";
        else
            botonBuy.GetComponent<BotonMenuPanel>().accion = "BUY";
#endif
    }
	
    public void CambiarTextos()
    {
        gameModes.text = LanguageManager.instance.IdiomaKey("gameModes");
        levels.text = LanguageManager.instance.IdiomaKey("levels");
        storyIncluded.text = LanguageManager.instance.IdiomaKey("storyIncluded");
        levelsIncluded.text = LanguageManager.instance.IdiomaKey("levelsIncluded");
        download.text = LanguageManager.instance.IdiomaKey("download");
        buy.text = LanguageManager.instance.IdiomaKey(ComprasManager.instance.hemosCompradoElJuego ? "included" : "buy");
        restorePurchases.text = LanguageManager.instance.IdiomaKey("restorePurchases");
    }

    public void RestaurarCompras()
    {
        ComprasManager.instance.Restaurar();
    }

    public void CompraGenerica()
    {
        Niveles.instance.hayUnPanelActivado = false;
        Niveles.instance.puedoEnviarAlgunEvento = true;

        if (!ComprasManager.instance.hemosCompradoElJuego)
            ComprasManager.instance.CompraGenerica();
    }

    public void Included()
    {
        Niveles.instance.puedoEnviarAlgunEvento = true;
        GameObject panelAmazon = Instantiate(Resources.Load("Prefabs/PanelAmazon" + (Niveles.instance.modoJuego == Niveles.ModoJuego.VR ? "VR" : "Normal")), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
        GameObject.Find("Camera").gameObject.GetComponent<MouseOrbit>().enabled = false;
    }
}
