﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelAyuda : MonoBehaviour
{
    public Text useHint;
    
    void Start()
    {
        useHint.text = LanguageManager.instance.IdiomaKey("clues");
    }
}
