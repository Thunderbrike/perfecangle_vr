﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelPista : PanelBase {

    const float DISTANCIA_RAYCAST = 5.6f;
    SpriteRenderer spriteRenderer;

    [Header("Textos a traducir")]
    public Text descripcion;
    public Text accept;

    void Start()
    {
        base.InicializarPanel(base.rotacionInicial);
        base.EstablecerDistanciaRayCast(DISTANCIA_RAYCAST);

        spriteRenderer = gameObject.transform.Find("Hint").gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = Niveles.instance.iconoNivel[Niveles.instance.GetNumeroDeSpriteSegunNombreDelNivel(Niveles.instance.nombreNivel[Niveles.instance.numPuzzle])];

        if (Niveles.instance.tipoPista == Niveles.TipoPista.Forma)
        {
            spriteRenderer.color = Color.white;
            CambiarTextos();
        }
        else
        {
            spriteRenderer.color = new Color(0.32f, 0.85f, 0.16f);
            CambiarTextos();
        }
    }

    public void CambiarTextos()
    {
        descripcion.text = (Niveles.instance.tipoPista == Niveles.TipoPista.Forma) ? LanguageManager.instance.IdiomaKey("shapeClue") : LanguageManager.instance.IdiomaKey("colorClue");
        accept.text = LanguageManager.instance.IdiomaKey("accept");
    }
}
