﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelGyroscope : MonoBehaviour
{
    public Text gyroscope;
    
    void Start()
    {
        gyroscope.text = LanguageManager.instance.IdiomaKey("gyroscope");
    }
}
