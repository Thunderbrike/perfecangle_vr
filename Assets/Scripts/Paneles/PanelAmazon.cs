﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelAmazon : PanelBase
{
    public Text descripcion;
    public Text boton;

    const float DISTANCIA_RAYCAST = 2.6f;

    void Start()
    {
        CambiarTextos();

        base.InicializarPanel(base.rotacionInicial);
        base.EstablecerDistanciaRayCast(DISTANCIA_RAYCAST);
    }

    public void CambiarTextos()
    {
        descripcion.text = LanguageManager.instance.IdiomaKey("amazonZEN");
        boton.text = LanguageManager.instance.IdiomaKey("accept");
    }

    public void OnDestroy()
    {
        base.CerrarPanel();
    }
}
