﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelNoVideo : MonoBehaviour {

	public Text useHint;
    
    void Start()
    {
		useHint.text = LanguageManager.instance.IdiomaKey("NoVideo");
    }
}
