﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelUrl : PanelBase {
	const float DISTANCIA_RAYCAST = 5.6f;

    [Header("Textos a traducir")]
    public Text resume;
    public Text showHint;
    public Text mainMenu;

    void Start()
    {
        base.InicializarPanel(base.rotacionInicial);
        base.EstablecerDistanciaRayCast(DISTANCIA_RAYCAST);

        CambiarTextos();
    }

    public void CambiarTextos()
    {
        resume.text = LanguageManager.instance.IdiomaKey("resume");
        showHint.text = BotonBase.Url_Nombre; 

    }
}
