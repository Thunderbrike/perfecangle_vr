﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelCompras : PanelBase 
{
    const float DISTANCIA_RAYCAST = 2.6f;

    [Header("Textos a traducir")]
    public Text congratulations;
    public Text allLevels;
    public Text ifYouliked;
    public Text downloadGame;
    public Text accept;
    public Text cancel;

    void Start()
    {
        base.InicializarPanel(base.rotacionInicial);
        base.EstablecerDistanciaRayCast(DISTANCIA_RAYCAST);

        CambiarTextos();
    }

    public void Cancelar()
    {
        base.CerrarPanel();
    }

    public void CompraGenerica()
    {
        if (!ComprasManager.instance.hemosCompradoElJuego)
            ComprasManager.instance.CompraGenerica();

        base.CerrarPanel();
    }

    public void CambiarTextos()
    {
        congratulations.text = LanguageManager.instance.IdiomaKey("congratulations");
        allLevels.text = string.Format(LanguageManager.instance.IdiomaKey("allLevels"), "PERFECT ANGLE ZEN");
        ifYouliked.text = string.Format(LanguageManager.instance.IdiomaKey("ifYouliked"), ComprasManager.instance.precioFullVersion);
        accept.text = LanguageManager.instance.IdiomaKey("accept");
        cancel.text = LanguageManager.instance.IdiomaKey("cancel");
    }
}
