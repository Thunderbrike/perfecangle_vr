﻿using UnityEngine;
using System.Collections;

public class AguaShader : MonoBehaviour {

    public Vector2 mainTexture1, mainTexture2;
    public Vector2 bumpTexture1, bumpTexture2;
    [Range (0.0f, 0.1f)]
    public float amplitud = 0.05f;
    [Range(0.0f, 10.0f)]
    public float longitud = 2.5f;
    [Range(0.0f, 300f)]
    public float frecuencia = 50f;

    public bool specularMap;

    Vector2 mt1, mt2;
    Vector2 bt1, bt2;

    void Start()
    {
        gameObject.GetComponent<Renderer>().materials[0].SetFloat("_Amplitude", amplitud);
        gameObject.GetComponent<Renderer>().materials[0].SetFloat("_Length", longitud);
        gameObject.GetComponent<Renderer>().materials[0].SetFloat("_Frequency", frecuencia);

        if (specularMap)
        {
            gameObject.GetComponent<Renderer>().materials[1].SetFloat("_Amplitude", amplitud);
            gameObject.GetComponent<Renderer>().materials[1].SetFloat("_Length", longitud);
            gameObject.GetComponent<Renderer>().materials[1].SetFloat("_Frequency", frecuencia);
        }
    }

	void Update ()
    {
        mt1 += new Vector2(Time.deltaTime * mainTexture1.x / 100, Time.deltaTime * mainTexture1.y / 100);
        mt2 += new Vector2(Time.deltaTime * mainTexture2.x / 100, Time.deltaTime * mainTexture2.y / 100);
        bt1 += new Vector2(Time.deltaTime * bumpTexture1.x / 100, Time.deltaTime * bumpTexture1.y / 100);
        bt2 += new Vector2(Time.deltaTime * bumpTexture2.x / 100, Time.deltaTime * bumpTexture2.y / 100);

        gameObject.GetComponent<Renderer>().materials[0].SetTextureOffset("_MainTex", mt1);
        gameObject.GetComponent<Renderer>().materials[0].SetTextureOffset("_MainTex2", mt2);
        gameObject.GetComponent<Renderer>().materials[0].SetTextureOffset("_BumpMap", bt1);
        gameObject.GetComponent<Renderer>().materials[0].SetTextureOffset("_BumpMap2", bt2);

        if (specularMap)
        {
            gameObject.GetComponent<Renderer>().materials[1].SetTextureOffset("_MainTex", mt1);
            gameObject.GetComponent<Renderer>().materials[1].SetTextureOffset("_BumpMap", bt1);
            gameObject.GetComponent<Renderer>().materials[1].SetTextureOffset("_BumpMap2", bt2);
        }
	}
}