﻿using UnityEngine;
using System.Collections;

public class DebajoAgua : MonoBehaviour
{
    const float POSICION_AGUA = 0.05f;
    const float VELOCIDAD_EFECTO_AGUA = 0.65f;
    const float PITCH_DENTRO_AGUA = 0.75f;

    static public GameObject pedestal, pedestal_UW;

    private Vector3 posInicialAgua;
    private float posLimiteAgua = 0;
    private GameObject mainCamera;
    private Canvas canvas;
    private bool estoyAbajo, estoyArriba;
    private bool efectoEntrarEnElAgua = false;
    private bool efectoSalirDelAgua = false;
    private CameraFilterPack_AAA_WaterDrop efectoDrop;

	void Start ()
    {
        estoyArriba = true;
        pedestal_UW.SetActive(false);
        mainCamera = Camera.main.gameObject;
        canvas = mainCamera.transform.Find("Agua").gameObject.GetComponent<Canvas>();
        efectoDrop = mainCamera.GetComponent<CameraFilterPack_AAA_WaterDrop>();
        posInicialAgua = gameObject.transform.position;

        if (Niveles.instance.modoJuego == Niveles.ModoJuego.VR)
            canvas.renderMode = RenderMode.WorldSpace;
	}

	void Update ()
    {
        if (mainCamera.transform.position.y >= posInicialAgua.y) {
            if (!estoyArriba) Arriba();
        }
        else {
            if (!estoyAbajo) Abajo();
        }

        if (efectoSalirDelAgua) EfectoSalirDelAgua();
	}


    float DistanciaCameraAgua()
    {
        return mainCamera.transform.position.y - gameObject.transform.position.y;
    }

    void Abajo()
    {
        estoyAbajo = true;
        estoyArriba = false;

        efectoDrop.enabled = true;

        gameObject.transform.position = new Vector3(0, posInicialAgua.y + POSICION_AGUA, 0);
        pedestal_UW.SetActive(true);
        pedestal.SetActive(false);
        posLimiteAgua = posInicialAgua.y - POSICION_AGUA;
        canvas.enabled = true;
        efectoEntrarEnElAgua = true;
    }

    void Arriba()
    {
        estoyArriba = true;
        estoyAbajo = false;

        efectoDrop.enabled = true;
        CameraFilterPack_AAA_WaterDrop.ChangeDistortion = 8;
        efectoSalirDelAgua = true;
        efectoEntrarEnElAgua = false;

        gameObject.transform.position = posInicialAgua;
        pedestal_UW.SetActive(false);
        pedestal.SetActive(true);
        posLimiteAgua = 0;
        canvas.enabled = false;

        AudioManager.instance.PlayAudio(0, Resources.Load("Audios/Efectos/SaliDelAgua") as AudioClip, 0.25f);
    }

    void EfectoSalirDelAgua()
    {
        CameraFilterPack_AAA_WaterDrop.ChangeDistortion += VELOCIDAD_EFECTO_AGUA;

        if (CameraFilterPack_AAA_WaterDrop.ChangeDistortion >= 100)
        {
            efectoDrop.enabled = false;
            efectoSalirDelAgua = false;
        }
    }
}
