﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class CreateSunShaft : MonoBehaviour {

    GameObject camLeft, camRight;

	void Start () {
        camLeft = transform.Find("Camera Left").gameObject;
        camRight = transform.Find("Camera Right").gameObject;

        camLeft.AddComponent<SunShafts>();
	}
	
	// Update is called once per frame
	void Update () {
        //camLeft.GetComponent<SunShafts>().enabled = true;
	}
}
