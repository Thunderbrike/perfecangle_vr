﻿using UnityEngine;
using System.Collections;

public class CanvasFade : MonoBehaviour
{
    public float canvasInitValue;
    public float canvasFinalValue;
    public float durationValue;

    CanvasGroup canvas;
    float counter;

	void Start ()
    {
        canvas = gameObject.GetComponent<CanvasGroup>() as CanvasGroup;
        canvas.alpha = canvasInitValue;
        canvas.blocksRaycasts = false;
	}
	
	void Update ()
    {
        counter += Time.deltaTime / durationValue;
        canvas.alpha = Mathf.Lerp(canvasInitValue, canvasFinalValue, counter);
        Niveles.instance.puedoEnviarAlgunEvento = false;

        if (canvas.alpha == canvasFinalValue)
        {
            Niveles.instance.puedoEnviarAlgunEvento = true;

            canvas.blocksRaycasts = (canvas.alpha == 0 ? false : true);
            Destroy(this);
        }
	}
}