﻿using UnityEngine;

public class FadeOut : MonoBehaviour {

    const float TIEMPO_DEL_FADE = 1.0f; // En segundos

    private CanvasGroup canvas;

    void Start()
    {
        canvas = gameObject.GetComponent<CanvasGroup>();
    }

    void Update()
    {
        canvas.alpha -= Time.deltaTime / TIEMPO_DEL_FADE;

        if (GetComponent<CanvasGroup>().alpha <= 0)
        {
            Niveles.instance.puedoEnviarAlgunEvento = true;
            Destroy(gameObject);
        }
    }
}
