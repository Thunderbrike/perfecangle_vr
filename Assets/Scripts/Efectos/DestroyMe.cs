﻿using UnityEngine;
using System.Collections;

public class DestroyMe : MonoBehaviour {

    public float timeToDestroyMe;	
	
    void Start()
    {
        StartCoroutine(DestroyGameObject());
    }

    IEnumerator DestroyGameObject()
    {
        yield return new WaitForSeconds(timeToDestroyMe);
        Destroy(gameObject);
    }
}
