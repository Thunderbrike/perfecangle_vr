﻿using UnityEngine;

public class FadeIn : MonoBehaviour {

    const float TIEMPO_DEL_FADE = 1.0f; // En segundos

    private bool fadeIn = false;
    private CanvasGroup canvas;
    private string nuevaEscena;

    void Start()
    {
        canvas = gameObject.GetComponent<CanvasGroup>();
    }

    void Update()
    {
        if (fadeIn)
        { 
            canvas.alpha += Time.deltaTime / TIEMPO_DEL_FADE;

            if (GetComponent<CanvasGroup>().alpha >= 1)
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(nuevaEscena);
            }
        }
    }

    public void CambioEscena(string nombreEscena)
    {
        fadeIn = true;
        //Niveles.instance.puedoEnviarAlgunEvento = false;
        nuevaEscena = nombreEscena;
	}
}
