﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Exit : MonoBehaviour
{
    const float TIEMPO_EN_EXIT = 1.5f;

    private float timeInSolution;

    void OnTriggerEnter(Collider item)		// En cuanto entramos en el collider activamos la cuenta atras
    {
        if (item.tag == "MainCamera")
        {
            timeInSolution = TIEMPO_EN_EXIT;
        }
    }

    void OnTriggerExit(Collider item)		// Si salimos de la solucion inicializamos la cuenta atras
    {
        if (item.tag == "MainCamera")
        {
            timeInSolution = TIEMPO_EN_EXIT;
        }
    }

    void OnTriggerStay(Collider item)		// Mientras estemos dentro de la solucion, restamos el tiempo
    {
        // Si estamos dentro del collider ...
        if (item.tag == "MainCamera")
        {
            // ... vamos restando el tiempo hasta agotar el contador ...
            if (timeInSolution > 0)
            {
                timeInSolution -= Time.deltaTime;
            }
            // ... y si seguimos dentro y el tiempo de contador ha llegado a 0, nos vamos
            else
            {
                if(Niveles.instance.modoJuego == Niveles.ModoJuego.VR)
                    ActionsButtons.instance.Accion("MENU_PAUSA", gameObject);
            }
        }
    }
}
