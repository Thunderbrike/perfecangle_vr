﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : BotonBase {

    //public string accion;
    public float tiempoTrigger = 1.0f;

    private float counter = 0;
    private bool accionEnviada = false;
    Image trigger;

	void Start ()
    {
        trigger = gameObject.transform.Find("Timer").GetComponent<Image>();
	}
	
	void Update () 
    {
        if (estoyEnFoco && !accionEnviada)
        {
            counter += Time.deltaTime / tiempoTrigger;

            trigger.fillAmount = counter;

            if (counter > 1)
            {
                accionEnviada = true;
                ActionsButtons.instance.Accion(accion, panel);
            }
        }
        else
        {
            accionEnviada = false;
            counter = 0;
            trigger.fillAmount = counter;
        }
	}

    public override void Accion()
    {
        Debug.Log("Mi acción como Timer");
    }
}
