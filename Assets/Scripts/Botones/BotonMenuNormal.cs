﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BotonMenuNormal : BotonBase 
{
	void Update () 
    {
        if (base.estoyEnFoco && !Niveles.instance.hayUnPanelActivado)
        {
            if (Input.GetMouseButtonUp(0) && DeteccionBoton.distance < 0.3f)
            {
                base.CargarNivel();
            }
        }
	}

    public override void Accion()
    {
        Debug.Log("Mi acción como Boton Menú Normal");
    }
}
