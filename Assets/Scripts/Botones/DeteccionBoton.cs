﻿using UnityEngine;

public class DeteccionBoton : MonoBehaviour {

    [HideInInspector]
    public float _rayCastDistance;

    public float rayCastDistance
    {
        get { return _rayCastDistance; }
        set { _rayCastDistance = value; }
    }

    static public float distance;

    private GameObject boton, botonActual;
    private float firstPoint;
    
	void Start () 
    {
        botonActual = null;
	}

	void Update () 
    {
        DetectarBoton();
        DetectarClickODrag();
	}

    void DetectarBoton()
    {
        Vector3 centroPantalla = new Vector3(Screen.width / 2, Screen.height / 2, 0);

        Ray ray = Camera.main.ScreenPointToRay(Niveles.instance.modoJuego == Niveles.ModoJuego.VR ? centroPantalla : Input.mousePosition);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, rayCastDistance) && Niveles.instance.puedoEnviarAlgunEvento)
        {
            Debug.DrawLine(ray.origin, hit.point, Color.green);
            boton = hit.transform.gameObject;
            
            if (boton != botonActual)
            {
                if (botonActual != null){
					if( botonActual.GetComponent<BotonBase>()!=null ) botonActual.GetComponent<BotonBase>().estoyEnFoco = false;
                }

				if( boton.GetComponent<BotonBase>()!=null ) boton.GetComponent<BotonBase>().estoyEnFoco = true;
                //boton.GetComponent<BotonBase>().Accion();
                botonActual = boton;
            }
        }
        else
        {
            botonActual = null;

            if (boton != null){
				if( boton.GetComponent<BotonBase>()!=null ) boton.GetComponent<BotonBase>().estoyEnFoco = false;
			}
        }
    }

    void DetectarClickODrag()
    {
        if (Input.GetMouseButtonDown(0))
            firstPoint = gameObject.transform.rotation.eulerAngles.y;

        if (Input.GetMouseButtonUp(0))
            distance = Mathf.Abs(gameObject.transform.rotation.eulerAngles.y - firstPoint);
    }
}
