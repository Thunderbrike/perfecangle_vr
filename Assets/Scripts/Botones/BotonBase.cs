﻿using UnityEngine;
using System.Collections;

public class BotonBase : MonoBehaviour {

    [HideInInspector]
    public bool estoyEnFoco = false;
    public string accion;
    public GameObject panel;
	public string Url;
	public string UrlJuego;

	public static string Url_destino;
	public static string Url_Nombre;

    public virtual void Accion(){
		if( accion=="MENU_PISTA" ){
			if( GameManager.numeroDePistasMostradas>1 || !Niveles.instance.puedoEnviarAlgunEvento ){
				return;
			}

//			Niveles.instance.puedoEnviarAlgunEvento = false;
			bool tengoQueGastarPista=HeUtilizadoEstaPistaEnEsteNivel(Niveles.instance.numPuzzle ,  GameManager.numeroDePistasMostradas );

			if( !ComprasManager.instance.hemosCompradoElJuego && Niveles.instance.modoJuego == Niveles.ModoJuego.Normal ){
				if( !tengoQueGastarPista ){
					if( GameManager.numPistas >0 ){
						GameManager.numPistas--;
						PlayerPrefs.SetInt("Pistas", GameManager.numPistas );
						PlayerPrefs.SetString(Niveles.instance.numPuzzle+"_"+GameManager.numeroDePistasMostradas,"");
						Camera.main.gameObject.GetComponent<GameManager>().numeroDePistasQueMeQuedan.text = GameManager.numPistas.ToString();
						ActionsButtons.instance.Accion(accion, panel);
				    	DispararSonido();
						GameManager.numeroDePistasMostradas++;
						Debug.Log("numeroDePistasMostradas: "+GameManager.numeroDePistasMostradas);
				    }else{
				   		//mostrar alerta: "Pistas insuficientes, mostrando video" y mostramos un video
						ActionsButtons.instance.Accion("sinPistas", panel);
				    }
				}else{
					ActionsButtons.instance.Accion(accion, panel);
				    DispararSonido();
					GameManager.numeroDePistasMostradas++;
					Debug.Log("numeroDePistasMostradas: "+GameManager.numeroDePistasMostradas);
				}
			}else{
				ActionsButtons.instance.Accion(accion, panel);
		    	DispararSonido();
			}


		}else{
			ActionsButtons.instance.Accion(accion, panel);
		    DispararSonido();
		}

    }

    bool HeUtilizadoEstaPistaEnEsteNivel(int numeroDePuzzle , int numeroDePista){
		return PlayerPrefs.HasKey(numeroDePuzzle+"_"+numeroDePista);
    }

    protected void CargarNivel()
    {
        Niveles.instance.numPuzzle = gameObject.GetComponent<ReturnNumeroNivel>().numLevel;
        Niveles.instance.vengoDelMenuEntrada = true;

		if( gameObject.GetComponent<BotonMenuNormal>()!=null ) Url_destino=gameObject.GetComponent<BotonMenuNormal>().Url;
		if( gameObject.GetComponent<BotonMenuNormal>()!=null ) Url_Nombre=gameObject.GetComponent<BotonMenuNormal>().UrlJuego;


        // Miramos si el jugador ha comprado el juego y si el nivel alcanzado está dentro de los niveles jugables
        if ((Niveles.instance.numPuzzle == Niveles.instance.maximoNivelFree) && 
			(Niveles.instance.modoJuego == Niveles.ModoJuego.VR) )
        {
            if (!ComprasManager.instance.hemosCompradoElJuego)
            {
                GameObject panelCompra = Instantiate(Resources.Load("Prefabs/PanelCompra" + (Niveles.instance.modoJuego == Niveles.ModoJuego.VR ? "VR" : "Normal")), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
                return;
            }
        }

		if( Niveles.instance.modoJuego == Niveles.ModoJuego.Normal && !ComprasManager.instance.hemosCompradoElJuego ){
	        if (Niveles.instance.tengoQueMostrarVideo && Niveles.instance.numNivelAlcanzado == Niveles.instance.numPuzzle ) {
	       		MetodosComunes.instance.MostrarPublicidad();
	       		return;
	       	}
		}

        if (Niveles.instance.numPuzzle > Niveles.instance.numNivelAlcanzado)
            Debug.Log("NIVEL BLOQUEADO");
        else
        {
			
            GetComponentInChildren<SpriteRenderer>().color = Niveles.instance.colorBotonesPresionados;
			if( Niveles.instance.modoJuego == Niveles.ModoJuego.Normal ){
				if( Url_destino!=""){
					ActionsButtons.instance.Accion("MENU_URL", gameObject);
	           		DispararSonido();
	            }else{
					ActionsButtons.instance.Accion(accion, gameObject);
	           		DispararSonido();
	            }
	        }else{
				ActionsButtons.instance.Accion(accion, gameObject);
	           	DispararSonido();
	        }
           
        }
    }

    protected void DispararSonido()
    {
        AudioManager.instance.PlayAudio(1, Resources.Load("Audios/Efectos/MagicPA") as AudioClip, 0.5f);
    }

	
}
