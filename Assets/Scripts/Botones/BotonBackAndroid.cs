﻿using UnityEngine;

public class BotonBackAndroid : MonoBehaviour
{
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

	void Update ()
    {
	    if (Input.GetKeyDown(KeyCode.Escape))
        {
            switch (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name)
            {
                case "Intro": MetodosComunes.instance.ExitGame(); break;
                case "ModoJuego": MetodosComunes.instance.ExitGame(); break;
                case "MenuEntrada": MetodosComunes.instance.ModoDeJuego(); break;
                case "MenuPausa": MetodosComunes.instance.PerfectAngle(); break;
                case "MenuPista": MetodosComunes.instance.PerfectAngle(); break;
                case "PerfectAngle": MetodosComunes.instance.MenuEntrada(); break;
				case "MenuJugarViendoVideo": MetodosComunes.instance.MenuEntrada(); break;
            }
        }
	}
}