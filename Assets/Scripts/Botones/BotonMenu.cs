﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BotonMenu : BotonBase {

    
    [Tooltip("La distancia va de 0 a 1, donde 1 es la posición origen y 0 el origen de coordenadas. Si queremos situarlo a la mitad, pondremos 0.5")]
    [Range(0.0f, 1.0f)]
    public float distanciaCentro = 0.5f;
    public float medidaBoton = 1f;
    public float tiempoEfecto = 1.0f;
    public float tiempoTrigger = 1.0f;
   
    private float distanciaInicial;
    private float medidaInicial;
    private float counter = 0;
    private float timer = 0;

    Vector3 distCentroCuandoActivo, medidaBotonCuandoActivo;
    GameObject boton;
    Image trigger;

	void Start ()
    {
        boton = gameObject.transform.parent.gameObject;
        trigger = gameObject.transform.Find("Timer").GetComponent<Image>();
        distanciaInicial = boton.transform.localScale.x;
        medidaInicial = gameObject.transform.localScale.x;
	}
	
	void Update () 
    {
        if (estoyEnFoco && !Niveles.instance.hayUnPanelActivado)
        {
            counter += Time.deltaTime / tiempoEfecto;

            if (counter < 1)
            {
                boton.transform.localScale = Vector3.MoveTowards(boton.transform.localScale, new Vector3(distanciaCentro, distanciaCentro, distanciaCentro), Time.deltaTime / tiempoEfecto);
                gameObject.transform.localScale = Vector3.MoveTowards(gameObject.transform.localScale, new Vector3(medidaBoton, medidaBoton, medidaBoton), Time.deltaTime / tiempoEfecto);
            }
            else
            {
                counter = 1;

                timer += Time.deltaTime / tiempoTrigger;
                trigger.fillAmount = timer;

                if (timer > 1)
                {
                    CargarNivel();
                }
            }
        }
        else
        {
            if (counter > 0)
            {
                counter -= Time.deltaTime;
                timer = 0;
                trigger.fillAmount = timer;

                boton.transform.localScale = Vector3.MoveTowards(boton.transform.localScale, new Vector3(distanciaInicial, distanciaInicial, distanciaInicial), Time.deltaTime / tiempoEfecto);
                gameObject.transform.localScale = Vector3.MoveTowards(gameObject.transform.localScale, new Vector3(medidaInicial, medidaInicial, medidaInicial), Time.deltaTime / tiempoEfecto);
            }
        }
	}

    public override void Accion()
    {
        Debug.Log("Mi acción como BotonMenu");
    }
}
