﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BotonMenuPanel : BotonBase 
{

	void Update () 
    {
        if (estoyEnFoco)
        {
            if (Input.GetMouseButtonUp(0) && DeteccionBoton.distance < 0.3f)
            {
                ActionsButtons.instance.Accion(accion, panel);
                GetComponentInChildren<Text>().color = Niveles.instance.colorBotonesDefecto;
                base.DispararSonido();
            }

            if (Input.GetMouseButtonDown(0) && DeteccionBoton.distance < 0.3f)
            {
                GetComponentInChildren<Text>().color = Niveles.instance.colorBotonesPresionados;
            }
        }
	}

    public override void Accion()
    {
        Debug.Log("Mi acción como Boton Menú Panel");
    }
}
