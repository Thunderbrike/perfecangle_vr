﻿using UnityEngine;
using System.Collections;

public class PistaNormal : PistaBase {

	void Start () 
    {
        base.InicializaVariables();
	}
	
	void Update () 
    {
        ColorearPista();
	}

    void ColorearPista()
    {
        if (Niveles.instance.tipoPista == Niveles.TipoPista.Color)
            gameObject.GetComponent<UnityEngine.UI.Image>().color = base.ColorearPista();
        else
            gameObject.GetComponent<UnityEngine.UI.Image>().color = Color.white;
    }
}
