﻿using UnityEngine;
using System.Collections;

public class PistaBase : MonoBehaviour {

    public Color verde = new Color(0.32f, 0.85f, 0.16f);
    public Color rojo = new Color(0.85f, 0.16f, 0.16f);

    protected GameObject mainCamera;
    protected Vector3 solution;
   
    protected void InicializaVariables()
    {
        mainCamera = Camera.main.gameObject;
        solution = mainCamera.GetComponent<GameManager>().solution;     
    }

    protected void LookAtCamera()
    {
        gameObject.transform.LookAt(mainCamera.transform.position);
    }

    protected Color ColorearPista()
    {
        Color colorPista = Color.white;
        return Color.Lerp(rojo, verde, 1.0f / Vector3.Distance(mainCamera.transform.position, solution));
    }

}
