﻿using UnityEngine;
using System.Collections;

public class PistaVR : PistaBase {

    private float radio;
    private float initScale;
    private float distance;

	void Start () 
    {
        base.InicializaVariables();

        radio = base.mainCamera.GetComponent<GameManager>().newDistance;
        initScale = gameObject.transform.localScale.x;

        if (Niveles.instance.tipoPista == Niveles.TipoPista.Color)
            gameObject.transform.Find("Fondo").gameObject.GetComponent<SpriteRenderer>().material.color = Color.white;

	}
	
	void Update () 
    {
        base.LookAtCamera();
        EscalarPista();
        ColorearPista();
	}

    void EscalarPista()
    {
        distance = Vector3.Distance(base.mainCamera.transform.position, gameObject.transform.position);
        float factorEscala = distance / radio;

        if (distance > radio)
            gameObject.transform.localScale = new Vector3(initScale, initScale, initScale);
        else
            gameObject.transform.localScale = new Vector3(initScale * factorEscala, initScale * factorEscala, initScale * factorEscala);
    }

    void ColorearPista()
    {
        if (Niveles.instance.tipoPista == Niveles.TipoPista.Color)
            gameObject.GetComponent<SpriteRenderer>().material.color = base.ColorearPista();
        else
            gameObject.GetComponent<SpriteRenderer>().material.color = Color.white;
    }
}
