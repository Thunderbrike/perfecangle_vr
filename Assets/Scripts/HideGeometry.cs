﻿using UnityEngine;
using System.Collections;

public class HideGeometry : MonoBehaviour {
	
	void Start ()
	{
		gameObject.GetComponent<MeshRenderer>().enabled = false;
	}
}
