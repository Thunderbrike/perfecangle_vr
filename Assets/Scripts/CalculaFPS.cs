﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CalculaFPS : MonoBehaviour {

    public Text textFPS;

    int cantidadFrames;
    float currentAverageFps;

    void Update()
    {

        textFPS.text = UpdateAverageFpsAcumulados(1 / Time.deltaTime).ToString();
    }

    float UpdateAverageFpsAcumulados(float newFPS)
    {
        ++cantidadFrames;
        currentAverageFps += (newFPS - currentAverageFps) / cantidadFrames;

        return currentAverageFps;
    }
}
