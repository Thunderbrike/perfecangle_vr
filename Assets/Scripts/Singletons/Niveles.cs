﻿using UnityEngine;
using System.Collections.Generic;

public class Niveles 
{
    private static Niveles sharedinstance = null;

    public static Niveles instance
    {
        get
        {
            if (Niveles.sharedinstance == null)
            {
                sharedinstance = new Niveles();
                return sharedinstance;
            }
            else
            {
                return Niveles.sharedinstance;
            }
        }
    }

    //WIP:Brike -- Poner la variable a false antes de compilar. Si no, no irá al menú pausa cuando pierde el foco
    public bool debugModoPausaDesactivado = false;
    public bool debugTodosLosNivelesDesbloqueados = false;

    public enum ModoJuego { VR, AR, Normal };
    public ModoJuego modoJuego = ModoJuego.VR;

    public enum TipoPista { None, Forma, Color };
    public TipoPista tipoPista = TipoPista.None;
 
    public bool escenaInicialCargada = false;
    public bool esLaPrimeraVezQueJuego = false;
    public bool puedoEmpezarAJugar = false;
    public bool hayUnPanelActivado = false;
    public bool tengoQueCargarPantallaCompras = false;
    public bool tengoQueCargarPantallaFinal = false;
    public bool heMostradoLaPantallaDeRate = false;
    public bool tengoQueMostrarLaPantallaDeRateAlInicio = false;
    public bool audioManagerInstanciado = false;
    public bool buttonBackManagerInstanciado = false;
    public bool puedoEnviarAlgunEvento = true;
    public bool yaHeUsadoUnaPistaAlgunaVez = false;
    public bool vengoDelMenuEntrada = true;
    public bool tengoQueMostrarVideo= false;

    //Colores
    public Color colorBotonesDefecto = new Color(0.86f, 0.86f, 0.86f);
    public Color colorBotonesPresionados = new Color(0.09f, 0.87f, 1.0f);

    //Opciones
    public bool musica = true;
    public bool sonido = true;

    //Niveles
    public int nivelDeCalidadParaModoVR = 0;
    public int nivelDeCalidadParaModoNormal = 0;

    public List<Sprite> iconoNivel = new List<Sprite>();

    public int minimoNivelRate = 5;
    public int maximoNivelRate = 10;
    public int maximoNivelFree = 12;
    public int numNivelAlcanzado;
    public int numPuzzle;

    public string[] nombreNivel = {
								"Rubik",                 
								"FinalKickCachitos",								
		                        "BarcoHundido",
		                        "Extintor",
								"CocheMinidrivers",
								"Manzana",
								"Trompeta",
								"Cohete",
								"MotoMinibikers",
								"AppleWatch",
								"EstrellaPolicia",
								"Refresco",
								"Cerezas",
								"Sapo",
                                "Tetera",
                                "PavoHorno",
                                "ArosOlimpicos",
                                "Helado",
                                "Peine",
                                "CuboPlaya",
                                "CaballitoMar",
                                "Excavadora",
                                "PorscheCoche",
                                "Martillo",
                                "Pinza",
                                "IvanovichRocas",
                                "RuedaEngranajes",
                                "BlocNotas",
		                        "Lupa",
                                "PlumaEstilografica", 
                                "Locomotora",
                                "CafeteraItaliana",
                                "JuegoBolasMovimiento",
                                "LlavePuerta",
                                "CruzImposible",
                                "LamparaMesa",
                                "TorreEiffel",
                                "Tobogan",
                                "BotaZapato",
                                "Hidrante",
                                "BarrilMadera",
                                "Taburete",
		                        "Esposas",
								"AvionJumbo", 
                                "MaquinillaAfeitar",
                                "Camilla",
                                "Esqueleto",
                                "Cerebro",
                                "Silla",
                                "Perro",
                                "ZapatoTacon",
                                "AvionF22",
								"TazaCafe",
								"BarcoVela",
                                "SombreroGangster",
								"Banana",
                                "Oso",
                                "Chupete",
								"IconoNumtris",
                                "BotellaDePlastico",
                                "ConchasDeMar",
                                "Caracola",
                                "TorreAjedrez",
                                "Gorra",
                                "Gaviota",             
                                "Paloma",
                                "Ancora",
                                "BreakingFarm",
                                "FinalKickSombras", 
								"Pelicano"
                           };

    public int GetNumeroDeSpriteSegunNombreDelNivel(string nivel)
    {
        int n = 0;

        for (int m = 0; m < iconoNivel.Count; m++)
        {
            if (iconoNivel[m].name == nivel)
            {
                n = m;
                break;
            }
        }

        return n;
    }
}

