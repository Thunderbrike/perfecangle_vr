﻿using UnityEngine;
using System.Collections;

public class ActionsButtons
{
    private static ActionsButtons sharedinstance = null;

    public static ActionsButtons instance
    {
        get
        {
            if (ActionsButtons.sharedinstance == null)
            {
                sharedinstance = new ActionsButtons();
                return sharedinstance;
            }
            else
            {
                return ActionsButtons.sharedinstance;
            }
        }
    }

    public void Accion(string accion, GameObject objeto)
    {
        if (Niveles.instance.puedoEnviarAlgunEvento)
        {
            Niveles.instance.puedoEnviarAlgunEvento = false;
			
            switch (accion)
            {
                case "MODO_VR": CargoModoJuego(objeto, Niveles.ModoJuego.VR); break;
                case "MODO_NORMAL": CargoModoJuego(objeto, Niveles.ModoJuego.Normal); break;
                case "RATE": Ratear(); break;
                case "RATE_PANEL": RatearDesdePanel(objeto); break;
                case "BUY_PANEL": ComprarJuegoDesdePanel(objeto); break;
                case "BUY": ComprarJuegoDesdeMenu(objeto); break;
                case "INCLUDED": Included(objeto); break;
                case "FREE": DescargarPerfectAngle(); break;
                case "RESTORE": RestaurarCompras(objeto); break;                
                case "CANCEL": CancelarCompra(objeto); break;
                case "PLAY": EmpiezoAJugar(objeto); break;
                case "MENU_PAUSA": MenuPausa(); break;
                case "MENU_PISTA": MenuPista(); break;
                case "MENU_ENTRADA": MenuEntrada(); break;
                case "PERFECT_ANGLE": PerfectAngle(); break;
                case "DOWNLOAD": DescargarPerfectAngleFinMenu(objeto); break;
                case "END": CerrarPanel(objeto); break;
                case "CIERRO_CANVAS": CerrarCanvas(objeto); break;
                case "OCULTO_CANVAS": OcultarCanvas(objeto); break;
                case "MUSIC": Musica(objeto); break;
                case "SOUND": Sonido(objeto); break;
                case "LANGUAGE": Idioma(objeto); break;
                case "QUALITY": Calidad(objeto); break;
                case "SPECTACLES": Gafas(objeto);  break;
                case "RESET_VALUES": ResetearValores(objeto);  break;
                case "SUPPORT": Soporte(objeto); break;
                case "FACEBOOK": Facebook(objeto); break;
                case "TWITTER": Twitter(objeto); break;
                case "RECOMMEND": Recomienda(objeto); break;
                case "IVANOVICH": IvanovichGames(objeto); break;
                case "EXIT": ExitGame(objeto); break;
				case "sinPistas": SinPistas(objeto);break; 
				case "MENU_URL": MenuUrl(); break;
				case "VER_URL": VerUrl(); break;
				case "CONTINUAR_JUGANDO_FREE": ContinuarJugandoFree(); break;
				case "PERFECT_ANGLE_CON_VIDEO": PerfectAngleConVideo(objeto); break;
				case "BUY_PREMIUM_VIDEO": PerfectAngleBuyPremium(objeto); break;
				case "NO_VIDEOS_DISPONIBLES": NoHayVideosDisponibles(objeto); break;
            }
        }
    }

    #region PRIVATE METHODS

	void NoHayVideosDisponibles(GameObject canvas){
		MetodosComunes.instance.NoHayVideosDisponibles();
	}

	void ContinuarJugandoFree(){
		MetodosComunes.instance.MenuJugarViendoVideo();
	}

    void CargoModoJuego(GameObject canvas, Niveles.ModoJuego modoJuego)
    {
        canvas.GetComponent<PanelModoJuego>().CargoModoJuego(modoJuego);
    }

    void Ratear()
    {
        MetodosComunes.instance.Ratear();
    }

    void RatearDesdePanel(GameObject canvas)
    {
        canvas.GetComponent<PanelRate>().Ratear();
    }

    void Included(GameObject canvas)
    {
        canvas.GetComponent<PanelMenu>().Included();
    }

    void ComprarJuegoDesdePanel(GameObject canvas)
    {
        canvas.GetComponent<PanelCompras>().CompraGenerica();
    }

    void ComprarJuegoDesdeMenu(GameObject canvas)
    {
        canvas.GetComponent<PanelMenu>().CompraGenerica();
    }

    void DescargarPerfectAngle()
    {
        MetodosComunes.instance.DescargarPerfectAngle();
    }

    void RestaurarCompras(GameObject canvas)
    {
        canvas.GetComponent<PanelMenu>().RestaurarCompras();
    }

    void CancelarCompra(GameObject canvas)
    {
        canvas.GetComponent<PanelCompras>().Cancelar();
    }

    void EmpiezoAJugar(GameObject canvas)
    {
        canvas.GetComponent<PanelInfo>().EmpiezoAJugar();
    }

    void MenuPausa()
    {
        MetodosComunes.instance.MenuPausa();
    }

	void MenuUrl()
    {
        MetodosComunes.instance.MenuUrl();
    }

	void VerUrl(){
		Debug.Log("Open Url");
		//Application.OpenURL();
	}

    void PerfectAngle()
    {
        MetodosComunes.instance.PerfectAngle();
    }

	
	void PerfectAngleConVideo(GameObject canvas)
    {
		canvas.GetComponent<PanelJugarViendoVideo>().MostrarUnVideo();
    }


	void PerfectAngleBuyPremium(GameObject canvas)
    {
		canvas.GetComponent<PanelJugarViendoVideo>().BuyPremium();
    }
	

    void MenuPista()
    {
		MetodosComunes.instance.MenuPista();
    }

    void MenuEntrada()
    {
        MetodosComunes.instance.MenuEntrada();
    }

    void CerrarCanvas(GameObject canvas)
    {
        MetodosComunes.instance.DestroyPanel(canvas);
    }

    void OcultarCanvas(GameObject canvas)
    {
        MetodosComunes.instance.OcultarCanvas(canvas);
    }

    void DescargarPerfectAngleFinMenu(GameObject canvas)
    {
        canvas.GetComponent<PanelFinJuego>().AbrirTienda();
    }

    void CerrarPanel(GameObject canvas)
    {
        canvas.GetComponent<PanelFinJuego>().Cancelar();
    }

    void Musica(GameObject canvas)
    {
        canvas.GetComponent<PanelOpciones>().Musica();
    }

    void Sonido(GameObject canvas)
    {
        canvas.GetComponent<PanelOpciones>().Sonido();
    }

    void Idioma(GameObject canvas)
    {
        canvas.GetComponent<PanelOpciones>().Idioma();
    }

    void Calidad(GameObject canvas)
    {
        canvas.GetComponent<PanelOpciones>().Calidad();
    }

    void Gafas(GameObject canvas)
    {
        canvas.GetComponent<PanelOpciones>().Gafas();
    }

    void ResetearValores(GameObject canvas)
    {
        canvas.GetComponent<PanelOpciones>().ResetearValores();
    }

    public void Soporte(GameObject canvas)
    {
        canvas.GetComponent<PanelAcciones>().Soporte();
    }

    public void Facebook(GameObject canvas)
    {
        canvas.GetComponent<PanelAcciones>().Facebook();
    }

    public void Twitter(GameObject canvas)
    {
        canvas.GetComponent<PanelAcciones>().Twitter();
    }

    public void Recomienda(GameObject canvas)
    {
        canvas.GetComponent<PanelAcciones>().Recomienda();
    }

    public void IvanovichGames(GameObject canvas)
    {
        MetodosComunes.instance.IvanovichGames();
    }

    public void ExitGame(GameObject canvas)
    {
        MetodosComunes.instance.ExitGame();
    }

	public void SinPistas(GameObject canvas)
    {
		canvas.transform.Find("PanelSinPistas").GetComponent<PanelSinPistas>().MostrarPanelSinPistas();
    }
    #endregion

	
	
}
