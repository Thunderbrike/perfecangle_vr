﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MetodosComunes : MonoBehaviour
{
    private static MetodosComunes sharedinstance = null;

    public static MetodosComunes instance
    {
        get
        {
            if (MetodosComunes.sharedinstance == null)
            {
                sharedinstance = new MetodosComunes();
                return sharedinstance;
            }
            else
            {
                return MetodosComunes.sharedinstance;
            }
        }
    }

    #region METODOS COMUNES

    public void ModoDeJuego()
    {
        FadeEntreEscenas.instance.FadeIn("ModoJuego");
    }

    public void MenuEntrada()
    {
        Niveles.instance.hayUnPanelActivado = false;
        FadeEntreEscenas.instance.FadeIn("MenuEntrada");
    }

    public void MenuPausa()
    {
        FadeEntreEscenas.instance.FadeIn("MenuPausa");
    }

	public void MenuUrl()
    {
        FadeEntreEscenas.instance.FadeIn("MenuUrl");
    }

	public void MenuJugarViendoVideo()
    {
		FadeEntreEscenas.instance.FadeIn("MenuJugarViendoVideo");
    }

	public void NoHayVideosDisponibles()
    {
		FadeEntreEscenas.instance.FadeIn("MenuNoHayVideos");
    }

    public void MenuPista()
    {
        FadeEntreEscenas.instance.FadeIn("MenuPista");
    }

    public void PerfectAngle()
    {
        FadeEntreEscenas.instance.FadeIn("PerfectAngle");
    }

    public void DestroyPanel(GameObject canvas)
    {
        if (SceneManager.GetActiveScene().name == "PerfectAngle" ||
            (SceneManager.GetActiveScene().name == "MenuEntrada" && Niveles.instance.modoJuego == Niveles.ModoJuego.Normal))
            GameObject.Find("Camera").gameObject.GetComponent<MouseOrbit>().enabled = true;

        Niveles.instance.puedoEnviarAlgunEvento = true;

        Destroy(canvas);
    }

    public void OcultarCanvas(GameObject canvas)
    {
        Niveles.instance.puedoEnviarAlgunEvento = true;
        canvas.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void IvanovichGames()
    {
        Niveles.instance.puedoEnviarAlgunEvento = true;
        Application.OpenURL("http://www.ivanovichgames.com/web/");
    }

    public void DescargarPerfectAngle()
    {
#if UNITY_IPHONE
		Application.OpenURL ("https://itunes.apple.com/app/id1040464173?mt=8&ign-mpt=uo%3D8");
#elif UNITY_ANDROID && !AMAZON
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.ivanovichgames.PerfectAngle");
#elif AMAZON
        Application.OpenURL("http://www.amazon.com/gp/product/B019S42SO4");
#elif UNITY_WSA
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.ivanovichgames.PerfectAngleAR");
#endif

        Niveles.instance.puedoEnviarAlgunEvento = true;
    }

    public void Ratear()
    {
#if UNITY_IPHONE
		Application.OpenURL ("https://itunes.apple.com/app/id1085087438?mt=8&ign-mpt=uo%3D8");
#elif UNITY_ANDROID && !AMAZON
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.ivanovichgames.PerfectAngleAR");
#elif AMAZON
        Application.OpenURL("http://www.amazon.com/gp/product/B01CP0QLGE");
#elif UNITY_WSA
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.ivanovichgames.PerfectAngleAR");
#endif

        Niveles.instance.puedoEnviarAlgunEvento = true;
    }

    public void ComprobarModoJuego(GameObject objeto)
    {
        float rayCastDistance = 0;

        switch (Niveles.instance.modoJuego)
        {
            case Niveles.ModoJuego.VR:
                rayCastDistance = 3;
                objeto.GetComponent<Camera>().fieldOfView = 60;
                objeto.transform.Find("PuntoMira").gameObject.SetActive(true);
                break;

            case Niveles.ModoJuego.Normal:
                rayCastDistance = 8;
                objeto.GetComponent<Camera>().fieldOfView = SceneManager.GetActiveScene().name == "MenuEntrada" ? 35 : 45;
                objeto.transform.Find("PuntoMira").gameObject.SetActive(false);

                Destroy(objeto.GetComponent<StereoController>());
                Destroy(objeto.GetComponent<CardboardHead>());
                Destroy(objeto.transform.Find("Camera Left").gameObject);
                Destroy(objeto.transform.Find("Camera Right").gameObject);
                Destroy(GameObject.Find("Cardboard").gameObject);

                objeto.GetComponent<MouseOrbit>().enabled = true;
                break;

            case Niveles.ModoJuego.AR:
                break;
        }

        objeto.GetComponent<DeteccionBoton>().rayCastDistance = rayCastDistance;
    }

	public void MostrarPublicidad(){


		if( Niveles.instance.modoJuego != Niveles.ModoJuego.Normal || ComprasManager.instance.hemosCompradoElJuego ) return;

		#if AMAZON
		return;
		#endif
		if( !VersionManager.haPasadoRevisionDefault_FALSE ) return;

		if( Niveles.instance.numPuzzle<3 ) return;

		if( Niveles.instance.tengoQueMostrarVideo ){
			GameManager.loopPublicidad=0;
		}


		if( GameManager.loopPublicidad==0){
		//WIP:VIDEO
		//mostrar Video
//			MostrarUnVideo();
			PlayerPrefs.SetInt("TengoQueMostrarVideo",1);
			Niveles.instance.tengoQueMostrarVideo = true ;
			MetodosComunes.instance.MenuJugarViendoVideo();
		}else{
		//mostrar Interticiales segun porciento del php, por defecto 50%
		#if !UNITY_STANDALONE && !UNITY_TVOS
			AdsManager.AdsManagerScript.PlayAIntertitial();
		#endif
		}

		GameManager.loopPublicidad++;
		if( GameManager.loopPublicidad >2) GameManager.loopPublicidad=0;

    }

	public void MostrarUnVideo(){

		if( Niveles.instance.modoJuego != Niveles.ModoJuego.Normal || ComprasManager.instance.hemosCompradoElJuego ) return;

		#if AMAZON
		return;
		#endif
		if( !VersionManager.haPasadoRevisionDefault_FALSE ) return;

		#if !UNITY_STANDALONE && !UNITY_TVOS
		if( AdsManager.AdsManagerScript.Hay_videos() ){

			Debug.Log("Hay VIDEOs") ;

			StartCoroutine( AdsManager.AdsManagerScript.PlayAVideo("null", value=>{
				if(value){
					Debug.Log("Video finalizado con Exito");
				}else{
					Debug.Log("Video no ha finalizado");
				}
			}) 
			);
			
		}else{
			Debug.Log("No hay videos");
		}
		#else
		#endif
    }

    #endregion
}
