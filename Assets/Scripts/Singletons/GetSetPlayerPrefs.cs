﻿using UnityEngine;

public class GetSetPlayerPrefs {

    private static GetSetPlayerPrefs sharedinstance = null;

    public static GetSetPlayerPrefs instance
    {
        get
        {
            if (GetSetPlayerPrefs.sharedinstance == null)
            {
                sharedinstance = new GetSetPlayerPrefs();
                return sharedinstance;
            }
            else
            {
                return GetSetPlayerPrefs.sharedinstance;
            }
        }
    }

    public GetSetPlayerPrefs()
    {
        //WIP:Brike -- RESETEAR VARIABLES
        //PlayerPrefs.DeleteKey("NumeroNivelAlcanzado");
        //PlayerPrefs.DeleteKey("TengoQueMostrarLaPantallaDeRateAlInicio");
        //PlayerPrefs.DeleteKey("HemosMostradoLaPantallaDeRate");
        //PlayerPrefs.DeleteKey("YaHeUsadoUnaPistaAlgunaVez");
//        PlayerPrefs.DeleteKey("HemosCompradoElJuego");
        //PlayerPrefs.DeleteKey("QualityVR");
        //PlayerPrefs.DeleteKey("QualityNormal");
        //PlayerPrefs.DeleteKey("OpcionesMusica");
        //PlayerPrefs.DeleteKey("Language");

        // Número de nivel alcanzado. Si es 0 es que aún no hemos jugado nunca
        if (PlayerPrefs.HasKey("NumeroNivelAlcanzado"))
        {
            Niveles.instance.numNivelAlcanzado = PlayerPrefs.GetInt("NumeroNivelAlcanzado");

            if (Niveles.instance.debugTodosLosNivelesDesbloqueados)
                Niveles.instance.numNivelAlcanzado = 70;

            if (Niveles.instance.numNivelAlcanzado > 0)
                Niveles.instance.esLaPrimeraVezQueJuego = false;
            else
                Niveles.instance.esLaPrimeraVezQueJuego = true;
        }
        else
        {
            Niveles.instance.numNivelAlcanzado = 0;
            Niveles.instance.esLaPrimeraVezQueJuego = true;
            PlayerPrefs.SetInt("NumeroNivelAlcanzado", 0);
        }

        //Debug.Log(Niveles.instance.numNivelAlcanzado);
        //Debug.Log("Es la primera vez que juego: " + Niveles.instance.esLaPrimeraVezQueJuego);

#if !AMAZON
        // Hemos comprado ya el juego?
        if (PlayerPrefs.HasKey("HemosCompradoElJuego"))
            ComprasManager.instance.hemosCompradoElJuego = PlayerPrefs.GetInt("HemosCompradoElJuego") == 1 ? true : false;
        else
        {
            ComprasManager.instance.hemosCompradoElJuego = false;
            PlayerPrefs.SetInt("HemosCompradoElJuego", 0);
        }
#else
        ComprasManager.instance.hemosCompradoElJuego = true;
        PlayerPrefs.SetInt("HemosCompradoElJuego", 1);
#endif

        // Paso el valor de la calidad a una variable en caso que ya hayamos pasado por la pantalla de detección de calidades
        if (PlayerPrefs.HasKey("QualityVR"))
        {
            Niveles.instance.nivelDeCalidadParaModoVR = PlayerPrefs.GetInt("QualityVR");
        }

        if (PlayerPrefs.HasKey("QualityNormal"))
        {
            Niveles.instance.nivelDeCalidadParaModoNormal = PlayerPrefs.GetInt("QualityNormal");
        }

        if (PlayerPrefs.HasKey("TengoQueMostrarLaPantallaDeRateAlInicio")) 
            Niveles.instance.tengoQueMostrarLaPantallaDeRateAlInicio = true;

        if (PlayerPrefs.HasKey("HemosMostradoLaPantallaDeRate"))
            Niveles.instance.heMostradoLaPantallaDeRate = true;

        if (PlayerPrefs.HasKey("YaHeUsadoUnaPistaAlgunaVez"))
            Niveles.instance.yaHeUsadoUnaPistaAlgunaVez = true;

		if (PlayerPrefs.HasKey("TengoQueMostrarVideo"))
            Niveles.instance.tengoQueMostrarVideo = true;

        // Música
        if (PlayerPrefs.HasKey("OpcionesMusica"))
            Niveles.instance.musica = PlayerPrefs.GetInt("OpcionesMusica") == 1 ? true : false;
        else
        {
            Niveles.instance.musica = true;
            PlayerPrefs.SetInt("OpcionesMusica", 1);
        }

        // Sonido
        if (PlayerPrefs.HasKey("OpcionesSonidos"))
            Niveles.instance.sonido = PlayerPrefs.GetInt("OpcionesSonidos") == 1 ? true : false;
        else
        {
            Niveles.instance.sonido = true;
            PlayerPrefs.SetInt("OpcionesSonidos", 1);
        }

		if (PlayerPrefs.HasKey("Pistas")){
			GameManager.numPistas=PlayerPrefs.GetInt("Pistas");
		}
		else
		{
			GameManager.numPistas=4;
			PlayerPrefs.SetInt("Pistas", 4);
		}
	
	}
}
