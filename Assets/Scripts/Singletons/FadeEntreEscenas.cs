﻿using UnityEngine;

public class FadeEntreEscenas : MonoBehaviour {

    private static FadeEntreEscenas sharedinstance = null;

    public static FadeEntreEscenas instance
    {
        get
        {
            if (FadeEntreEscenas.sharedinstance == null)
            {
                sharedinstance = new FadeEntreEscenas();
                return sharedinstance;
            }
            else
            {
                return FadeEntreEscenas.sharedinstance;
            }
        }
    }

    public void FadeIn(string nombreEscena)
    {
        GameObject fadeIn = Instantiate(Resources.Load("Prefabs/FadeIn")) as GameObject;
        fadeIn.GetComponent<FadeIn>().CambioEscena(nombreEscena);
	}

    public void FadeOut()
    {
        GameObject fadeOut = Instantiate(Resources.Load("Prefabs/FadeOut")) as GameObject;
    }
}
