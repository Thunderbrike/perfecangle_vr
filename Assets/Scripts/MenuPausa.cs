﻿using UnityEngine;
using System.Collections;

public class MenuPausa : MonoBehaviour 
{
	void Start ()
    {
        FadeEntreEscenas.instance.FadeOut();
        GameObject panelnfo = Instantiate(Resources.Load("Prefabs/PanelPausa" + (Niveles.instance.modoJuego == Niveles.ModoJuego.VR ? "VR" : "Normal")), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
        MetodosComunes.instance.ComprobarModoJuego(gameObject);
	}
}
