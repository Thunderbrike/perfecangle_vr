﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Temporizador : MonoBehaviour, ICardboardPointer
{
    private GameObject temporizador;
    GazeInputModule gazeInput;

    void Start()
    {

    }

    void OnEnable()
    {
        GazeInputModule.cardboardPointer = this;
    }

    void OnDisable()
    {
        if (GazeInputModule.cardboardPointer == this)
        {
            GazeInputModule.cardboardPointer = null;
        }
    }

    void Update() {
    }

    public void OnGazeEnabled() {
    }

    public void OnGazeDisabled() {
    }

    public void OnGazeStart(Camera camera, GameObject targetObject, Vector3 intersectionPosition) {
        //Debug.Log("DENTRO");
        gameObject.GetComponent<Animator>().SetBool("temporizador", true);
        //gameObject.GetComponent<SpriteRenderer>().enabled = true;
    }

    public void OnGazeStay(Camera camera, GameObject targetObject, Vector3 intersectionPosition) {
    }

    public void OnGazeExit(Camera camera, GameObject targetObject) {

        ApagarTemporizador();
    }

    public void OnGazeTriggerStart(Camera camera) {
    }

    public void OnGazeTriggerEnd(Camera camera) {

    }

    public void Accion()
    {
        ApagarTemporizador();
        UnityEngine.SceneManagement.SceneManager.LoadScene("MenuEntrada");
        
        //EventSystem.current.GetComponent<GazeInputModule>().HandleTrigger();
    }

    void ApagarTemporizador()
    {
        //Debug.Log("FUERA");
        gameObject.GetComponent<Animator>().SetBool("temporizador", false);
        gameObject.GetComponent<Animator>().Rebind();
        //gameObject.GetComponent<SpriteRenderer>().enabled = false;
    }
}