﻿using UnityEngine;
using System.Collections;

public class GenericEffectHelpers : MonoBehaviour {

    private static GenericEffectHelpers sharedinstance = null;

    public static GenericEffectHelpers instance
    {
        get
        {
            if (GenericEffectHelpers.sharedinstance == null)
            {
                sharedinstance = new GenericEffectHelpers();
                return sharedinstance;
            }
            else
            {
                return GenericEffectHelpers.sharedinstance;
            }
        }
    }

    public void FadeCanvas(GameObject obj, float initValue, float finalValue, float duration)
    {
		if(obj == null){

			return;
		}

        if (obj.GetComponent<CanvasFade>() != null) {
            Destroy(obj.GetComponent<CanvasFade>());
        }

        obj.AddComponent<CanvasFade>();
        obj.GetComponent<CanvasFade>().canvasInitValue = initValue;
        obj.GetComponent<CanvasFade>().canvasFinalValue = finalValue;
        obj.GetComponent<CanvasFade>().durationValue = duration;
    }
}
