﻿using UnityEngine;
using System.Collections;

public class MenuNoHayVideoDisponibles : MonoBehaviour {
	void Start()
    {
		FadeEntreEscenas.instance.FadeOut();
		GameObject panelnfo = Instantiate(Resources.Load("Prefabs/PanelNoHayVideos"), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
        MetodosComunes.instance.ComprobarModoJuego(gameObject);
     }
}
