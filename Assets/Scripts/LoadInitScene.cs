﻿using UnityEngine;
using System.Collections;

public class LoadInitScene : MonoBehaviour 
{
    void Awake()
    {
        if (!Niveles.instance.escenaInicialCargada)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
            Niveles.instance.escenaInicialCargada = true;
        }
        Destroy(gameObject);
    }
}
