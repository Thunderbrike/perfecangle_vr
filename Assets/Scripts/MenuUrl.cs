﻿using UnityEngine;
using System.Collections;

public class MenuUrl : MonoBehaviour {

	void Start ()
    {
        FadeEntreEscenas.instance.FadeOut();
        GameObject panelnfo = Instantiate(Resources.Load("Prefabs/PanelURL"), gameObject.transform.position, gameObject.transform.rotation) as GameObject;
        MetodosComunes.instance.ComprobarModoJuego(gameObject);
	}
}
