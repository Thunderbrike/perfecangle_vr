﻿using UnityEngine;
using System.Collections;

public class Solution : MonoBehaviour
{
    public GameObject explosion;
    public GameObject cambioModelo;

    const float TIEMPO_EN_SOLUCION                      = 2.5f;     // En segundos
    const float TIEMPO_DE_APARICION_SIGUIENTE_PUZZLE    = 4f;       // En segundos
    const float TIEMPO_DE_POSICIONAMIENTO               = 0.3f;     // En segundos
    const float FACTOR_DE_ACERCAMIENTO                  = 0.75f;    // En porcentage 0.75 -> 75% respecto la distancia actual. (1.0 -> 100% y no nos acercaríamos)
    const float VELOCIDAD_DE_ACERCAMIENTO               = 0.5f;     // Cuanto mayor es el valor, más rápido se acerca

	float timeInSolution;
    float timeInitLevel;
    float nuevaDistancia;

    bool flagActivoModeloSolucionado;
    bool flagPosicionarModelo = false;
    bool flagAcercarCamara = false;

    GameObject explosionPosition;
    GameObject modeloSolucionado, clone;

    void Start()
    {
        timeInitLevel = TIEMPO_EN_SOLUCION;
        explosionPosition = gameObject.transform.Find("Explosion").gameObject;
    }

    void Update()
    {
        PosicionarModelo();
        AcercarCamara();
    }

	void OnTriggerEnter (Collider item)		// En cuanto entramos en el collider activamos la cuenta atras
	{
        if (item.tag == "Solution") timeInSolution = timeInitLevel;
	}
	
	void OnTriggerExit (Collider item)		// Si salimos de la solucion inicializamos la cuenta atras
	{
        if (item.tag == "Solution") timeInSolution = timeInitLevel;
	}
	
	void OnTriggerStay (Collider item)		// Mientras estemos dentro de la solucion, restamos el tiempo
	{
        // Si estamos dentro del collider ...
        if (item.tag == "Solution")
        {
            // ... vamos restando el tiempo hasta agotar el contador ...
            if (timeInSolution > 0)
            {
                timeInSolution -= Time.deltaTime;

            }
            // ... y si seguimos dentro y el tiempo de contador ha llegado a 0, activamos la solución
            else
            {
                if (!flagActivoModeloSolucionado)
                {
                    flagActivoModeloSolucionado = true;
                    StartCoroutine(MostrarObjetoSolucionado());
                }
            }
        }
	}

    IEnumerator MostrarObjetoSolucionado()
    {
        //WIP:Brike --Paro el tiempo para poder hacer las capturas
        //Time.timeScale = 0.1f;

        GetComponent<GameManager>().EstablezcoNuevoPuzzle();

        // Creamos la explosión 
        GameObject clone = Instantiate(explosion, explosionPosition.transform.position, explosionPosition.transform.rotation) as GameObject;
        clone.transform.parent = gameObject.transform;
        AudioManager.instance.PlayAudio(1, Resources.Load("Audios/Efectos/Solucion") as AudioClip, 1.0f);


		gameObject.GetComponent<GameManager>().puzzle.transform.Find("LandingController").gameObject.GetComponent<LandingSpotController>().ScareAll();

        // Apagamos las pistas y las reiniciamos
        Niveles.instance.tipoPista = Niveles.TipoPista.None;
        GetComponent<GameManager>().OcultarPistaVR();
        if (Niveles.instance.modoJuego == Niveles.ModoJuego.Normal)
            GetComponent<GameManager>().OcultarPistaNormal();

        // Pasado un poco, cuando las chisapas ya han salido, mostramos el objeto solucionado
        yield return new WaitForSeconds(0.5f);
        GameManager.modelo.SetActive(false);                
        modeloSolucionado = GameManager.modeloSolucionado;
        modeloSolucionado.SetActive(true);
        GameManager.piezas.SetActive(true);

        // Activamos las piezas si es que tiene y acercamos un poco la cámara al modelo solucionado
        if (GameManager.piezas.transform.childCount > 0)
            AudioManager.instance.PlayAudio(1, Resources.Load("Audios/Efectos/CaidaPiezas") as AudioClip, 0.25f);

        GameManager.soportes.SetActive(false);
        nuevaDistancia = MouseOrbit.instance.distance * FACTOR_DE_ACERCAMIENTO;
        flagAcercarCamara = true;

        // Posicionamos el objeto en su sitio en caso que no esté correctamente y lo rotamos
        yield return new WaitForSeconds(1);

        if (modeloSolucionado.GetComponent<RotateObject>().enabled == false)
            flagPosicionarModelo = true;
        else
            StartCoroutine(SiguientePuzzle());
    }

    IEnumerator SiguientePuzzle()
    {
        flagAcercarCamara = false;

        yield return new WaitForSeconds(1);
        clone = Instantiate(cambioModelo, GameManager.modeloSolucionado.transform.position, GameManager.modeloSolucionado.transform.rotation) as GameObject;

        yield return new WaitForSeconds(TIEMPO_DE_APARICION_SIGUIENTE_PUZZLE - 1);
        AudioManager.instance.PlayAudio(1, Resources.Load("Audios/Efectos/MagicPA") as AudioClip, 1.0f);
        flagActivoModeloSolucionado = false;

		gameObject.GetComponent<GameManager>().escena.transform.Find("Efectos/Mariposas").gameObject.GetComponent<FlockController>().destroyBirds();
        GetComponent<GameManager>().DestroyNullObject();
        GetComponent<GameManager>().PuzzleSiguiente();

		//WIP:VIDEO
        //Mostrar video ó Interticial segun toca
		MetodosComunes.instance.MostrarPublicidad();

		yield return new WaitForSeconds(0.05f);
		gameObject.GetComponent<GameManager>().puzzle.transform.Find("LandingController").gameObject.GetComponent<LandingSpotController>()._landOnStart = true;
		gameObject.GetComponent<GameManager>().puzzle.transform.Find("LandingController").gameObject.GetComponent<LandingSpotController>()._soarLand = false;
		gameObject.GetComponent<GameManager>().puzzle.transform.Find("LandingController").gameObject.GetComponent<LandingSpotController>()._onlyBirdsAbove = true;
		gameObject.GetComponent<GameManager>().escena.transform.Find("Efectos/Mariposas").gameObject.GetComponent<FlockController>()._childAmount = 4;
		gameObject.GetComponent<GameManager>().puzzle.transform.Find("LandingController").gameObject.GetComponent<LandingSpotController>().Start();
		gameObject.GetComponent<GameManager>().puzzle.transform.Find("LandingController").gameObject.GetComponent<LandingSpotController>().InstantLandOnStart(0.01f);
    }

    void PosicionarModelo()
    {
        if (flagPosicionarModelo)
        {
            modeloSolucionado.transform.rotation = Quaternion.Lerp(modeloSolucionado.transform.rotation, Quaternion.EulerAngles(0, 0, 0), Time.deltaTime / TIEMPO_DE_POSICIONAMIENTO);
            modeloSolucionado.transform.position = Vector3.Lerp(modeloSolucionado.transform.position, new Vector3(0, 1, 0), Time.deltaTime / TIEMPO_DE_POSICIONAMIENTO);
            modeloSolucionado.transform.localScale = Vector3.Lerp(modeloSolucionado.transform.localScale, new Vector3(1, 1, 1), Time.deltaTime / TIEMPO_DE_POSICIONAMIENTO);

            float distance = Vector3.Distance(modeloSolucionado.transform.position, new Vector3(0, 1, 0));

            if (distance < 0.001f)
            {
                modeloSolucionado.GetComponent<RotateObject>().enabled = true;
                StartCoroutine(SiguientePuzzle());
                flagPosicionarModelo = false;
            }
        }
    }

    void AcercarCamara()
    {
        if (flagAcercarCamara) {
            MouseOrbit.instance.distance = Mathf.Lerp(MouseOrbit.instance.distance, nuevaDistancia, Time.deltaTime / VELOCIDAD_DE_ACERCAMIENTO);
        }
    }
}
