﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Intro : MonoBehaviour {

    const int MIN_MEMORIA_DISPOSITIVO = 700; //Valor en Mb

    GameObject texto;
    GameObject canvasLowRam;
	
    GetSetPlayerPrefs getSetPlayerPrefs;
    Niveles niveles;
    LanguageManager languageManager;

    void Awake()
    {
        getSetPlayerPrefs = GetSetPlayerPrefs.instance;
        niveles = Niveles.instance;
        languageManager = LanguageManager.instance;
    }

	void Start () 
    {
        canvasLowRam = transform.Find("../Canvas").gameObject;
        texto = transform.Find("TextoLogoIvanovich").gameObject;
		texto.SetActive(false);
        niveles.escenaInicialCargada = true;
	}
		
	public void FinalAnimacionIntro()
    {
        StartCoroutine(ActivarTexto());
	}
	
    bool TengoSuficienteMemoriaEnDispositivo()
    {
#if UNITY_ANDROID || UNITY_EDITOR
        return SystemInfo.systemMemorySize <= MIN_MEMORIA_DISPOSITIVO ? false : true;
#else   // STEAM, STANDALONE...
        return true;
#endif
    }

    IEnumerator ActivarTexto(){
		yield return new WaitForSeconds(0.5f);
		texto.SetActive(true);

        if (TengoSuficienteMemoriaEnDispositivo()){
            CargarEscena();
        }
        else
        {
            canvasLowRam.SetActive(true);
        }
	}

    public void CargarEscena()
    {
        canvasLowRam.SetActive(false);

		Niveles.instance.modoJuego = Niveles.ModoJuego.VR;
		InstanciarAudioManager ();
		InstanciarButtonBackAndroid ();
		FadeEntreEscenas.instance.FadeIn("MenuEntrada");

		//FadeEntreEscenas.instance.FadeIn("ModoJuego");
    }

	private void InstanciarAudioManager()
	{
		if (!Niveles.instance.audioManagerInstanciado)
		{
			Niveles.instance.audioManagerInstanciado = true;
			GameObject audioManager = Instantiate(Resources.Load("Prefabs/AudioManager")) as GameObject;

			audioManager.name = "AudioManager";
		}
	}

	private void InstanciarButtonBackAndroid()
	{
		if (!Niveles.instance.buttonBackManagerInstanciado)
		{
			Niveles.instance.buttonBackManagerInstanciado = true;

			GameObject buttonBack = Instantiate(Resources.Load("Prefabs/ButtonBackManager")) as GameObject;
			buttonBack.name = "ButtonBackManager";
		}
	}
}